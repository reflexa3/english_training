package training;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import data.TakeProgresWords;
import windows.MainWindow;
import javazoom.jl.player.Player;

public class WordsTranslate extends JPanel {

	private static final long serialVersionUID = 4025773613259984868L;
	String[][] words;
	String[][] randwords;
	String[][] rightwords = new String[4][2];
	String[] temp =new String[5];
	Map<Integer, String> map = new HashMap<Integer, String>();
	Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
	int action,error=0; 
	int prog,prog1,x,x1,k;
	String item,item1,training;
	JButton var1, var2, var3, var4, var5, unknown, further;
	JLabel right_answer, wrong_answer,answer;
	
	public WordsTranslate(int a, String [][] data, String [][] data1, String q, String q1, String q2, int q3, int q4, Map<Integer, String> q5, Map<Integer, Integer> q6){
		
		k=a;
		words=data;
		randwords=data1;
		item=q;
		item1=q1;
		training=q2;
		prog=q3;  //Прогрес постепенно будет увеличиваться на х1 при каждом вызове конструктора
		x1=q4;   // Одинаковое значения
		map=q5;
		map1=q6;
				
		/*------------------------Создаем панели---------------------------------------*/

		// Создаем основную панель Border Layout
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(800, 500));
		
		//Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		Font fontword = new Font("Arial", Font.PLAIN, 24);
		Font fonttraining = new Font("Arial", Font.PLAIN, 24);
		Font fontbuttons = new Font("Arial", Font.PLAIN, 20);
		Font fonttrans = new Font("Arial", Font.PLAIN, 14);
		
		// Область где будет прогрес тренровки (North p1)
		Color yellow = new Color(250, 194, 90); // yellow
		JPanel p1 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p1.setLayout(fl);
		//p1.setBorder(Line1);
		p1.setBackground(yellow);
		
		// Создаем панель для Слова, переовода, картинки (West p2)
		JPanel p2 = new JPanel();
		GridBagLayout gbl = new GridBagLayout();
		p2.setLayout(gbl);
		//p2.setBorder(Line1);
		p2.setPreferredSize(new Dimension(350, 380));
		p2.setBackground(Color.white);

		// Создаем панель для выбора вариантов (East p3)
		JPanel p3 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p3.setLayout(gbl1);
		//p3.setBorder(Line1);
		p3.setBackground(Color.white);
		p3.setPreferredSize(new Dimension(450, 380));
		
		// Создаем панель для результата ответа (South p4)
		Color grey = new Color(221, 221, 221); // grey
		JPanel p4 = new JPanel();
		FlowLayout fl1 = new FlowLayout();
		p4.setLayout(fl1);
		p4.setBackground(grey);
		p4.setPreferredSize(new Dimension(800, 70));
		/*----------------------------------------------------------------------------*/
		
		// ------------------------------Добавляем елементы на панель p1
		// Button back
		Box box = Box.createHorizontalBox();
		JButton back = new JButton(new ImageIcon("res/images/back.png"));
		back.setToolTipText("Вернуться обратно к тренировкам");
		back.setBorder(null);
		back.setBackground(null);
		back.setForeground(Color.white);
		back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MainWindow m = new MainWindow();
					TakeProgresWords s = new TakeProgresWords();
					if (map.get(1)!="test"){
						s.ChangeProgressWords(training, map, map1);	
					}
					if(item==null){
						m.showTrainingPanel(item1);
						m.createAndShowGUI();
					}else{
						m.showTrainingPanelAll(item,item1);
						m.createAndShowGUI();
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}});
		box.add(back);
		box.add(Box.createHorizontalStrut(150));
		p1.add(box);
		
		JLabel nametraining = new JLabel("Слово-перевод");
		nametraining.setFont(fonttraining);
		box.add(nametraining);
		box.add(Box.createHorizontalStrut(130));
		p1.add(box);
						
		UIManager.put("ProgressBar.selectionForeground", Color.black);
		UIManager.put("ProgressBar.selectionBackground", Color.black);	
		JProgressBar progressBar = new JProgressBar();
		progressBar.setPreferredSize(new Dimension(250, 20));
		progressBar.setStringPainted(true);
		progressBar.setBackground(Color.white);
		progressBar.setForeground(Color.green);
		progressBar.setMinimum(0);
		progressBar.setMaximum(100);
		progressBar.setValue(prog);
		box.add(progressBar);
		p1.add(box);
				
		// ------------------------------Добавляем елементы на панель p2
		GridBagConstraints c2 = new GridBagConstraints();
				
		//Word in english
		JLabel word = new JLabel(words[k][0]);
		c2.anchor = GridBagConstraints.CENTER;
		c2.gridx = 0;
		c2.gridy = 0;
		c2.insets = new Insets(0, 0, 0, 0);
		word.setFont(fontword);
		p2.add(word, c2);
			
		// Audio for words
		Box box2 = Box.createHorizontalBox();
		JButton audio = new JButton(new ImageIcon("res/images/9.png"));
		audio.setText("");
		audio.setBorder(null);
		audio.setBackground(null);
		audio.setForeground(Color.white);
		audio.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3();
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}});
		c2.insets = new Insets(0, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 1;
		box2.add(audio);
		p2.add(box2, c2);
		box2.add(Box.createHorizontalStrut(8));
		
		// транскрипцыя слова
		JLabel trans = new JLabel(words[k][2]);
		trans.setForeground(Color.BLUE);
		c2.anchor = GridBagConstraints.CENTER;
		c2.gridx = 0;
		c2.gridy = 2;
		c2.insets = new Insets(10, 0, 0, 0);
		trans.setFont(fonttrans);
		box2.add(trans);
		p2.add(box2, c2);
		
		// Ассоциативная картинка
		final JLabel image = new JLabel();
		image.setPreferredSize(new Dimension(300, 200));
		c2.insets = new Insets(20, 0, 30, 0);
		c2.gridx = 0;
		c2.gridy = 3;
		p2.add(image, c2);
		
		// ------------------------------Добавляем елементы на панель p3
			
		GridBagConstraints c3 = new GridBagConstraints();
		Font fontbuttons1 = new Font("Arial", Font.PLAIN, 14);
		Color Color1 = new Color(75, 142, 197); // btn
		final Color red = new Color(181,64,40); 
		final Color green = new Color(86,141,70); 
				
		int rnd1 = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
		int rnd2 = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
		int rnd3 = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
		int rnd4 = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
						
		temp[0]= randwords[rnd1][1];
		rightwords[0] = new String []{randwords[rnd1][0], randwords[rnd1][1]};
				
		if(randwords[rnd2][1]!=temp[0]&&randwords[rnd2][1]!=words[k][1]){
			temp[1]= randwords[rnd2][1];
			rightwords[1] = new String []{randwords[rnd2][0], randwords[rnd2][1]};
		} else {
			int rnd = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
			temp[1]= randwords[rnd][1];
		}
		
		if(randwords[rnd2][1]!=temp[0]&&randwords[rnd3][1]!=temp[1]&&randwords[rnd3][1]!=words[k][1]){
			rightwords[2] = new String []{randwords[rnd3][0], randwords[rnd3][1]};
			temp[2]= randwords[rnd3][1];
		} else {
			int rnd = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
			temp[2]= randwords[rnd][1];
		}
	
		if(randwords[rnd2][1]!=temp[0]&&randwords[rnd3][1]!=temp[1]&&randwords[rnd4][1]!=temp[2]&&randwords[rnd4][1]!=words[k][1]){
			rightwords[3] = new String []{randwords[rnd4][0], randwords[rnd4][1]};
			temp[3]= randwords[rnd4][1];
		} else {
			int rnd = 0 + (int)(Math.random() * ((randwords.length - 2) + 1));
			temp[3]= randwords[rnd][1];
		}
		
		temp[4]= words[k][1];
				
		shuffleArray(temp);
		
		final String tmp1=temp[0];
		final String tmp2=temp[1];
		final String tmp3=temp[2];
		final String tmp4=temp[3];
		final String tmp5=temp[4];
		
		// Прогрес изученого слова
		String prog_pass1 ="res/images/progress/"+words[k][6]+".png";
		final JLabel pr = new JLabel(new ImageIcon(prog_pass1));
		if(words[k][6].equalsIgnoreCase("0")){
			pr.setToolTipText("Прогресс слова: 0%.");
			prog1=1;
		}else if(words[k][6].equalsIgnoreCase("1")){
			pr.setToolTipText("Прогресс слова: 20%.");
			prog1=2;
		}else if(words[k][6].equalsIgnoreCase("2")){
			pr.setToolTipText("Прогресс слова: 40%.");
			prog1=3;
		}else if(words[k][6].equalsIgnoreCase("3")){
			pr.setToolTipText("Прогресс слова: 60%.");
			prog1=4;
		}else if(words[k][6].equalsIgnoreCase("4")){
			pr.setToolTipText("Прогресс слова: 80%.");
			prog1=5;
		}else if(words[k][6].equalsIgnoreCase("5")){
			pr.setToolTipText("Прогресс слова: 100%.");
			prog1=6;
		}
		pr.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr.setVisible(true);
	
		String prog_pass11 ="res/images/progress/"+prog1+".png";
		final JLabel pr1 = new JLabel(new ImageIcon(prog_pass11));
		if(prog1==1){
			pr1.setToolTipText("Прогресс слова: 20%.");
		}else if(prog1==2){
			pr1.setToolTipText("Прогресс слова: 40%.");
		}else if(prog1==3){
			pr1.setToolTipText("Прогресс слова: 60%.");
		}else if(prog1==4){
			pr1.setToolTipText("Прогресс слова: 80%.");
		}else if(prog1==5){
			pr1.setToolTipText("Прогресс слова: 100%.");
		}    
		pr1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr1.setVisible(false);
		
		Box box1 = Box.createHorizontalBox();
		box1.add(Box.createHorizontalStrut(320));
		//box1.setBorder(Line1);
		box1.add(pr);
		box1.add(pr1);
		c3.insets = new Insets(0, 0, 15, 0);
		c3.gridx = 0;
		c3.gridy = 0;
		p3.add(box1, c3);
		
		var1 = new JButton(tmp1);
		var1.setPreferredSize(new Dimension(350, 40));
		var1.setFont(fontbuttons);
		var1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		var1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			if (action==0){
				image.setIcon(new ImageIcon(words[k][4]));
				action++;
				if (words[k][1].equalsIgnoreCase(tmp1)){
					var1.setBackground(green);
					var1.setForeground(Color.white);
					pr.setVisible(false);
					pr1.setVisible(true);
					unknown.setVisible(false);
					further.setVisible(true);
					right_answer.setVisible(true);
				}else{
					error++;
					var1.setBackground(red);
					var1.setForeground(Color.white);
					unknown.setVisible(false);
					further.setVisible(true);
					wrong_answer.setVisible(true);
					answer.setVisible(true);
					for(int i=0; i<4; i++){
						if(tmp1.equalsIgnoreCase(rightwords[i][1])){
							answer.setText(rightwords[i][1]+" - "+rightwords[i][0]);
						}
					}
														
					if(words[k][1].equalsIgnoreCase(tmp2)){
						var2.setBackground(green);
						var2.setForeground(Color.white);
					}else if (words[k][1].equalsIgnoreCase(tmp3)){
						var3.setBackground(green);
						var3.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp4)){
						var4.setBackground(green);
						var4.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp5)){
						var5.setBackground(green);
						var5.setForeground(Color.white);
					}
				}
					try {
						audioMP3();
					} catch (Throwable e1) {
					// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}else{
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}});
		c3.insets = new Insets(0, 0, 10, 0);
		c3.gridx = 0;
		c3.gridy = 1;
		p3.add(var1,c3);
		
		var2 = new JButton(tmp2);
		var2.setPreferredSize(new Dimension(350, 40));
		var2.setFont(fontbuttons);
		var2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		var2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			if (action==0){
				image.setIcon(new ImageIcon(words[k][4]));
				action++;
				if (words[k][1].equalsIgnoreCase(tmp2)){
					var2.setBackground(green);
					var2.setForeground(Color.white);
					pr.setVisible(false);
					pr1.setVisible(true);
					unknown.setVisible(false);
					further.setVisible(true);
					right_answer.setVisible(true);
				}else{
					error++;
					var2.setBackground(red);
					var2.setForeground(Color.white);
					unknown.setVisible(false);
					further.setVisible(true);
					wrong_answer.setVisible(true);
					answer.setVisible(true);
					for(int i=0; i<4; i++){
						if(tmp2.equalsIgnoreCase(rightwords[i][1])){
							answer.setText(rightwords[i][1]+" - "+rightwords[i][0]);
						}
					}
						
					if(words[k][1].equalsIgnoreCase(tmp1)){
						var1.setBackground(green);
						var1.setForeground(Color.white);
					}else if (words[k][1].equalsIgnoreCase(tmp3)){
						var3.setBackground(green);
						var3.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp4)){
						var4.setBackground(green);
						var4.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp5)){
						var5.setBackground(green);
						var5.setForeground(Color.white);
					}
				}
					try {
						audioMP3();
					} catch (Throwable e1) {
					// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}else{
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}});
		c3.insets = new Insets(0, 0, 10, 0);
		c3.gridx = 0;
		c3.gridy = 2;
		p3.add(var2, c3);
		
		var3 = new JButton(tmp3);
		var3.setPreferredSize(new Dimension(350, 40));
		var3.setFont(fontbuttons);
		var3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		var3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			if (action==0){
				image.setIcon(new ImageIcon(words[k][4]));
				action++;
				if (words[k][1].equalsIgnoreCase(tmp3)){
					var3.setBackground(green);
					var3.setForeground(Color.white);
					pr.setVisible(false);
					pr1.setVisible(true);
					unknown.setVisible(false);
					further.setVisible(true);
					right_answer.setVisible(true);
				}else{
					error++;
					var3.setBackground(red);
					var3.setForeground(Color.white);
					unknown.setVisible(false);
					further.setVisible(true);
					wrong_answer.setVisible(true);
					answer.setVisible(true);
					for(int i=0; i<4; i++){
						if(tmp3.equalsIgnoreCase(rightwords[i][1])){
							answer.setText(rightwords[i][1]+" - "+rightwords[i][0]);
						}
					}
						
					if(words[k][1].equalsIgnoreCase(tmp1)){
						var1.setBackground(green);
						var1.setForeground(Color.white);
					}else if (words[k][1].equalsIgnoreCase(tmp2)){
						var2.setBackground(green);
						var2.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp4)){
						var4.setBackground(green);
						var4.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp5)){
						var5.setBackground(green);
						var5.setForeground(Color.white);
					}
				}
					try {
						audioMP3();
					} catch (Throwable e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}else{
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}});
		c3.insets = new Insets(0, 0, 10, 0);
		c3.gridx = 0;
		c3.gridy = 3;
		p3.add(var3, c3);
		
		var4 = new JButton(tmp4);
		var4.setPreferredSize(new Dimension(350, 40));
		var4.setFont(fontbuttons);
		var4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		var4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			if (action==0){
				image.setIcon(new ImageIcon(words[k][4]));
				action++;
				if (words[k][1].equalsIgnoreCase(tmp4)){
					var4.setBackground(green);
					var4.setForeground(Color.white);
					pr.setVisible(false);
					pr1.setVisible(true);
					unknown.setVisible(false);
					further.setVisible(true);
					right_answer.setVisible(true);
				}else{
					error++;
					var4.setBackground(red);
					var4.setForeground(Color.white);
					unknown.setVisible(false);
					further.setVisible(true);
					wrong_answer.setVisible(true);
					answer.setVisible(true);
					for(int i=0; i<4; i++){
						if(tmp4.equalsIgnoreCase(rightwords[i][1])){
							answer.setText(rightwords[i][1]+" - "+rightwords[i][0]);
						}
					}
						
					if(words[k][1].equalsIgnoreCase(tmp1)){
						var1.setBackground(green);
						var1.setForeground(Color.white);
					}else if (words[k][1].equalsIgnoreCase(tmp2)){
						var2.setBackground(green);
						var2.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp3)){
						var3.setBackground(green);
						var3.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp5)){
						var5.setBackground(green);
						var5.setForeground(Color.white);
					}
				}
					try {
						audioMP3();
					} catch (Throwable e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}else{
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}});
		c3.insets = new Insets(0, 0, 10, 0);
		c3.gridx = 0;
		c3.gridy = 4;
		p3.add(var4, c3);
		
		var5 = new JButton(tmp5);
		var5.setPreferredSize(new Dimension(350, 40));
		var5.setFont(fontbuttons);
		var5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		var5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			if (action==0){
				image.setIcon(new ImageIcon(words[k][4]));
				action++;
				if (words[k][1].equalsIgnoreCase(tmp5)){
					var5.setBackground(green);
					var5.setForeground(Color.white);
					pr.setVisible(false);
					pr1.setVisible(true);
					unknown.setVisible(false);
					further.setVisible(true);
					right_answer.setVisible(true);
				}else{
					error++;
					var5.setBackground(red);
					var5.setForeground(Color.white);
					unknown.setVisible(false);
					further.setVisible(true);
					wrong_answer.setVisible(true);
					answer.setVisible(true);
					for(int i=0; i<4; i++){
						if(tmp5.equalsIgnoreCase(rightwords[i][1])){
							answer.setText(rightwords[i][1]+" - "+rightwords[i][0]);
						}
					}
						
					if(words[k][1].equalsIgnoreCase(tmp1)){
						var1.setBackground(green);
						var1.setForeground(Color.white);
					}else if (words[k][1].equalsIgnoreCase(tmp2)){
						var2.setBackground(green);
						var2.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp3)){
						var3.setBackground(green);
						var3.setForeground(Color.white);
					}else if(words[k][1].equalsIgnoreCase(tmp4)){
						var4.setBackground(green);
						var4.setForeground(Color.white);
					}
				}
					try {
						audioMP3();
					} catch (Throwable e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}else{
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}});
		c3.insets = new Insets(0, 0, 10, 0);
		c3.gridx = 0;
		c3.gridy = 5;
		p3.add(var5, c3);
		
		unknown = new JButton("Не знаю");
		unknown.setPreferredSize(new Dimension(100, 35));
		unknown.setBackground(Color1);
		unknown.setForeground(Color.white);
		unknown.setFont(fontbuttons1);
		unknown.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		unknown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				unknown.setVisible(false);
				further.setVisible(true);
				error++;
				action++;
				image.setIcon(new ImageIcon(words[k][4]));
				if(words[k][1].equalsIgnoreCase(tmp1)){
					var1.setBackground(green);
					var1.setForeground(Color.white);
				}else if (words[k][1].equalsIgnoreCase(tmp2)){
					var2.setBackground(green);
					var2.setForeground(Color.white);
				}else if(words[k][1].equalsIgnoreCase(tmp3)){
					var3.setBackground(green);
					var3.setForeground(Color.white);
				}else if(words[k][1].equalsIgnoreCase(tmp4)){
					var4.setBackground(green);
					var4.setForeground(Color.white);
				}else if(words[k][1].equalsIgnoreCase(tmp5)){
					var5.setBackground(green);
					var5.setForeground(Color.white);
				}
			}
		});
		c3.insets = new Insets(20, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 6;
		p3.add(unknown, c3);

		further = new JButton("Дальше");
		further.setPreferredSize(new Dimension(100, 35));
		further.setBackground(Color1);
		further.setForeground(Color.white);
		further.setFont(fontbuttons1);
		further.setVisible(false);
		further.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		further.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		c3.insets = new Insets(20, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 7;
		p3.add(further, c3);
		
		// ------------------------------Добавляем елементы на панель p4
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		
		Box b = Box.createHorizontalBox();
		b.setPreferredSize(new Dimension(300, 60));
		//b.setBorder(Line1);
		
		Box b1 = Box.createHorizontalBox();
		b1.setPreferredSize(new Dimension(400, 60));
		//b1.setBorder(Line1);
		
		right_answer = new JLabel("Верный ответ");
		right_answer.setFont(fontbuttons);
		right_answer.setForeground(green);
		right_answer.setVisible(false);
		b.add(Box.createHorizontalStrut(70));
		b.add(right_answer);
				
		wrong_answer = new JLabel("Неверный ответ");
		wrong_answer.setFont(fontbuttons);
		wrong_answer.setForeground(red);
		wrong_answer.setVisible(false);
		b.add(wrong_answer);
				
		answer = new JLabel();
		answer.setFont(fontbuttons);
		answer.setVisible(false);
		b1.add(answer);
		
		p4.add(Box.createHorizontalStrut(20));
		p4.add(b);
		p4.add(Box.createHorizontalStrut(55));
		p4.add(b1);
		
		add("North", p1);
		add("West", p2);
		add("East", p3);
		add("South", p4);
	}
	
	//Кнопка Дальше + записываем результат
	private void actionClickNextButton() throws Exception {
		
			MainWindow m1 = new MainWindow();
			if (k==1){
				x1=prog; //Первое значения прогреса 
				x=prog+x1; 
			} else{
				x=prog+x1; // Добавляем к прогресу который уже есть первое значения
			}
			
			int d = words.length;
			//System.out.println(k+"   >>>");	
			if (error==0 && k==d){
				map.put(k, words[k-1][5]);
				map1.put(k, 1);
				m1.showResultPanel(words, map, map1,item,item1,training);
				m1.createAndShowGUI();
			} else if (error!=0 && k==d){
				map.put(k, words[k-1][5]);
				map1.put(k, 0);
				m1.showResultPanel(words, map, map1,item,item1,training);
				m1.createAndShowGUI();
			 
			}else if (error==0 && k<12){
				map.put(k, words[k-1][5]);
				map1.put(k, 1);
				removeAll();
				add(new WordsTranslate(k, words, randwords,item,item1,training,x,x1,map,map1));
				validate();
				repaint();
			} else if (error!=0 && k<12){
				map.put(k, words[k-1][5]);
				map1.put(k, 0);
				removeAll();
				add(new WordsTranslate(k, words, randwords,item,item1,training,x,x1,map,map1));
				validate();
				repaint();
				
			} else if (error==0 && k>=12){
				map.put(k, words[k-1][5]);
				map1.put(k, 1);
				m1.showResultPanel(words, map, map1,item,item1,training);
				m1.createAndShowGUI();
			} else if (error!=0 && k>=12){
				map.put(k, words[k-1][5]);
				map1.put(k, 0);
				m1.showResultPanel(words, map, map1,item,item1,training);
				m1.createAndShowGUI();
			}
		}
	
	// Вызов озвучки к слову mp3
	public void audioMP3() throws Throwable {
			
		FileInputStream fis = new FileInputStream(words[k][3]);
		final Player playMP3 = new Player(fis);
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					playMP3.play();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}});
	}
	
	// Алгоритм перемешивания рандомно
	public static String shuffleArray(String [] tmp) {
		
		int n = tmp.length;
		Random random = new Random();
		random.nextInt();
		for (int i = 0; i < n; i++) {
			int change = i + random.nextInt(n - i);
			swap(tmp, i, change);
		}
		return null;
	}

	private static void swap(String[] tmp, int i, int change) {
		// TODO Auto-generated method stub
		String temp = tmp[i];
		tmp[i] = tmp[change];
		tmp[change] = temp;
	}	
}
