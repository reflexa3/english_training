package training;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import windows.MainWindow;
import data.TakeProgresWords;
import javazoom.jl.player.Player;

public class Result_of_training extends JPanel {

	private static final long serialVersionUID = 7996162374191759838L;
	Map<Integer, String> map = new HashMap<Integer, String>();
	Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
	Map<Integer, String> map2 = new HashMap<Integer, String>(); // Картинка
	Map<Integer, String> map3 = new HashMap<Integer, String>(); // Слово
	Map<Integer, String> map4 = new HashMap<Integer, String>(); // Перевод
	String[][] words;
	Boolean [] visible = new Boolean[12];      
	JLabel text1,text1_1,text2,text21,text3,text31,text4,text41,text5,text51,text6,text61,text7,text71,text8,text81;	
	JLabel text9,text91,text10,text101,text11,text111,text12,text121;
	TakeProgresWords s = new TakeProgresWords();
	String item,item1, training;
	int count=0;// the number of correct words
	
	public Result_of_training(String [][] data, Map<Integer, String> a, Map<Integer, Integer> b, String c, String d,  String e){
		
		words=data;
		map=a;
		map1=b;
		item=c;
		item1=d;
		training=e;
		int dl=words.length;
		//System.out.println(item);				
			for(int i=0; i<=11; i++){
			if (i<dl){
				visible[i]=true;
				map2.put(i+1,words[i][4]);
				map3.put(i+1, words[i][0]);
				map4.put(i+1, words[i][1]);
			}else{
				visible[i]=false;
				map1.put(i+1, 0);
				map2.put(i+1,words[0][4]);
				map3.put(i+1, words[0][0]);
				map4.put(i+1, words[0][1]);
				}
		}
								
		setLayout(new FlowLayout(0, 30, 0));
		//setOpaque(false);
		setPreferredSize(new Dimension(1320, 605));
		
		Font fonttext = new Font("Arial", Font.PLAIN, 24);
		
		Box box1 = Box.createHorizontalBox();
		box1.setPreferredSize(new Dimension(1320, 20));
				
		Box box2 = Box.createHorizontalBox();
		box2.setPreferredSize(new Dimension(1320, 20));
				
		Box box3 = Box.createHorizontalBox();
		box3.setPreferredSize(new Dimension(1320, 50));
		
		// ------------------------------------------------Панель 1
		JPanel p1 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p1.setLayout(gbl1);
		p1.setVisible(visible[0]);
		p1.setPreferredSize(new Dimension(300, 165));
		if (map1.get(1)==1){
			count++;
			p1.setBackground(Color.green);
		}else{
			p1.setBackground(Color.red);
		}
		p1.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text1.setVisible(true);
				text1_1.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text1.setVisible(false);
				text1_1.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(0);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р1 
		GridBagConstraints c1 = new GridBagConstraints();
							
		JLabel image1 = new JLabel(new ImageIcon(map2.get(1)));
		image1.setPreferredSize(new Dimension(300, 135));
		c1.gridx = 0;
		c1.gridy = 0;
		p1.add(image1, c1);
							
		text1 = new JLabel(map3.get(1));
		text1.setFont(fonttext);
		c1.gridx = 0;
		c1.gridy = 1;
		p1.add(text1, c1);
		
		text1_1 = new JLabel(map4.get(1));
		text1_1.setFont(fonttext);
		text1_1.setVisible(false);
		c1.gridx = 0;
		c1.gridy = 1;
		p1.add(text1_1, c1);
		
		// ------------------------------------------------Панель 2
		JPanel p2 = new JPanel();
		GridBagLayout gbl2 = new GridBagLayout();
		p2.setLayout(gbl2);
		p2.setVisible(visible[1]);
		p2.setPreferredSize(new Dimension(300, 165));
		if (map1.get(2)==1){
			count++;
			p2.setBackground(Color.green);
		}else{
			p2.setBackground(Color.red);
		}
		p2.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text2.setVisible(true);
				text21.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text2.setVisible(false);
				text21.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(1);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р2 
		GridBagConstraints c2 = new GridBagConstraints();
							
		JLabel image2 = new JLabel(new ImageIcon(map2.get(2))); 
		image2.setPreferredSize(new Dimension(300, 135));
		c2.gridx = 0;
		c2.gridy = 0;
		p2.add(image2, c2);
							
		text2 = new JLabel(map3.get(2));
		text2.setFont(fonttext);
		c2.gridx = 0;
		c2.gridy = 1;
		p2.add(text2, c2);
		
		text21 = new JLabel(map4.get(2));
		text21.setFont(fonttext);
		text21.setVisible(false);
		c2.gridx = 0;
		c2.gridy = 1;
		p2.add(text21, c2);
		
		// ------------------------------------------------Панель 3
		JPanel p3 = new JPanel();
		GridBagLayout gbl3 = new GridBagLayout();
		p3.setLayout(gbl3);
		p3.setVisible(visible[2]);
		p3.setPreferredSize(new Dimension(300, 165));
		if (map1.get(3)==1){
			count++;
			p3.setBackground(Color.green);
		}else{
			p3.setBackground(Color.red);
		}
		p3.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text3.setVisible(true);
				text31.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text3.setVisible(false);
				text31.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(2);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р3 
		GridBagConstraints c3 = new GridBagConstraints();
							
		JLabel image3 = new JLabel(new ImageIcon(map2.get(3))); 
		image3.setPreferredSize(new Dimension(300, 135));
		c3.gridx = 0;
		c3.gridy = 0;
		p3.add(image3, c3);
							
		text3 = new JLabel(map3.get(3));
		text3.setFont(fonttext);
		c3.gridx = 0;
		c3.gridy = 1;
		p3.add(text3, c3);
		
		text31 = new JLabel(map4.get(3));
		text31.setFont(fonttext);
		text31.setVisible(false);
		c3.gridx = 0;
		c3.gridy = 1;
		p3.add(text31, c3);
		
		// ------------------------------------------------Панель 4
		JPanel p4 = new JPanel();
		GridBagLayout gbl4 = new GridBagLayout();
		p4.setLayout(gbl4);
		p4.setVisible(visible[3]);
		p4.setPreferredSize(new Dimension(300, 165));
		if (map1.get(4)==1){
			count++;
			p4.setBackground(Color.green);
		}else{
			p4.setBackground(Color.red);
		}
		p4.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text4.setVisible(true);
				text41.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text4.setVisible(false);
				text41.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(3);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р4 
		GridBagConstraints c4 = new GridBagConstraints();
							
		JLabel image4 = new JLabel(new ImageIcon(map2.get(4))); 
		image4.setPreferredSize(new Dimension(300, 135));
		c4.gridx = 0;
		c4.gridy = 0;
		p4.add(image4, c4);
							
		text4 = new JLabel(map3.get(4));
		text4.setFont(fonttext);
		c4.gridx = 0;
		c4.gridy = 1;
		p4.add(text4, c4);
		
		text41 = new JLabel(map4.get(4));
		text41.setFont(fonttext);
		text41.setVisible(false);
		c4.gridx = 0;
		c4.gridy = 1;
		p4.add(text41, c4);
		
		// ------------------------------------------------Панель 5
		JPanel p5 = new JPanel();
		GridBagLayout gbl5 = new GridBagLayout();
		p5.setLayout(gbl5);
		p5.setVisible(visible[4]);
		p5.setPreferredSize(new Dimension(300, 165));
		if (map1.get(5)==1){
			count++;
			p5.setBackground(Color.green);
		}else{
			p5.setBackground(Color.red);
		}
		p5.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text5.setVisible(true);
				text51.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text5.setVisible(false);
				text51.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(4);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р5 
		GridBagConstraints c5 = new GridBagConstraints();
							
		
		JLabel image5 = new JLabel(new ImageIcon(map2.get(5)));
		image5.setPreferredSize(new Dimension(300, 135));
		c5.gridx = 0;
		c5.gridy = 0;
		p5.add(image5, c5);
							
		text5 = new JLabel(map3.get(5));
		text5.setFont(fonttext);
		c5.gridx = 0;
		c5.gridy = 1;
		p5.add(text5, c5);
		
		text51 = new JLabel(map4.get(5));
		text51.setFont(fonttext);
		text51.setVisible(false);
		c5.gridx = 0;
		c5.gridy = 1;
		p5.add(text51, c1);
		
		// ------------------------------------------------Панель 6
		JPanel p6 = new JPanel();
		GridBagLayout gbl6 = new GridBagLayout();
		p6.setLayout(gbl6);
		p6.setVisible(visible[5]);
		p6.setPreferredSize(new Dimension(300, 165));
		if (map1.get(6)==1){
			count++;
			p6.setBackground(Color.green);
		}else{
			p6.setBackground(Color.red);
		}
		p6.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text6.setVisible(true);
				text61.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text6.setVisible(false);
				text61.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(5);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р6 
		GridBagConstraints c6 = new GridBagConstraints();
							
		JLabel image6 = new JLabel(new ImageIcon(map2.get(6))); 
		image6.setPreferredSize(new Dimension(300, 135));
		c6.gridx = 0;
		c6.gridy = 0;
		p6.add(image6, c6);
							
		text6 = new JLabel(map3.get(6));
		text6.setFont(fonttext);
		c6.gridx = 0;
		c6.gridy = 1;
		p6.add(text6, c6);
		
		text61 = new JLabel(map4.get(6));
		text61.setFont(fonttext);
		text61.setVisible(false);
		c6.gridx = 0;
		c6.gridy = 1;
		p6.add(text61, c6);
		
		// ------------------------------------------------Панель 7
		JPanel p7 = new JPanel();
		GridBagLayout gbl7 = new GridBagLayout();
		p7.setLayout(gbl7);
		p7.setVisible(visible[6]);
		p7.setPreferredSize(new Dimension(300, 165));
		if (map1.get(7)==1){
			count++;
			p7.setBackground(Color.green);
		}else{
			p7.setBackground(Color.red);
		}
		p7.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text7.setVisible(true);
				text71.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text7.setVisible(false);
				text71.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(6);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р7 
		GridBagConstraints c7 = new GridBagConstraints();
							
		JLabel image7 = new JLabel(new ImageIcon(map2.get(7))); 
		image7.setPreferredSize(new Dimension(300, 135));
		c7.gridx = 0;
		c7.gridy = 0;
		p7.add(image7, c7);
							
		text7 = new JLabel(map3.get(7));
		text7.setFont(fonttext);
		c7.gridx = 0;
		c7.gridy = 1;
		p7.add(text7, c7);
		
		text71 = new JLabel(map4.get(7));
		text71.setFont(fonttext);
		text71.setVisible(false);
		c7.gridx = 0;
		c7.gridy = 1;
		p7.add(text71, c7);
		
		// ------------------------------------------------Панель 8
		JPanel p8 = new JPanel();
		GridBagLayout gbl8 = new GridBagLayout();
		p8.setLayout(gbl8);
		p8.setVisible(visible[7]);
		p8.setPreferredSize(new Dimension(300, 165));
		if (map1.get(8)==1){
			count++;
			p8.setBackground(Color.green);
		}else{
			p8.setBackground(Color.red);
		}
		p8.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text8.setVisible(true);
				text81.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text8.setVisible(false);
				text81.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(7);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р8 
		GridBagConstraints c8 = new GridBagConstraints();
							
		JLabel image8 = new JLabel(new ImageIcon(map2.get(8))); 
		image8.setPreferredSize(new Dimension(300, 135));
		c8.gridx = 0;
		c8.gridy = 0;
		p8.add(image8, c8);
							
		text8 = new JLabel(map3.get(8));
		text8.setFont(fonttext);
		c8.gridx = 0;
		c8.gridy = 1;
		p8.add(text8, c8);
		
		text81 = new JLabel(map4.get(8));
		text81.setFont(fonttext);
		text81.setVisible(false);
		c8.gridx = 0;
		c8.gridy = 1;
		p8.add(text81, c8);
		
		// ------------------------------------------------Панель 9
		JPanel p9 = new JPanel();
		GridBagLayout gbl9 = new GridBagLayout();
		p9.setLayout(gbl9);
		p9.setVisible(visible[8]);
		p9.setPreferredSize(new Dimension(300, 165));
		if (map1.get(9)==1){
			count++;
			p9.setBackground(Color.green);
		}else{
			p9.setBackground(Color.red);
		}
		p9.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text9.setVisible(true);
				text91.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text9.setVisible(false);
				text91.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(8);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р9 
		GridBagConstraints c9 = new GridBagConstraints();
							
		JLabel image9 = new JLabel(new ImageIcon(map2.get(9)));
		image9.setPreferredSize(new Dimension(300, 135));
		c9.gridx = 0;
		c9.gridy = 0;
		p9.add(image9, c9);
							
		text9 = new JLabel(map3.get(9));
		text9.setFont(fonttext);
		c9.gridx = 0;
		c9.gridy = 1;
		p9.add(text9, c9);
		
		text91 = new JLabel(map4.get(9));
		text91.setFont(fonttext);
		text91.setVisible(false);
		c9.gridx = 0;
		c9.gridy = 1;
		p9.add(text91, c9);
		
		// ------------------------------------------------Панель 10
		JPanel p10 = new JPanel();
		GridBagLayout gbl10 = new GridBagLayout();
		p10.setLayout(gbl10);
		p10.setVisible(visible[9]);
		p10.setPreferredSize(new Dimension(300, 165));
		if (map1.get(10)==1){
			count++;
			p10.setBackground(Color.green);
		}else{
			p10.setBackground(Color.red);
		}
		p10.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text10.setVisible(true);
				text101.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text10.setVisible(false);
				text101.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(9);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р10 
		GridBagConstraints c10 = new GridBagConstraints();
							
		JLabel image10 = new JLabel(new ImageIcon(map2.get(10))); 
		image10.setPreferredSize(new Dimension(300, 135));
		c10.gridx = 0;
		c10.gridy = 0;
		p10.add(image10, c10);
							
		text10 = new JLabel(map3.get(10));
		text10.setFont(fonttext);
		c10.gridx = 0;
		c10.gridy = 1;
		p10.add(text10, c10);
		
		text101 = new JLabel(map4.get(10));
		text101.setFont(fonttext);
		text101.setVisible(false);
		c10.gridx = 0;
		c10.gridy = 1;
		p10.add(text101, c10);
		
		// ------------------------------------------------Панель 11
		JPanel p11 = new JPanel();
		GridBagLayout gbl11 = new GridBagLayout();
		p11.setLayout(gbl11);
		p11.setVisible(visible[10]);
		p11.setPreferredSize(new Dimension(300, 165));
		if (map1.get(11)==1){
			count++;
			p11.setBackground(Color.green);
		}else{
			p11.setBackground(Color.red);
		}
		p11.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text11.setVisible(true);
				text111.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text11.setVisible(false);
				text111.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(10);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р11 
		GridBagConstraints c11 = new GridBagConstraints();
							
		JLabel image11 = new JLabel(new ImageIcon(map2.get(11))); 
		image11.setPreferredSize(new Dimension(300, 135));
		c11.gridx = 0;
		c11.gridy = 0;
		p11.add(image11, c11);
							
		text11 = new JLabel(map3.get(11));
		text11.setFont(fonttext);
		c11.gridx = 0;
		c11.gridy = 1;
		p11.add(text11, c11);
		
		text111 = new JLabel(map4.get(11));
		text111.setFont(fonttext);
		text111.setVisible(false);
		c11.gridx = 0;
		c11.gridy = 1;
		p11.add(text111, c11);
		
		// ------------------------------------------------Панель 12
		JPanel p12 = new JPanel();
		GridBagLayout gbl12 = new GridBagLayout();
		p12.setLayout(gbl12);
		p12.setVisible(visible[11]);
		p12.setPreferredSize(new Dimension(300, 165));
		if (map1.get(12)==1){
			count++;
			p12.setBackground(Color.green);
		}else{
			p12.setBackground(Color.red);
		}
		p12.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {
				text12.setVisible(true);
				text121.setVisible(false);
			}
			public void mouseEntered(MouseEvent e) {
				text12.setVisible(false);
				text121.setVisible(true);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3(11);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}});
		p12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							
		// Добавляем елементы на панель р12 
		GridBagConstraints c12 = new GridBagConstraints();
							
		JLabel image12 = new JLabel(new ImageIcon(map2.get(12))); 
		image12.setPreferredSize(new Dimension(300, 135));
		c12.gridx = 0;
		c12.gridy = 0;
		p12.add(image12, c12);
							
		text12 = new JLabel(map3.get(12));
		text12.setFont(fonttext);
		c12.gridx = 0;
		c12.gridy = 1;
		p12.add(text12, c12);
		
		text121 = new JLabel(map4.get(12));
		text121.setFont(fonttext);
		text121.setVisible(false);
		c12.gridx = 0;
		c12.gridy = 1;
		p12.add(text121, c12);
		
		JLabel message = new JLabel(new ImageIcon("res/images/1.gif"));
		String text="Верных ответов: "+count+" из "+ map.size();
		message.setFont(fonttext);
		message.setText(text);
		box3.add(message);
			
		JButton previous = new JButton("Вернуться к тренировкам");
		Color Color1 = new Color(75, 142, 197);
		Font fontbuttons1 = new Font("Arial", Font.PLAIN, 14);
		previous.setPreferredSize(new Dimension(100, 35));
		previous.setFont(fontbuttons1);
		previous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		previous.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					MainWindow m = new MainWindow();
					s.ChangeProgressWords(training, map, map1);
					if(item==null){
						m.showTrainingPanel(item1);
						m.createAndShowGUI();
					}else{
						m.showTrainingPanelAll(item,item1);
						m.createAndShowGUI();
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		box3.add(Box.createHorizontalStrut(30));
		box3.add(previous);
		
		JButton nextset = new JButton("Продолжить тренировку");
		if (dl<=12 && dl==count){
			nextset.setVisible(false);
		}
		nextset.setPreferredSize(new Dimension(100, 35));
		nextset.setBackground(Color1);
		nextset.setForeground(Color.white);
		nextset.setFont(fontbuttons1);
		nextset.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		nextset.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
		
				try {
					MainWindow m = new MainWindow();
					s.ChangeProgressWords(training, map, map1); 
					
					 if(training=="words_translate"){
						m.showWordsTranslatePanel(item,item1,training);
						
					}else if(training=="translate_words"){
						m.showWordsTranslatePanel(item,item1,training);
						
					}else if(training=="konstructor"){
						m.showWordsKonstructorPanel(item,item1,training);
						
					}else if(training=="words_card"){
						m.showWordsKard(item,item1,training);
					}
					m.createAndShowGUI();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		box3.add(Box.createHorizontalStrut(30));
		box3.add(nextset);
			
		add(p1);
		add(p2);
		add(p3);
		add(p4);
		if (dl>4){
			add(box1);	
		}
		add(p5);
		add(p6);
		add(p7);
		add(p8);
		if (dl>4){
			add(box2);	
		}
		add(p9);
		add(p10);
		add(p11);
		add(p12);
		
		add(box3);
	}

	// Вызов озвучки к слову mp3
	public void audioMP3(int k) throws Throwable {
	
		FileInputStream fis = new FileInputStream(words[k][3]);
		Player playMP3 = new Player(fis);
		playMP3.play();
			
		}
}
