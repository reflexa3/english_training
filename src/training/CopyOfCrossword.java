package training;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import javazoom.jl.player.Player;
import windows.MainWindow;

public class CopyOfCrossword extends JPanel {

	private static final long serialVersionUID = 4826216375579021293L;
	JButton[] numButtons = new JButton[500];
	int q; // счетчик которой идет по буквам в первом слове
	int ok2=0; //позиция пересечения первого и второго слова
	int ok3=0; //позиция пересечени третьего слова
	int ok4=0; //позиция пересечения четвертого слова
	int w; //последняя буква которая совпала во втором слове с первым словом, по этому индексу движемся вниз  
	int w1; //последняя буква которая совпала во втором слове с первым словом, по этому индексу движемся вверх
	int dl1,dl2,dl3,dl4; // длина 1..12
	String s1,s2,s3,s4; //words 1..12 
	int down2,up2,down3,up3,left3,right3,down4,up4,left4,right4;
	String temp3[];
	String temp4[];
	int stop=0;
	String[][] words; 
	Map<Integer, String> steck = new HashMap<Integer, String>();
	
	public CopyOfCrossword(int k, String[][] data, Map<Integer, String> map, Map<Integer, Integer> map1, String item, String item1, int prog, int k1, String training) {
		
		words=data;
		
		for (int k11=0; k11<12; k11++){
			//System.out.println(k11+" "+words[k11][0]+" "+words[k11][1]+" "+words[k11][2]+" "+words[k11][3]+" "+words[k11][4]+" "+words[k11][5]);
			System.out.println(k11+" "+words[k11][0]);
		}
		
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1100, 605));
		Border line = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		
		// Область где будет прогрес тренровки (North p1)
		Color yellow = new Color(250, 194, 90); // yellow
		JPanel p1 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p1.setPreferredSize(new Dimension(1100, 50));
		p1.setLayout(fl);
		p1.setBackground(yellow);
		
		// Область где будет прогрес тренровки (Center p2)
		JPanel p2 = new JPanel();
		GridLayout gl = new GridLayout(20, 20, 0, 0);
		p2.setLayout(gl);
		p2.setBorder(line);
		
		// Область где будет прогрес тренровки (West p3)
		JPanel p3 = new JPanel();
		FlowLayout fl3 = new FlowLayout();
		p3.setPreferredSize(new Dimension(250, 565));
		p3.setLayout(fl3);
		p3.setBorder(line);
				
		Font fontbuttons = new Font("Tahoma", Font.PLAIN, 15);
		Color grey = new Color(221, 221, 221); // grey
		
		// Добавляем пустые кнопки
		for (int i = 0; i<400; i++){
			numButtons[i] = new JButton();
			numButtons[i].setBackground(grey);
			numButtons[i].setFont(fontbuttons);
			p2.add(numButtons[i]);
		}
		//--------------------------------------------------------------------FIRST WORDS--------
		
		s1 = words[0][0];// получаем первое слово и его длину
		dl1 = s1.length();
		
		if (dl1>12 || dl1<7){
			for (int wordslength=1; wordslength<words.length; wordslength++){
				if (dl1>12 || dl1<7){
					System.out.println("ERROR FIRST WORDS (Big length)  "+dl1+" "+words[wordslength][0]);
					s1 = words[wordslength][0];// получаем повторно первое слово и его длину
					dl1 = s1.length();
				}
			}
		}
		
		steck.put(1, s1);
		String temp1[] = new String[dl1];// разбиваем на буквы слово
		temp1 = s1.split("");
								
		q=1;
		for (int i = 148; i<148+dl1; i++){
			if (i==148){
				numButtons[i].setText(temp1[q]);
				numButtons[i].setBackground(Color.green);
			}else{
				numButtons[i].setText(temp1[q]);
				numButtons[i].setBackground(Color.green);
				}q++;
			}			
				
		//-------------------------------------------------------------------SECOND WORDS----------

		for (int wordslength2=0; wordslength2<words.length; wordslength2++){
			if (ok2!=0 && up2>=8 && down2<400){
				System.out.println("STOP");
				steck.put(2, s2);
				break;
			}
			System.out.println("2>> "+words[wordslength2][0]);
			s2 = words[wordslength2][0];// получаем слово и его длину
			
			// проверка первого и второго слова по горизонтали
			if(ok2==0 && s1!=s2 ){
				dl2 = s2.length();
				String temp2[] = new String[dl2];// разбиваем на буквы слово
				temp2 = s2.split("");
				System.out.println(">>Second words (1 vs 2) - "+words[wordslength2][0]);
				for (int i = 1; i<dl2; i++){
					for (int j = 148; j<148+dl1; j++){
						if (ok2!=0){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp2[i])){
							numButtons[j].setBackground(Color.red);
							w=i;
							w1=i;
							ok2=j;
						}
					}
				}
				
				down2=ok2+((dl2-w)*20); // граница вниз по вертикали
				up2=ok2-((w1-1)*20); // граница вверх по вертикали
				if (ok2!=0 && up2<8 || down2>=400){
					ok2=0;
					System.out.println("Выход за пределы границы, Верх граница="+up2+", Низ граница="+down2);
				}
				
				//Проверка границ построения второго слова по вертикали
				prov_vert(up2,down2,ok2,temp2,w,w1);
			}
		}
		
		//-------------------------------------------------------------------THIRD WORDS----------
		stop=0;
		for (int wordslength3=0; wordslength3<words.length; wordslength3++){
			if (ok3!=0 && ok3!=ok2  && ok2+1!=ok3 && ok2-1!=ok3 && stop==1){ 
				System.out.println("STOP");
				steck.put(3, s3);
				break;
			}
			System.out.println("3>> "+words[wordslength3][0]);	
			s3 = words[wordslength3][0];// получаем слово и его длину
			
//			// проверка 1 vs 3 слова по горизонтали
//			if(ok3==0 && s1!=s3 && s2!=s3){
//				dl3 = s3.length();
//				temp3 = new String[dl3];// разбиваем на буквы слово
//				temp3 = s3.split("");
//				System.out.println(">>Third words (1 vs 3) - "+words[wordslength3][0]);
//				for (int i = 1; i<dl3; i++){
//					for (int j = 148; j<148+dl1; j++){
//						if (ok3!=0 && ok3!=ok2 && ok2+1!=ok3 && ok2-1!=ok3){
//							break;
//						}
//						if (numButtons[j].getText().equalsIgnoreCase(temp3[i])){
//							w=i;
//							w1=i;
//							int tmp=j;
//							if (tmp!=ok2 && ok2+1!=tmp && ok2-1!=tmp){
//								ok3=j;
//								numButtons[j].setBackground(Color.pink);
//							}
//						}
//					}
//				}
//				
//				down3=ok3+((dl3-w)*20); // граница вниз по вертикали
//				up3=ok3-((w1-1)*20); // граница вверх по вертикали
//				if (ok3!=0 && up3<8 || down3>=400){
//					ok3=0;
//					System.out.println("Выход за пределы границы, Верх граница="+up3+", Низ граница="+down3);
//				}
//								
//				//Проверка границ построения третьего слова по вертикали
//				prov_vert(up3,down3,ok3,temp3,w,w1);
//			}
			
			// проверка 2 vs 3 слова по вертикали
			if (ok3==0 && s1!=s3 && s2!=s3){
				dl3 = s3.length();
				temp3 = new String[dl3];
				temp3 = s3.split("");
				System.out.println(">>Third words (2 vs 3) - "+words[wordslength3][0]);
				for (int i = 1; i<dl3; i++){
					for (int j = up2; j<=down2;){
						if (ok3!=0 && ok3!=ok2 && ok2+20!=ok3 && ok2-20!=ok3){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp3[i])){
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok2 && ok2+20!=tmp && ok2-20!=tmp){
								ok3=j;
								numButtons[j].setBackground(Color.pink);
							}
						}j=j+20;
					}
				}
				int a_temp3=w-1;
				left3= ok3-a_temp3; // граница влево по горизонтали
				right3= ok3+(dl3-w1); // граница вправо по горизонтали
				
				if (ok3!=0){
					for(int j=0; j<400;){
						if(left3>=j && right3<=j+19){
							stop=1;
							System.out.println("stop="+stop+" j="+j);
							break;
						}
					j=j+20;
					}
				}
				
				if (ok3!=0 && stop==0){
					System.out.println("BIG LENGTH  left3="+left3+" right3="+right3);
					ok3=0;
				}

				//Проверка границ построения третьего слова по горизонтали
				prov_horiz(left3,right3,ok3,temp3,w,w1);
			}
		}
				
		//-------------------------------------------------------------------FOURTH WORDS----------
		stop=0;
		for (int wordslength4=0; wordslength4<words.length; wordslength4++){
			if (ok4!=0 && stop==1){ 
				System.out.println("STOP");
				steck.put(4, s4);
				break;
			}
			System.out.println("4>> "+words[wordslength4][0]);	
			s4 = words[wordslength4][0];
			
			// проверка 1 vs 4 слова по горизонтали
			if(ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
				dl4 = s4.length();
				temp4 = new String[dl4];
				temp4 = s4.split("");
				System.out.println(">>Fourth words (1 vs 4) - "+words[wordslength4][0]);
				for (int i = 1; i<dl4; i++){
					for (int j = 148; j<148+dl1; j++){
						if (ok4!=0 && ok4!=ok2 && ok2+1!=ok4 && ok2-1!=ok4 && ok3!=ok4 && ok3+1!=ok4 && ok3-1!=ok4){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok2 && ok2+1!=tmp && ok2-1!=tmp && tmp!=ok3 && ok3+1!=tmp && ok3-1!=tmp){
								ok4=j;
								numButtons[j].setBackground(Color.yellow);
							}
						}
					}
				}
				
				down4=ok4+((dl4-w)*20); // граница вниз по вертикали
				up4=ok4-((w1-1)*20); // граница вверх по вертикали
				if (ok4!=0 && up4<8 || down4>=400){
					ok4=0;
					System.out.println("Выход за пределы границы, Верх граница="+up4+", Низ граница="+down4);
				}
								
				//Проверка границ построения 4 слова по вертикали
				prov_vert(up4,down4,ok4,temp4,w,w1);
			}
			
			// проверка 2 vs 4 слова по вертикали
			if (ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
				dl4 = s4.length();
				temp4 = new String[dl4];
				temp4 = s4.split("");
				System.out.println(">>Fourth words (2 vs 4) - "+words[wordslength4][0]);
				for (int i = 1; i<dl4; i++){
					
					//System.out.println(i+"   >>>i="+temp4[i]);
					
					for (int j = up2; j<=down2;){
						
						//System.out.println(j+"   >>>j="+numButtons[j].getText());
						
						if (ok4!=0 && ok4!=ok2 && ok2+20!=ok4 && ok2-20!=ok4){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
							//System.out.println("2="+numButtons[j].getText()+"   "+temp4[i]+"   up2="+up2+"   down2="+down2+"   j="+j+"   i="+i);
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok2 && ok2+20!=tmp && ok2-20!=tmp){
								ok4=j;
								numButtons[j].setBackground(Color.yellow);
							}
						}j=j+20;
					}
				}
				int a_temp4=w-1;
				left4= ok4-a_temp4; // граница влево по горизонтали
				right4= ok4+(dl4-w1); // граница вправо по горизонтали
				
				//System.out.println("left4="+left4+" right4="+right4+"  up2="+up2+" down2="+down2+"  ok4="+ok4);
				if (ok4!=0){
					for(int j=0; j<400;){
						if(left4>=j && right4<=j+19){
							stop=1;
							System.out.println("stop="+stop+" j="+j);
							break;
						}
					j=j+20;
					}
				}
				
				
				if (ok4!=0 && stop==0){
					System.out.println("BIG LENGTH  left4="+left4+" right4="+right4);
					ok4=0;
				}

				//Проверка границ построения третьего слова по горизонтали
				prov_horiz(left4,right4,ok4,temp4,w,w1);
			}
			
			// проверка 3 vs 4 слова по вертикали
			if (ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
				dl4 = s4.length();
				temp4 = new String[dl4];
				temp4 = s4.split("");
				System.out.println(">>Fourth words (3 vs 4) - "+words[wordslength4][0]);
				for (int i = 1; i<dl4; i++){
					
					//System.out.println(i+"   >>>i="+temp4[i]);
					
					for (int j = up3; j<=down3;){
						
						//System.out.println(j+"   >>>j="+numButtons[j].getText());
						
						if (ok4!=0 && ok4!=ok3 && ok3+20!=ok4 && ok3-20!=ok4){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
							//System.out.println("2="+numButtons[j].getText()+"   "+temp4[i]+"   up2="+up2+"   down2="+down2+"   j="+j+"   i="+i);
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok3 && ok3+20!=tmp && ok3-20!=tmp){
								ok4=j;
								numButtons[j].setBackground(Color.yellow);
							}
						}j=j+20;
					}
				}
				int a_temp4=w-1;
				left4= ok4-a_temp4; // граница влево по горизонтали
				right4= ok4+(dl4-w1); // граница вправо по горизонтали
				
				//System.out.println("left4="+left4+" right4="+right4+"  up2="+up2+" down2="+down2+"  ok4="+ok4);
				if (ok4!=0){
					for(int j=0; j<400;){
						if(left4>=j && right4<=j+19){
							stop=1;
							System.out.println("stop="+stop+" j="+j);
							break;
						}
					j=j+20;
					}
				}
				
				
				if (ok4!=0 && stop==0){
					System.out.println("BIG LENGTH  left4="+left4+" right4="+right4);
					ok4=0;
				}

				//Проверка границ построения третьего слова по горизонтали
				prov_horiz(left4,right4,ok4,temp4,w,w1);
			}
		}
		
		add("North",p1);
		add("Center",p2);
		add("West",p3);
	}
	
	//Проверка границ построения по горизонтали
	public void prov_horiz(int left, int right, int ok, String word[], int a, int b){
		
		if (left>=0 && right<400 && ok!=0){ 
			System.out.println("Точка пересечения="+ok+", Левая граница="+left+", Правая граница="+right+" w="+a);
			for (int j = ok; j<=right; j++){
				numButtons[j].setText(word[a]);
				a++;
			}
			for (int j = ok; j>=left; j--){
				 numButtons[j].setText(word[b]);
				 b--;
			}
		}
	}
 	
	//Проверка границ построения по вертикали
	public  void prov_vert(int up, int down, int ok, String word[], int a, int b){
		if (up>=8 && down<400 && ok!=0){ 
			stop=1;
			System.out.println("Точка пересечения="+ok+", Верх граница="+up+", Низ граница="+down);
			for (int j = ok; j<down;){
				numButtons[j=j+20].setText(word[a+1]);
				numButtons[j].setBackground(Color.green);
				a++;
			}
			for (int j = ok; j>up;){
				 numButtons[j=j-20].setText(word[b-1]);
				 numButtons[j].setBackground(Color.green);
				 b--;
			}
		}
	}
	
//	public void words(int number_words, String wd, int ok_position){
//		
//		int b0=steck.size();
//		 
//				
//		for (int wordslength=0; wordslength<words.length; wordslength++){
//			System.out.println(">>>>");
//			if (ok_position!=0 ){ 
//				System.out.println("STOP");
//				break;
//			}
//			int count=0;
//			wd = words[wordslength][0];
//						
//			for (int i=0; i<=b0; i++){ //проверяем на совпадения слова между собой
//				if(ok_position==0 &&  wd!=steck.get(i)){
//					count++;
//				}
//			}
//			
//			if(count==number_words){ //поиск по горизонтали
//				int d = wd.length();
//				String temp1[] = new String[d];
//				temp1 = wd.split("");
//				//ok_position=1;
//				
//				System.out.println(">>Fourth words (1 vs 4) - "+words[wordslength][0]);
//				for (int i = 1; i<d; i++){
//					for (int j = 148; j<148+dl1; j++){
//					if (ok_position!=0 && ok_position!=ok && ok+1!=ok_position && ok-1!=ok_position && ok_position!=ok3 && ok3+1!=ok_position && ok3-1!=ok_position){
//						break;
//					}
//					if (numButtons[j].getText().equalsIgnoreCase(temp1[i])){
//						w=i;
//						w1=i;
//						int tmp=j;
//						if (tmp!=ok && ok+1!=tmp && ok-1!=tmp && tmp!=ok3 && ok3+1!=tmp && ok3-1!=tmp){
//							ok_position=j;
//							numButtons[j].setBackground(Color.blue);
//						}
//					}
//					}
//				}
//			}
//			
//		}
//	}
	
	// Вызов озвучки к слову mp3
	public void audioMP3(int i) throws Throwable {
			
	FileInputStream fis = new FileInputStream(words[i][3]);
	final Player playMP3 = new Player(fis);
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			try {
				playMP3.play();
			} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}});
	}
}


