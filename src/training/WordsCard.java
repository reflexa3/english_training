package training;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import windows.MainWindow;
import javazoom.jl.player.Player;

public class WordsCard extends JPanel{
	
	private static final long serialVersionUID = 5491963174442752852L;
	String[][] words;
	Boolean [] visible = new Boolean[12]; 
	Map<Integer, String> map = new HashMap<Integer, String>();//id_words
	Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();//result training
	Map<Integer, String> map2 = new HashMap<Integer, String>(); // Words
	Map<Integer, String> map3 = new HashMap<Integer, String>(); // Translate
	Map<Integer, String> map4 = new HashMap<Integer, String>(); // Transcription
	Map<Integer, String> map5 = new HashMap<Integer, String>(); // Progress
	JButton n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,y1,y2,y3,y4,y5,y6,y7,y8,y9,y10,y11,y12;
	Box b12,b15,b16,b22,b25,b26,b32,b35,b36,b42,b45,b46,b52,b55,b56,b62,b65,b66,b72,b75,b76,b82,b85,b86,
	b92,b95,b96,b102,b105,b106,b112,b115,b116,b122,b125,b126; 
	JLabel pr1,pr11,pr2,pr21,pr3,pr31,pr4,pr41,pr5,pr51,pr6,pr61,pr7,pr71,pr8,pr81,pr9,pr91,pr10,pr101,pr11_1,pr11_2,pr12,pr121;
	String item,item1,training;
	JButton stop;
	int dl1,count1,count2; 
	int prog1,prog2,prog3,prog4,prog5,prog6,prog7,prog8,prog9,prog10,prog11,prog12;
		
	public WordsCard(String [][] data, String a, String b, String c) {
		
		words=data;
		item=a;
		item1=b;
		training=c;
		final int dl=words.length;
		
		if (dl>12){
			dl1=12;
		}else if(dl<=12){
			dl1=dl;
		}
				
		for(int i=0; i<=11; i++){
			if (i<dl){
				//System.out.println(i+"  "+words[i][0]+" "+words[i][1]+" "+words[i][2]+" "+words[i][3]+" "+words[i][4]+" "+words[i][5]+" "+words[i][6]);
				visible[i]=true;
				map.put(i+1,words[i][5]);
				map2.put(i+1,words[i][0]);
				map3.put(i+1, words[i][1]);
				map4.put(i+1, words[i][2]);
				map5.put(i+1, words[i][6]);
			}else{
				visible[i]=false;
				map2.put(i+1,words[0][0]);
				map3.put(i+1, words[0][1]);
				map4.put(i+1, words[0][2]);
				map5.put(i+1, words[0][6]);
				}
		}
		
		/*------------------------Создаем панели---------------------------------------*/
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1000, 605));
		//setOpaque(false);
		
		// Область где будет прогрес тренровки (North p1)
		Color yellow = new Color(250, 194, 90); // yellow
		JPanel p = new JPanel();
		FlowLayout fl = new FlowLayout();
		p.setLayout(fl);
		p.setBackground(yellow);
		
		// Область самой тренировки (Center pp)
		JPanel pp = new JPanel();
		FlowLayout fl1 = new FlowLayout(0, 10, 0);
		pp.setLayout(fl1);
						
		Box box = Box.createHorizontalBox();
		box.setPreferredSize(new Dimension(999, 50));
		Box box1 = Box.createHorizontalBox();
		box1.setPreferredSize(new Dimension(999, 10));
		Box box2 = Box.createHorizontalBox();
		box2.setPreferredSize(new Dimension(999, 10));
				
		Font fontwords = new Font("Arial", Font.PLAIN, 24);
		Font fonttranslate = new Font("Arial", Font.PLAIN, 14);
		Font fontbuttons = new Font("Arial", Font.PLAIN, 14);
		//Border line = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		Color Color1 = new Color(75, 142, 197); // btn
		
		// ------------------------------------------------Панель 1
		JPanel p1 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p1.setLayout(gbl1);
		p1.setBackground(Color.white);
		p1.setVisible(visible[0]);
		p1.setPreferredSize(new Dimension(240, 170));
		
		Box t1=Box.createHorizontalBox();
		Box b1 = Box.createHorizontalBox();
		b12 = Box.createHorizontalBox();
		Box b13 = Box.createHorizontalBox();
		Box b14 = Box.createHorizontalBox();
		b15 = Box.createHorizontalBox();
		b16 = Box.createHorizontalBox();
		Box b17 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р1 
		GridBagConstraints c1 = new GridBagConstraints();
				
		// Прогрес изученого слова
		String prog_pass1 ="res/images/progress/"+map5.get(1)+".png";
		pr1 = new JLabel(new ImageIcon(prog_pass1));
		if(map5.get(1).equalsIgnoreCase("0")){
			pr1.setToolTipText("Прогресс слова: 0%.");
			prog1=1;
		}else if(map5.get(1).equalsIgnoreCase("1")){
			pr1.setToolTipText("Прогресс слова: 20%.");
			prog1=2;
		}else if(map5.get(1).equalsIgnoreCase("2")){
			pr1.setToolTipText("Прогресс слова: 40%.");
			prog1=3;
		}else if(map5.get(1).equalsIgnoreCase("3")){
			pr1.setToolTipText("Прогресс слова: 60%.");
			prog1=4;
		}else if(map5.get(1).equalsIgnoreCase("4")){
			pr1.setToolTipText("Прогресс слова: 80%.");
			prog1=5;
		}else if(map5.get(1).equalsIgnoreCase("5")){
			pr1.setToolTipText("Прогресс слова: 100%.");
			prog1=6;
		}
		pr1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr1.setVisible(true);
		
		String prog_pass11 ="res/images/progress/"+prog1+".png";
		pr11 = new JLabel(new ImageIcon(prog_pass11));
		if(prog1==1){
			pr11.setToolTipText("Прогресс слова: 20%.");
		}else if(prog1==2){
			pr11.setToolTipText("Прогресс слова: 40%.");
		}else if(prog1==3){
			pr11.setToolTipText("Прогресс слова: 60%.");
		}else if(prog1==4){
			pr11.setToolTipText("Прогресс слова: 80%.");
		}else if(prog1==5){
			pr11.setToolTipText("Прогресс слова: 100%.");
		}    
		pr11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr11.setVisible(false);
		
		t1.add(Box.createHorizontalStrut(200));
		t1.add(pr1);
		t1.add(pr11);
		c1.insets = new Insets(2, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 0;
		p1.add(t1, c1);
		
		// Words in english
		JLabel words1 = new JLabel(map2.get(1));
		words1.setFont(fontwords);
		b1.add(words1);
		c1.insets = new Insets(0, 0, 3, 0);
		c1.gridx = 0;
		c1.gridy = 1;
		p1.add(b1, c1);
		
		// Words in russian
		JLabel transl1 = new JLabel(map3.get(1));
		transl1.setFont(fonttranslate);
		b12.add(transl1);
		c1.insets = new Insets(0, 0, 5, 0);
		c1.gridx = 0;
		c1.gridy = 2;
		b12.setVisible(false);
		p1.add(b12, c1);
		
		// Audio for words
		JButton audio1 = new JButton(new ImageIcon("res/images/9.png"));
		audio1.setText("");
		audio1.setBorder(null);
		audio1.setBackground(null);
		audio1.setForeground(Color.white);
		audio1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(0);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b13.add(audio1);
		c1.insets = new Insets(0, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 3;
		p1.add(b13, c1);
		
		// Transcription for words
		JLabel transc1 = new JLabel(map4.get(1));
		transc1.setFont(fonttranslate);
		b14.add(transc1);
		c1.gridx = 0;
		c1.gridy = 4;
		c1.insets = new Insets(8, 0, 5, 0);
		p1.add( b14, c1);
		
		JLabel good = new JLabel(new ImageIcon("res/images/ok.png"));
		b15.add(good);
		b15.add(Box.createHorizontalStrut(40));
		
		JButton undo = new JButton("На повторения");
		undo.setBorder(null);
		undo.setBackground(null);
		undo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr1.setVisible(true);
				pr11.setVisible(false);
				b16.setVisible(true);
				b15.setVisible(false);
				map1.put(1, 0);
			}
		});
		b15.add(undo);
		c1.insets = new Insets(0, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 5;
		b15.setVisible(false);
		p1.add(b15, c1);
		
		JLabel bad = new JLabel(new ImageIcon("res/images/bad.png"));
		b16.add(bad);
		c1.insets = new Insets(0, 0, 0, 170);
		c1.gridx = 0;
		c1.gridy = 6;
		b16.setVisible(false);
		p1.add(b16, c1);
		
		n1 = new JButton("Не знаю");
		n1.setBackground(Color1);
		n1.setForeground(Color.white);
		n1.setFont(fontbuttons);
		n1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n1.setVisible(false);
				y1.setVisible(false);
				b12.setVisible(true);
				b16.setVisible(true);
				map1.put(1, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(0);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b17.add(n1);
		b17.add(Box.createHorizontalStrut(30));
				
		y1 = new JButton("Я знаю!");
		y1.setBackground(Color1);
		y1.setForeground(Color.white);
		y1.setFont(fontbuttons);
		y1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr1.setVisible(false);
				pr11.setVisible(true);
				count2++;
				n1.setVisible(false);
				y1.setVisible(false);
				b12.setVisible(true);
				b15.setVisible(true);
				map1.put(1, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(0);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b17.add(y1);
		c1.insets = new Insets(5, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 7;
		p1.add(b17, c1);
					
		// ------------------------------------------------Панель 2
		JPanel p2 = new JPanel();
		GridBagLayout gbl2 = new GridBagLayout();
		p2.setLayout(gbl2);
		p2.setBackground(Color.white);
		p2.setVisible(visible[1]);
		p2.setPreferredSize(new Dimension(240, 170));
		
		Box t2=Box.createHorizontalBox();
		Box b2 = Box.createHorizontalBox();
		b22 = Box.createHorizontalBox();
		Box b23 = Box.createHorizontalBox();
		Box b24 = Box.createHorizontalBox();
		b25 = Box.createHorizontalBox();
		b26 = Box.createHorizontalBox();
		Box b27 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р2 
		GridBagConstraints c2 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass2 ="res/images/progress/"+map5.get(2)+".png";
		pr2 = new JLabel(new ImageIcon(prog_pass2));
		if(map5.get(2).equalsIgnoreCase("0")){
			pr2.setToolTipText("Прогресс слова: 0%.");
			prog2=1;
		}else if(map5.get(2).equalsIgnoreCase("1")){
			pr2.setToolTipText("Прогресс слова: 20%.");
			prog2=2;
		}else if(map5.get(2).equalsIgnoreCase("2")){
			pr2.setToolTipText("Прогресс слова: 40%.");
			prog2=3;
		}else if(map5.get(2).equalsIgnoreCase("3")){
			pr2.setToolTipText("Прогресс слова: 60%.");
			prog2=4;
		}else if(map5.get(2).equalsIgnoreCase("4")){
			pr2.setToolTipText("Прогресс слова: 80%.");
			prog2=5;
		}else if(map5.get(2).equalsIgnoreCase("5")){
			pr2.setToolTipText("Прогресс слова: 100%.");
			prog2=6;
		}
		pr2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr2.setVisible(true);
		
		String prog_pass21 ="res/images/progress/"+prog2+".png";
		pr21= new JLabel(new ImageIcon(prog_pass21));
		if(prog2==1){
			pr21.setToolTipText("Прогресс слова: 20%.");
		}else if(prog2==2){
			pr21.setToolTipText("Прогресс слова: 40%.");
		}else if(prog2==3){
			pr21.setToolTipText("Прогресс слова: 60%.");
		}else if(prog2==4){
			pr21.setToolTipText("Прогресс слова: 80%.");
		}else if(prog2==5){
			pr21.setToolTipText("Прогресс слова: 100%.");
		}    
		pr21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr21.setVisible(false);
		
		t2.add(Box.createHorizontalStrut(200));
		t2.add(pr2);
		t2.add(pr21);
		c2.insets = new Insets(2, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 0;
		p2.add(t2, c2);
		
		// Words in english
		JLabel words2 = new JLabel(map2.get(2));
		words2.setFont(fontwords);
		b2.add(words2);
		c2.insets = new Insets(0, 0, 3, 0);
		c2.gridx = 0;
		c2.gridy = 1;
		p2.add(b2, c2);
		
		// Words in russian
		JLabel transl2 = new JLabel(map3.get(2));
		transl2.setFont(fonttranslate);
		b22.add(transl2);
		c2.insets = new Insets(0, 0, 5, 0);
		c2.gridx = 0;
		c2.gridy = 2;
		b22.setVisible(false);
		p2.add(b22, c2);
		
		// Audio for words
		JButton audio2 = new JButton(new ImageIcon("res/images/9.png"));
		audio2.setText("");
		audio2.setBorder(null);
		audio2.setBackground(null);
		audio2.setForeground(Color.white);
		audio2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(1);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b23.add(audio2);
		c2.insets = new Insets(0, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 3;
		p2.add(b23, c2);
		
		// Transcription for words
		JLabel transc2 = new JLabel(map4.get(2));
		transc2.setFont(fonttranslate);
		b24.add(transc2);
		c2.gridx = 0;
		c2.gridy = 4;
		c2.insets = new Insets(8, 0, 5, 0);
		p2.add( b24, c2);
		
		JLabel good2 = new JLabel(new ImageIcon("res/images/ok.png"));
		b25.add(good2);
		b25.add(Box.createHorizontalStrut(40));
		
		JButton undo2 = new JButton("На повторения");
		undo2.setBorder(null);
		undo2.setBackground(null);
		undo2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr2.setVisible(true);
				pr21.setVisible(false);
				b26.setVisible(true);
				b25.setVisible(false);
				map1.put(2, 0);
			}
		});
		b25.add(undo2);
		c2.insets = new Insets(0, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 5;
		b25.setVisible(false);
		p2.add(b25, c2);
		
		JLabel bad2 = new JLabel(new ImageIcon("res/images/bad.png"));
		b26.add(bad2);
		c2.insets = new Insets(0, 0, 0, 170);
		c2.gridx = 0;
		c2.gridy = 6;
		b26.setVisible(false);
		p2.add(b26, c2);
		
		n2 = new JButton("Не знаю");
		n2.setBackground(Color1);
		n2.setForeground(Color.white);
		n2.setFont(fontbuttons);
		n2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n2.setVisible(false);
				y2.setVisible(false);
				b22.setVisible(true);
				b26.setVisible(true);
				map1.put(2, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(1);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b27.add(n2);
		b27.add(Box.createHorizontalStrut(30));
				
		y2 = new JButton("Я знаю!");
		y2.setBackground(Color1);
		y2.setForeground(Color.white);
		y2.setFont(fontbuttons);
		y2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr2.setVisible(false);
				pr21.setVisible(true);
				count2++;
				n2.setVisible(false);
				y2.setVisible(false);
				b22.setVisible(true);
				b25.setVisible(true);
				map1.put(2, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(1);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b27.add(y2);
		c2.insets = new Insets(5, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 7;
		p2.add(b27, c2);
				
		// ------------------------------------------------Панель 3
		JPanel p3 = new JPanel();
		GridBagLayout gbl3 = new GridBagLayout();
		p3.setLayout(gbl3);
		p3.setBackground(Color.white);
		p3.setVisible(visible[2]);
		p3.setPreferredSize(new Dimension(240, 170));
		
		Box t3 = Box.createHorizontalBox();
		Box b3 = Box.createHorizontalBox();
		b32 = Box.createHorizontalBox();
		Box b33 = Box.createHorizontalBox();
		Box b34 = Box.createHorizontalBox();
		b35 = Box.createHorizontalBox();
		b36 = Box.createHorizontalBox();
		Box b37 = Box.createHorizontalBox();
				
		// Добавляем елементы на панель р3 
		GridBagConstraints c3 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass3 ="res/images/progress/"+map5.get(3)+".png";
		pr3 = new JLabel(new ImageIcon(prog_pass3));
		if(map5.get(3).equalsIgnoreCase("0")){
			pr3.setToolTipText("Прогресс слова: 0%.");
			prog3=1;
		}else if(map5.get(3).equalsIgnoreCase("1")){
			pr3.setToolTipText("Прогресс слова: 20%.");
			prog3=2;
		}else if(map5.get(3).equalsIgnoreCase("2")){
			pr3.setToolTipText("Прогресс слова: 40%.");
			prog3=3;
		}else if(map5.get(3).equalsIgnoreCase("3")){
			pr3.setToolTipText("Прогресс слова: 60%.");
			prog3=4;
		}else if(map5.get(3).equalsIgnoreCase("4")){
			pr3.setToolTipText("Прогресс слова: 80%.");
			prog3=5;
		}else if(map5.get(3).equalsIgnoreCase("5")){
			pr3.setToolTipText("Прогресс слова: 100%.");
			prog3=6;
		}
		pr3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr3.setVisible(true);
		
		String prog_pass31 ="res/images/progress/"+prog3+".png";
		pr31= new JLabel(new ImageIcon(prog_pass31));
		if(prog3==1){
			pr31.setToolTipText("Прогресс слова: 20%.");
		}else if(prog3==2){
			pr31.setToolTipText("Прогресс слова: 40%.");
		}else if(prog3==3){
			pr31.setToolTipText("Прогресс слова: 60%.");
		}else if(prog3==4){
			pr31.setToolTipText("Прогресс слова: 80%.");
		}else if(prog3==5){
			pr31.setToolTipText("Прогресс слова: 100%.");
		}    
		pr31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr31.setVisible(false);
		
		t3.add(Box.createHorizontalStrut(200));
		t3.add(pr3);
		t3.add(pr31);
		c3.insets = new Insets(2, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 0;
		p3.add(t3, c3);
		
		// Words in english
		JLabel words3 = new JLabel(map2.get(3));
		words3.setFont(fontwords);
		b3.add(words3);
		c3.insets = new Insets(0, 0, 3, 0);
		c3.gridx = 0;
		c3.gridy = 1;
		p3.add(b3, c3);
		
		// Words in russian
		JLabel transl3 = new JLabel(map3.get(3));
		transl3.setFont(fonttranslate);
		b32.add(transl3);
		c3.insets = new Insets(0, 0, 5, 0);
		c3.gridx = 0;
		c3.gridy = 2;
		b32.setVisible(false);
		p3.add(b32, c3);
		
		// Audio for words
		JButton audio3 = new JButton(new ImageIcon("res/images/9.png"));
		audio3.setText("");
		audio3.setBorder(null);
		audio3.setBackground(null);
		audio3.setForeground(Color.white);
		audio3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(2);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b33.add(audio3);
		c3.insets = new Insets(0, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 3;
		p3.add(b33, c3);
		
		// Transcription for words
		JLabel transc3 = new JLabel(map4.get(3));
		transc3.setFont(fonttranslate);
		b34.add(transc3);
		c3.gridx = 0;
		c3.gridy = 4;
		c3.insets = new Insets(8, 0, 5, 0);
		p3.add( b34, c3);
		
		JLabel good3 = new JLabel(new ImageIcon("res/images/ok.png"));
		b35.add(good3);
		b35.add(Box.createHorizontalStrut(40));
		
		JButton undo3 = new JButton("На повторения");
		undo3.setBorder(null);
		undo3.setBackground(null);
		undo3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr3.setVisible(true);
				pr31.setVisible(false);
				b36.setVisible(true);
				b35.setVisible(false);
				map1.put(3, 0);
			}
		});
		b35.add(undo3);
		c3.insets = new Insets(0, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 5;
		b35.setVisible(false);
		p3.add(b35, c3);
		
		JLabel bad3 = new JLabel(new ImageIcon("res/images/bad.png"));
		b36.add(bad3);
		c3.insets = new Insets(0, 0, 0, 170);
		c3.gridx = 0;
		c3.gridy = 6;
		b36.setVisible(false);
		p3.add(b36, c3);
		
		n3 = new JButton("Не знаю");
		n3.setBackground(Color1);
		n3.setForeground(Color.white);
		n3.setFont(fontbuttons);
		n3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n3.setVisible(false);
				y3.setVisible(false);
				b32.setVisible(true);
				b36.setVisible(true);
				map1.put(3, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(2);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b37.add(n3);
		b37.add(Box.createHorizontalStrut(30));
				
		y3 = new JButton("Я знаю!");
		y3.setBackground(Color1);
		y3.setForeground(Color.white);
		y3.setFont(fontbuttons);
		y3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr3.setVisible(false);
				pr31.setVisible(true);
				count2++;
				n3.setVisible(false);
				y3.setVisible(false);
				b32.setVisible(true);
				b35.setVisible(true);
				map1.put(3, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
						stop.setVisible(true);
				}
				try {
					audioMP3(2);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b37.add(y3);
		c3.insets = new Insets(5, 0, 0, 0);
		c3.gridx = 0;
		c3.gridy = 7;
		p3.add(b37, c3);
				
		// ------------------------------------------------Панель 4
		JPanel p4 = new JPanel();
		GridBagLayout gbl4 = new GridBagLayout();
		p4.setLayout(gbl4);
		p4.setBackground(Color.white);
		p4.setVisible(visible[3]);
		p4.setPreferredSize(new Dimension(240, 170));
		
		Box t4 = Box.createHorizontalBox();
		Box b4 = Box.createHorizontalBox();
		b42 = Box.createHorizontalBox();
		Box b43 = Box.createHorizontalBox();
		Box b44 = Box.createHorizontalBox();
		b45 = Box.createHorizontalBox();
		b46 = Box.createHorizontalBox();
		Box b47 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р4 
		GridBagConstraints c4 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass4 ="res/images/progress/"+map5.get(4)+".png";
		pr4 = new JLabel(new ImageIcon(prog_pass4));
		if(map5.get(4).equalsIgnoreCase("0")){
			pr4.setToolTipText("Прогресс слова: 0%.");
			prog4=1;
		}else if(map5.get(4).equalsIgnoreCase("1")){
			pr4.setToolTipText("Прогресс слова: 20%.");
			prog4=2;
		}else if(map5.get(4).equalsIgnoreCase("2")){
			pr4.setToolTipText("Прогресс слова: 40%.");
			prog4=3;
		}else if(map5.get(4).equalsIgnoreCase("3")){
			pr4.setToolTipText("Прогресс слова: 60%.");
			prog4=4;
		}else if(map5.get(4).equalsIgnoreCase("4")){
			pr4.setToolTipText("Прогресс слова: 80%.");
			prog4=5;
		}else if(map5.get(4).equalsIgnoreCase("5")){
			pr4.setToolTipText("Прогресс слова: 100%.");
			prog4=6;
		}
		pr3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr3.setVisible(true);
		
		String prog_pass41 ="res/images/progress/"+prog4+".png";
		pr41= new JLabel(new ImageIcon(prog_pass41));
		if(prog4==1){
			pr41.setToolTipText("Прогресс слова: 20%.");
		}else if(prog4==2){
			pr41.setToolTipText("Прогресс слова: 40%.");
		}else if(prog4==3){
			pr41.setToolTipText("Прогресс слова: 60%.");
		}else if(prog4==4){
			pr41.setToolTipText("Прогресс слова: 80%.");
		}else if(prog4==5){
			pr41.setToolTipText("Прогресс слова: 100%.");
		}    
		pr41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr41.setVisible(false);		
		
		t4.add(Box.createHorizontalStrut(200));
		t4.add(pr4);
		t4.add(pr41);
		c4.insets = new Insets(2, 0, 0, 0);
		c4.gridx = 0;
		c4.gridy = 0;
		p4.add(t4, c4);
		
		// Words in english
		JLabel words4 = new JLabel(map2.get(4));
		words4.setFont(fontwords);
		b4.add(words4);
		c4.insets = new Insets(0, 0, 3, 0);
		c4.gridx = 0;
		c4.gridy = 1;
		p4.add(b4, c4);
		
		// Words in russian
		JLabel transl4 = new JLabel(map3.get(4));
		transl4.setFont(fonttranslate);
		b42.add(transl4);
		c4.insets = new Insets(0, 0, 5, 0);
		c4.gridx = 0;
		c4.gridy = 2;
		b42.setVisible(false);
		p4.add(b42, c4);
		
		// Audio for words
		JButton audio4 = new JButton(new ImageIcon("res/images/9.png"));
		audio4.setText("");
		audio4.setBorder(null);
		audio4.setBackground(null);
		audio4.setForeground(Color.white);
		audio4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(3);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b43.add(audio4);
		c4.insets = new Insets(0, 0, 0, 0);
		c4.gridx = 0;
		c4.gridy = 3;
		p4.add(b43, c4);
		
		// Transcription for words
		JLabel transc4 = new JLabel(map4.get(4));
		transc4.setFont(fonttranslate);
		b44.add(transc4);
		c4.gridx = 0;
		c4.gridy = 4;
		c4.insets = new Insets(8, 0, 5, 0);
		p4.add( b44, c4);
		
		JLabel good4 = new JLabel(new ImageIcon("res/images/ok.png"));
		b45.add(good4);
		b45.add(Box.createHorizontalStrut(40));
		
		JButton undo4 = new JButton("На повторения");
		undo4.setBorder(null);
		undo4.setBackground(null);
		undo4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr4.setVisible(true);
				pr41.setVisible(false);
				b46.setVisible(true);
				b45.setVisible(false);
				map1.put(4, 0);
			}
		});
		b45.add(undo4);
		c4.insets = new Insets(0, 0, 0, 0);
		c4.gridx = 0;
		c4.gridy = 5;
		b45.setVisible(false);
		p4.add(b45, c4);
		
		JLabel bad4 = new JLabel(new ImageIcon("res/images/bad.png"));
		b46.add(bad4);
		c4.insets = new Insets(0, 0, 0, 170);
		c4.gridx = 0;
		c4.gridy = 6;
		b46.setVisible(false);
		p4.add(b46, c4);
		
		n4 = new JButton("Не знаю");
		n4.setBackground(Color1);
		n4.setForeground(Color.white);
		n4.setFont(fontbuttons);
		n4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n4.setVisible(false);
				y4.setVisible(false);
				b42.setVisible(true);
				b46.setVisible(true);
				map1.put(4, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(3);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b47.add(n4);
		b47.add(Box.createHorizontalStrut(30));
				
		y4 = new JButton("Я знаю!");
		y4.setBackground(Color1);
		y4.setForeground(Color.white);
		y4.setFont(fontbuttons);
		y4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr4.setVisible(false);
				pr41.setVisible(true);
				count2++;
				n4.setVisible(false);
				y4.setVisible(false);
				b42.setVisible(true);
				b45.setVisible(true);
				map1.put(4, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(3);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b47.add(y4);
		c4.insets = new Insets(5, 0, 0, 0);
		c4.gridx = 0;
		c4.gridy = 7;
		p4.add(b47, c4);
			
		// ------------------------------------------------Панель 5
		JPanel p5 = new JPanel();
		GridBagLayout gbl5 = new GridBagLayout();
		p5.setLayout(gbl5);
		p5.setBackground(Color.white);
		p5.setVisible(visible[4]);
		p5.setPreferredSize(new Dimension(240, 170));
		
		Box t5 = Box.createHorizontalBox();
		Box b5 = Box.createHorizontalBox();
		b52 = Box.createHorizontalBox();
		Box b53 = Box.createHorizontalBox();
		Box b54 = Box.createHorizontalBox();
		b55 = Box.createHorizontalBox();
		b56 = Box.createHorizontalBox();
		Box b57 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р5 
		GridBagConstraints c5 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass5 ="res/images/progress/"+map5.get(5)+".png";
		pr5 = new JLabel(new ImageIcon(prog_pass5));
		if(map5.get(5).equalsIgnoreCase("0")){
			pr5.setToolTipText("Прогресс слова: 0%.");
			prog5=1;
		}else if(map5.get(5).equalsIgnoreCase("1")){
			pr5.setToolTipText("Прогресс слова: 20%.");
			prog5=2;
		}else if(map5.get(5).equalsIgnoreCase("2")){
			pr5.setToolTipText("Прогресс слова: 40%.");
			prog5=3;
		}else if(map5.get(5).equalsIgnoreCase("3")){
			pr5.setToolTipText("Прогресс слова: 60%.");
			prog5=4;
		}else if(map5.get(5).equalsIgnoreCase("4")){
			pr5.setToolTipText("Прогресс слова: 80%.");
			prog5=5;
		}else if(map5.get(5).equalsIgnoreCase("5")){
			pr5.setToolTipText("Прогресс слова: 100%.");
			prog5=6;
		}
		pr3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr3.setVisible(true);
		
		String prog_pass51 ="res/images/progress/"+prog5+".png";
		pr51= new JLabel(new ImageIcon(prog_pass51));
		if(prog5==1){
			pr51.setToolTipText("Прогресс слова: 20%.");
		}else if(prog5==2){
			pr51.setToolTipText("Прогресс слова: 40%.");
		}else if(prog5==3){
			pr51.setToolTipText("Прогресс слова: 60%.");
		}else if(prog5==4){
			pr51.setToolTipText("Прогресс слова: 80%.");
		}else if(prog5==5){
			pr51.setToolTipText("Прогресс слова: 100%.");
		}    
		pr51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr51.setVisible(false);	
		
		t5.add(Box.createHorizontalStrut(200));
		t5.add(pr5);
		t5.add(pr51);
		c5.insets = new Insets(2, 0, 0, 0);
		c5.gridx = 0;
		c5.gridy = 0;
		p5.add(t5, c5);
		
		// Words in english
		JLabel words5 = new JLabel(map2.get(5));
		words5.setFont(fontwords);
		b5.add(words5);
		c5.insets = new Insets(0, 0, 3, 0);
		c5.gridx = 0;
		c5.gridy = 1;
		p5.add(b5, c5);
		
		// Words in russian
		JLabel transl5 = new JLabel(map3.get(5));
		transl5.setFont(fonttranslate);
		b52.add(transl5);
		c5.insets = new Insets(0, 0, 5, 0);
		c5.gridx = 0;
		c5.gridy = 2;
		b52.setVisible(false);
		p5.add(b52, c5);
		
		// Audio for words
		JButton audio5 = new JButton(new ImageIcon("res/images/9.png"));
		audio5.setText("");
		audio5.setBorder(null);
		audio5.setBackground(null);
		audio5.setForeground(Color.white);
		audio5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(4);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b53.add(audio5);
		c5.insets = new Insets(0, 0, 0, 0);
		c5.gridx = 0;
		c5.gridy = 3;
		p5.add(b53, c5);
		
		// Transcription for words
		JLabel transc5 = new JLabel(map4.get(5));
		transc5.setFont(fonttranslate);
		b54.add(transc5);
		c5.gridx = 0;
		c5.gridy = 4;
		c5.insets = new Insets(8, 0, 5, 0);
		p5.add( b54, c5);
		
		JLabel good5 = new JLabel(new ImageIcon("res/images/ok.png"));
		b55.add(good5);
		b55.add(Box.createHorizontalStrut(40));
		
		JButton undo5 = new JButton("На повторения");
		undo5.setBorder(null);
		undo5.setBackground(null);
		undo5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr5.setVisible(true);
				pr51.setVisible(false);
				b56.setVisible(true);
				b55.setVisible(false);
				map1.put(5, 0);
			}
		});
		b55.add(undo5);
		c5.insets = new Insets(0, 0, 0, 0);
		c5.gridx = 0;
		c5.gridy = 5;
		b55.setVisible(false);
		p5.add(b55, c5);
		
		JLabel bad5 = new JLabel(new ImageIcon("res/images/bad.png"));
		b56.add(bad5);
		c5.insets = new Insets(0, 0, 0, 170);
		c5.gridx = 0;
		c5.gridy = 6;
		b56.setVisible(false);
		p5.add(b56, c5);
		
		n5 = new JButton("Не знаю");
		n5.setBackground(Color1);
		n5.setForeground(Color.white);
		n5.setFont(fontbuttons);
		n5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n5.setVisible(false);
				y5.setVisible(false);
				b52.setVisible(true);
				b56.setVisible(true);
				map1.put(5, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(4);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b57.add(n5);
		b57.add(Box.createHorizontalStrut(30));
				
		y5 = new JButton("Я знаю!");
		y5.setBackground(Color1);
		y5.setForeground(Color.white);
		y5.setFont(fontbuttons);
		y5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr5.setVisible(false);
				pr51.setVisible(true);
				count2++;
				n5.setVisible(false);
				y5.setVisible(false);
				b52.setVisible(true);
				b55.setVisible(true);
				map1.put(5, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(4);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b57.add(y5);
		c5.insets = new Insets(5, 0, 0, 0);
		c5.gridx = 0;
		c5.gridy = 7;
		p5.add(b57, c5);
				
		// ------------------------------------------------Панель 6
		JPanel p6 = new JPanel();
		GridBagLayout gbl6 = new GridBagLayout();
		p6.setLayout(gbl6);
		p6.setBackground(Color.white);
		p6.setVisible(visible[5]);
		p6.setPreferredSize(new Dimension(240, 170));
		
		Box t6 = Box.createHorizontalBox();
		Box b6 = Box.createHorizontalBox();
		b62 = Box.createHorizontalBox();
		Box b63 = Box.createHorizontalBox();
		Box b64 = Box.createHorizontalBox();
		b65 = Box.createHorizontalBox();
		b66 = Box.createHorizontalBox();
		Box b67 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р6 
		GridBagConstraints c6 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass6 ="res/images/progress/"+map5.get(6)+".png";
		pr6 = new JLabel(new ImageIcon(prog_pass6));
		if(map5.get(6).equalsIgnoreCase("0")){
			pr6.setToolTipText("Прогресс слова: 0%.");
			prog6=1;
		}else if(map5.get(6).equalsIgnoreCase("1")){
			pr6.setToolTipText("Прогресс слова: 20%.");
			prog6=2;
		}else if(map5.get(6).equalsIgnoreCase("2")){
			pr6.setToolTipText("Прогресс слова: 40%.");
			prog6=3;
		}else if(map5.get(6).equalsIgnoreCase("3")){
			pr6.setToolTipText("Прогресс слова: 60%.");
			prog6=4;
		}else if(map5.get(6).equalsIgnoreCase("4")){
			pr6.setToolTipText("Прогресс слова: 80%.");
			prog6=5;
		}else if(map5.get(6).equalsIgnoreCase("5")){
			pr6.setToolTipText("Прогресс слова: 100%.");
			prog6=6;
		}
		pr6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr6.setVisible(true);
		
		String prog_pass61 ="res/images/progress/"+prog6+".png";
		pr61= new JLabel(new ImageIcon(prog_pass61));
		if(prog6==1){
			pr61.setToolTipText("Прогресс слова: 20%.");
		}else if(prog6==2){
			pr61.setToolTipText("Прогресс слова: 40%.");
		}else if(prog6==3){
			pr61.setToolTipText("Прогресс слова: 60%.");
		}else if(prog6==4){
			pr61.setToolTipText("Прогресс слова: 80%.");
		}else if(prog6==5){
			pr61.setToolTipText("Прогресс слова: 100%.");
		}    
		pr61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr61.setVisible(false);
		
		t6.add(Box.createHorizontalStrut(200));
		t6.add(pr6);
		t6.add(pr61);
		c6.insets = new Insets(2, 0, 0, 0);
		c6.gridx = 0;
		c6.gridy = 0;
		p6.add(t6, c6);
		
		// Words in english
		JLabel words6 = new JLabel(map2.get(6));
		words6.setFont(fontwords);
		b6.add(words6);
		c6.insets = new Insets(0, 0, 3, 0);
		c6.gridx = 0;
		c6.gridy = 1;
		p6.add(b6, c6);
		
		// Words in russian
		JLabel transl6 = new JLabel(map3.get(6));
		transl6.setFont(fonttranslate);
		b62.add(transl6);
		c6.insets = new Insets(0, 0, 5, 0);
		c6.gridx = 0;
		c6.gridy = 2;
		b62.setVisible(false);
		p6.add(b62, c6);
		
		// Audio for words
		JButton audio6 = new JButton(new ImageIcon("res/images/9.png"));
		audio6.setText("");
		audio6.setBorder(null);
		audio6.setBackground(null);
		audio6.setForeground(Color.white);
		audio6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(5);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b63.add(audio6);
		c6.insets = new Insets(0, 0, 0, 0);
		c6.gridx = 0;
		c6.gridy = 3;
		p6.add(b63, c6);
		
		// Transcription for words
		JLabel transc6 = new JLabel(map4.get(6));
		transc6.setFont(fonttranslate);
		b64.add(transc6);
		c6.gridx = 0;
		c6.gridy = 4;
		c6.insets = new Insets(8, 0, 5, 0);
		p6.add( b64, c6);
		
		JLabel good6 = new JLabel(new ImageIcon("res/images/ok.png"));
		b65.add(good6);
		b65.add(Box.createHorizontalStrut(40));
		
		JButton undo6 = new JButton("На повторения");
		undo6.setBorder(null);
		undo6.setBackground(null);
		undo6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr6.setVisible(true);
				pr61.setVisible(false);
				b66.setVisible(true);
				b65.setVisible(false);
				map1.put(6, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
			}
		});
		b65.add(undo6);
		c6.insets = new Insets(0, 0, 0, 0);
		c6.gridx = 0;
		c6.gridy = 5;
		b65.setVisible(false);
		p6.add(b65, c6);
		
		JLabel bad6 = new JLabel(new ImageIcon("res/images/bad.png"));
		b66.add(bad6);
		c6.insets = new Insets(0, 0, 0, 170);
		c6.gridx = 0;
		c6.gridy = 6;
		b66.setVisible(false);
		p6.add(b66, c6);
		
		n6 = new JButton("Не знаю");
		n6.setBackground(Color1);
		n6.setForeground(Color.white);
		n6.setFont(fontbuttons);
		n6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n6.setVisible(false);
				y6.setVisible(false);
				b62.setVisible(true);
				b66.setVisible(true);
				map1.put(6, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(5);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b67.add(n6);
		b67.add(Box.createHorizontalStrut(30));
				
		y6 = new JButton("Я знаю!");
		y6.setBackground(Color1);
		y6.setForeground(Color.white);
		y6.setFont(fontbuttons);
		y6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr6.setVisible(false);
				pr61.setVisible(true);
				count2++;
				n6.setVisible(false);
				y6.setVisible(false);
				b62.setVisible(true);
				b65.setVisible(true);
				map1.put(6, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(5);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b67.add(y6);
		c6.insets = new Insets(5, 0, 0, 0);
		c6.gridx = 0;
		c6.gridy = 7;
		p6.add(b67, c6);
				
		// ------------------------------------------------Панель 7
		JPanel p7 = new JPanel();
		GridBagLayout gbl7 = new GridBagLayout();
		p7.setLayout(gbl7);
		p7.setBackground(Color.white);
		p7.setVisible(visible[6]);
		p7.setPreferredSize(new Dimension(240, 170));
		
		Box t7 = Box.createHorizontalBox();
		Box b7 = Box.createHorizontalBox();
		b72 = Box.createHorizontalBox();
		Box b73 = Box.createHorizontalBox();
		Box b74 = Box.createHorizontalBox();
		b75 = Box.createHorizontalBox();
		b76 = Box.createHorizontalBox();
		Box b77 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р7 
		GridBagConstraints c7 = new GridBagConstraints();
		

		// Прогрес изученого слова
		String prog_pass7 ="res/images/progress/"+map5.get(7)+".png";
		pr7 = new JLabel(new ImageIcon(prog_pass7));
		if(map5.get(7).equalsIgnoreCase("0")){
			pr7.setToolTipText("Прогресс слова: 0%.");
			prog7=1;
		}else if(map5.get(7).equalsIgnoreCase("1")){
			pr7.setToolTipText("Прогресс слова: 20%.");
			prog7=2;
		}else if(map5.get(7).equalsIgnoreCase("2")){
			pr7.setToolTipText("Прогресс слова: 40%.");
			prog7=3;
		}else if(map5.get(7).equalsIgnoreCase("3")){
			pr7.setToolTipText("Прогресс слова: 60%.");
			prog7=4;
		}else if(map5.get(7).equalsIgnoreCase("4")){
			pr7.setToolTipText("Прогресс слова: 80%.");
			prog7=5;
		}else if(map5.get(7).equalsIgnoreCase("5")){
			pr7.setToolTipText("Прогресс слова: 100%.");
			prog7=6;
		}
		pr7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr7.setVisible(true);
		
		String prog_pass71 ="res/images/progress/"+prog7+".png";
		pr71= new JLabel(new ImageIcon(prog_pass71));
		if(prog7==1){
			pr71.setToolTipText("Прогресс слова: 20%.");
		}else if(prog7==2){
			pr71.setToolTipText("Прогресс слова: 40%.");
		}else if(prog7==3){
			pr71.setToolTipText("Прогресс слова: 60%.");
		}else if(prog7==4){
			pr71.setToolTipText("Прогресс слова: 80%.");
		}else if(prog7==5){
			pr71.setToolTipText("Прогресс слова: 100%.");
		}    
		pr71.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr71.setVisible(false);
		
		t7.add(Box.createHorizontalStrut(200));
		t7.add(pr7);
		t7.add(pr71);
		c7.insets = new Insets(2, 0, 0, 0);
		c7.gridx = 0;
		c7.gridy = 0;
		p7.add(t7, c7);
		
		// Words in english
		JLabel words7 = new JLabel(map2.get(7));
		words7.setFont(fontwords);
		b7.add(words7);
		c7.insets = new Insets(0, 0, 3, 0);
		c7.gridx = 0;
		c7.gridy = 1;
		p7.add(b7, c7);
		
		// Words in russian
		JLabel transl7 = new JLabel(map3.get(7));
		transl7.setFont(fonttranslate);
		b72.add(transl7);
		c7.insets = new Insets(0, 0, 5, 0);
		c7.gridx = 0;
		c7.gridy = 2;
		b72.setVisible(false);
		p7.add(b72, c7);
		
		// Audio for words
		JButton audio7 = new JButton(new ImageIcon("res/images/9.png"));
		audio7.setText("");
		audio7.setBorder(null);
		audio7.setBackground(null);
		audio7.setForeground(Color.white);
		audio7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(6);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b73.add(audio7);
		c7.insets = new Insets(0, 0, 0, 0);
		c7.gridx = 0;
		c7.gridy = 3;
		p7.add(b73, c7);
		
		// Transcription for words
		JLabel transc7 = new JLabel(map4.get(7));
		transc7.setFont(fonttranslate);
		b74.add(transc7);
		c7.gridx = 0;
		c7.gridy = 4;
		c7.insets = new Insets(8, 0, 5, 0);
		p7.add( b74, c7);
		
		JLabel good7 = new JLabel(new ImageIcon("res/images/ok.png"));
		b75.add(good7);
		b75.add(Box.createHorizontalStrut(40));
		
		JButton undo7 = new JButton("На повторения");
		undo7.setBorder(null);
		undo7.setBackground(null);
		undo7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr7.setVisible(true);
				pr71.setVisible(false);
				b76.setVisible(true);
				b75.setVisible(false);
				map1.put(7, 0);
			}
		});
		b75.add(undo7);
		c7.insets = new Insets(0, 0, 0, 0);
		c7.gridx = 0;
		c7.gridy = 5;
		b75.setVisible(false);
		p7.add(b75, c7);
		
		JLabel bad7 = new JLabel(new ImageIcon("res/images/bad.png"));
		b76.add(bad7);
		c7.insets = new Insets(0, 0, 0, 170);
		c7.gridx = 0;
		c7.gridy = 6;
		b76.setVisible(false);
		p7.add(b76, c7);
		
		n7 = new JButton("Не знаю");
		n7.setBackground(Color1);
		n7.setForeground(Color.white);
		n7.setFont(fontbuttons);
		n7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n7.setVisible(false);
				y7.setVisible(false);
				b72.setVisible(true);
				b76.setVisible(true);
				map1.put(7, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(6);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b77.add(n7);
		b77.add(Box.createHorizontalStrut(30));
				
		y7 = new JButton("Я знаю!");
		y7.setBackground(Color1);
		y7.setForeground(Color.white);
		y7.setFont(fontbuttons);
		y7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr7.setVisible(false);
				pr71.setVisible(true);
				count2++;
				n7.setVisible(false);
				y7.setVisible(false);
				b72.setVisible(true);
				b75.setVisible(true);
				map1.put(7, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(6);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b77.add(y7);
		c7.insets = new Insets(5, 0, 0, 0);
		c7.gridx = 0;
		c7.gridy = 7;
		p7.add(b77, c7);
				
		// ------------------------------------------------Панель 8
		JPanel p8 = new JPanel();
		GridBagLayout gbl8 = new GridBagLayout();
		p8.setLayout(gbl8);
		p8.setBackground(Color.white);
		p8.setVisible(visible[7]);
		p8.setPreferredSize(new Dimension(240, 170));
		
		Box t8 = Box.createHorizontalBox();
		Box b8 = Box.createHorizontalBox();
		b82 = Box.createHorizontalBox();
		Box b83 = Box.createHorizontalBox();
		Box b84 = Box.createHorizontalBox();
		b85 = Box.createHorizontalBox();
		b86 = Box.createHorizontalBox();
		Box b87 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р8 
		GridBagConstraints c8 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass8 ="res/images/progress/"+map5.get(8)+".png";
		pr8 = new JLabel(new ImageIcon(prog_pass8));
		if(map5.get(8).equalsIgnoreCase("0")){
			pr8.setToolTipText("Прогресс слова: 0%.");
			prog8=1;
		}else if(map5.get(8).equalsIgnoreCase("1")){
			pr8.setToolTipText("Прогресс слова: 20%.");
			prog8=2;
		}else if(map5.get(8).equalsIgnoreCase("2")){
			pr8.setToolTipText("Прогресс слова: 40%.");
			prog8=3;
		}else if(map5.get(8).equalsIgnoreCase("3")){
			pr8.setToolTipText("Прогресс слова: 60%.");
			prog8=4;
		}else if(map5.get(8).equalsIgnoreCase("4")){
			pr8.setToolTipText("Прогресс слова: 80%.");
			prog8=5;
		}else if(map5.get(8).equalsIgnoreCase("5")){
			pr8.setToolTipText("Прогресс слова: 100%.");
			prog8=6;
		}
		pr8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr8.setVisible(true);
		
		String prog_pass81 ="res/images/progress/"+prog8+".png";
		pr81= new JLabel(new ImageIcon(prog_pass81));
		if(prog8==1){
			pr81.setToolTipText("Прогресс слова: 20%.");
		}else if(prog8==2){
			pr81.setToolTipText("Прогресс слова: 40%.");
		}else if(prog8==3){
			pr81.setToolTipText("Прогресс слова: 60%.");
		}else if(prog8==4){
			pr81.setToolTipText("Прогресс слова: 80%.");
		}else if(prog8==5){
			pr81.setToolTipText("Прогресс слова: 100%.");
		}    
		pr81.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr81.setVisible(false);
		
		t8.add(Box.createHorizontalStrut(200));
		t8.add(pr8);
		t8.add(pr81);
		c8.insets = new Insets(2, 0, 0, 0);
		c8.gridx = 0;
		c8.gridy = 0;
		p8.add(t8, c8);
		
		// Words in english
		JLabel words8 = new JLabel(map2.get(8));
		words8.setFont(fontwords);
		b8.add(words8);
		c8.insets = new Insets(0, 0, 3, 0);
		c8.gridx = 0;
		c8.gridy = 1;
		p8.add(b8, c8);
		
		// Words in russian
		JLabel transl8 = new JLabel(map3.get(8));
		transl8.setFont(fonttranslate);
		b82.add(transl8);
		c8.insets = new Insets(0, 0, 5, 0);
		c8.gridx = 0;
		c8.gridy = 2;
		b82.setVisible(false);
		p8.add(b82, c8);
		
		// Audio for words
		JButton audio8 = new JButton(new ImageIcon("res/images/9.png"));
		audio8.setText("");
		audio8.setBorder(null);
		audio8.setBackground(null);
		audio8.setForeground(Color.white);
		audio8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(7);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b83.add(audio8);
		c8.insets = new Insets(0, 0, 0, 0);
		c8.gridx = 0;
		c8.gridy = 3;
		p8.add(b83, c8);
		
		// Transcription for words
		JLabel transc8 = new JLabel(map4.get(8));
		transc8.setFont(fonttranslate);
		b84.add(transc8);
		c8.gridx = 0;
		c8.gridy = 4;
		c8.insets = new Insets(8, 0, 5, 0);
		p8.add( b84, c8);
		
		JLabel good8 = new JLabel(new ImageIcon("res/images/ok.png"));
		b85.add(good8);
		b85.add(Box.createHorizontalStrut(40));
		
		JButton undo8 = new JButton("На повторения");
		undo8.setBorder(null);
		undo8.setBackground(null);
		undo8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr8.setVisible(true);
				pr81.setVisible(false);
				b86.setVisible(true);
				b85.setVisible(false);
				map1.put(8, 0);
			}
		});
		b85.add(undo8);
		c8.insets = new Insets(0, 0, 0, 0);
		c8.gridx = 0;
		c8.gridy = 5;
		b85.setVisible(false);
		p8.add(b85, c8);
		
		JLabel bad8 = new JLabel(new ImageIcon("res/images/bad.png"));
		b86.add(bad8);
		c8.insets = new Insets(0, 0, 0, 170);
		c8.gridx = 0;
		c8.gridy = 6;
		b86.setVisible(false);
		p8.add(b86, c8);
		
		n8 = new JButton("Не знаю");
		n8.setBackground(Color1);
		n8.setForeground(Color.white);
		n8.setFont(fontbuttons);
		n8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n8.setVisible(false);
				y8.setVisible(false);
				b82.setVisible(true);
				b86.setVisible(true);
				map1.put(8, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(7);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b87.add(n8);
		b87.add(Box.createHorizontalStrut(30));
				
		y8 = new JButton("Я знаю!");
		y8.setBackground(Color1);
		y8.setForeground(Color.white);
		y8.setFont(fontbuttons);
		y8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr8.setVisible(false);
				pr81.setVisible(true);
				count2++;
				n8.setVisible(false);
				y8.setVisible(false);
				b82.setVisible(true);
				b85.setVisible(true);
				map1.put(8, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(7);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b87.add(y8);
		c8.insets = new Insets(5, 0, 0, 0);
		c8.gridx = 0;
		c8.gridy = 7;
		p8.add(b87, c8);
				
		// ------------------------------------------------Панель 9
		JPanel p9 = new JPanel();
		GridBagLayout gbl9 = new GridBagLayout();
		p9.setLayout(gbl9);
		p9.setBackground(Color.white);
		p9.setVisible(visible[8]);
		p9.setPreferredSize(new Dimension(240, 170));
		
		Box t9 = Box.createHorizontalBox();
		Box b9 = Box.createHorizontalBox();
		b92 = Box.createHorizontalBox();
		Box b93 = Box.createHorizontalBox();
		Box b94 = Box.createHorizontalBox();
		b95 = Box.createHorizontalBox();
		b96 = Box.createHorizontalBox();
		Box b97 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р9 
		GridBagConstraints c9 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass9 ="res/images/progress/"+map5.get(9)+".png";
		pr9 = new JLabel(new ImageIcon(prog_pass9));
		if(map5.get(9).equalsIgnoreCase("0")){
			pr9.setToolTipText("Прогресс слова: 0%.");
			prog9=1;
		}else if(map5.get(9).equalsIgnoreCase("1")){
			pr9.setToolTipText("Прогресс слова: 20%.");
			prog9=2;
		}else if(map5.get(9).equalsIgnoreCase("2")){
			pr9.setToolTipText("Прогресс слова: 40%.");
			prog9=3;
		}else if(map5.get(9).equalsIgnoreCase("3")){
			pr9.setToolTipText("Прогресс слова: 60%.");
			prog9=4;
		}else if(map5.get(9).equalsIgnoreCase("4")){
			pr9.setToolTipText("Прогресс слова: 90%.");
			prog9=5;
		}else if(map5.get(9).equalsIgnoreCase("5")){
			pr9.setToolTipText("Прогресс слова: 100%.");
			prog9=6;
		}
		pr9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr9.setVisible(true);
		
		String prog_pass91 ="res/images/progress/"+prog9+".png";
		pr91= new JLabel(new ImageIcon(prog_pass91));
		if(prog9==1){
			pr91.setToolTipText("Прогресс слова: 20%.");
		}else if(prog9==2){
			pr91.setToolTipText("Прогресс слова: 40%.");
		}else if(prog9==3){
			pr91.setToolTipText("Прогресс слова: 60%.");
		}else if(prog9==4){
			pr91.setToolTipText("Прогресс слова: 90%.");
		}else if(prog9==5){
			pr91.setToolTipText("Прогресс слова: 100%.");
		}    
		pr91.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr91.setVisible(false);
		
		t9.add(Box.createHorizontalStrut(200));
		t9.add(pr9);
		t9.add(pr91);
		c9.insets = new Insets(2, 0, 0, 0);
		c9.gridx = 0;
		c9.gridy = 0;
		p9.add(t9, c9);		
		
		// Words in english
		JLabel words9 = new JLabel(map2.get(9));
		words9.setFont(fontwords);
		b9.add(words9);
		c9.insets = new Insets(0, 0, 3, 0);
		c9.gridx = 0;
		c9.gridy = 1;
		p9.add(b9, c9);
		
		// Words in russian
		JLabel transl9 = new JLabel(map3.get(9));
		transl9.setFont(fonttranslate);
		b92.add(transl9);
		c9.insets = new Insets(0, 0, 5, 0);
		c9.gridx = 0;
		c9.gridy = 2;
		b92.setVisible(false);
		p9.add(b92, c9);
		
		// Audio for words
		JButton audio9 = new JButton(new ImageIcon("res/images/9.png"));
		audio9.setText("");
		audio9.setBorder(null);
		audio9.setBackground(null);
		audio9.setForeground(Color.white);
		audio9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(8);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b93.add(audio9);
		c9.insets = new Insets(0, 0, 0, 0);
		c9.gridx = 0;
		c9.gridy = 3;
		p9.add(b93, c9);
		
		// Transcription for words
		JLabel transc9 = new JLabel(map4.get(9));
		transc9.setFont(fonttranslate);
		b94.add(transc9);
		c9.gridx = 0;
		c9.gridy = 4;
		c9.insets = new Insets(8, 0, 5, 0);
		p9.add( b94, c9);
		
		JLabel good9 = new JLabel(new ImageIcon("res/images/ok.png"));
		b95.add(good9);
		b95.add(Box.createHorizontalStrut(40));
		
		JButton undo9 = new JButton("На повторения");
		undo9.setBorder(null);
		undo9.setBackground(null);
		undo9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr9.setVisible(true);
				pr91.setVisible(false);
				b96.setVisible(true);
				b95.setVisible(false);
				map1.put(9, 0);
			}
		});
		b95.add(undo9);
		c9.insets = new Insets(0, 0, 0, 0);
		c9.gridx = 0;
		c9.gridy = 5;
		b95.setVisible(false);
		p9.add(b95, c9);
		
		JLabel bad9 = new JLabel(new ImageIcon("res/images/bad.png"));
		b96.add(bad9);
		c9.insets = new Insets(0, 0, 0, 170);
		c9.gridx = 0;
		c9.gridy = 6;
		b96.setVisible(false);
		p9.add(b96, c9);
		
		n9 = new JButton("Не знаю");
		n9.setBackground(Color1);
		n9.setForeground(Color.white);
		n9.setFont(fontbuttons);
		n9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n9.setVisible(false);
				y9.setVisible(false);
				b92.setVisible(true);
				b96.setVisible(true);
				map1.put(9, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(8);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b97.add(n9);
		b97.add(Box.createHorizontalStrut(30));
				
		y9 = new JButton("Я знаю!");
		y9.setBackground(Color1);
		y9.setForeground(Color.white);
		y9.setFont(fontbuttons);
		y9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr9.setVisible(false);
				pr91.setVisible(true);
				count2++;
				n9.setVisible(false);
				y9.setVisible(false);
				b92.setVisible(true);
				b95.setVisible(true);
				map1.put(9, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(8);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b97.add(y9);
		c9.insets = new Insets(5, 0, 0, 0);
		c9.gridx = 0;
		c9.gridy = 7;
		p9.add(b97, c9);
		
		// ------------------------------------------------Панель 10
		JPanel p10 = new JPanel();
		GridBagLayout gbl10 = new GridBagLayout();
		p10.setLayout(gbl10);
		p10.setBackground(Color.white);
		p10.setVisible(visible[9]);
		p10.setPreferredSize(new Dimension(240, 170));
		
		Box t10 = Box.createHorizontalBox();
		Box b10 = Box.createHorizontalBox();
		b102 = Box.createHorizontalBox();
		Box b103 = Box.createHorizontalBox();
		Box b104 = Box.createHorizontalBox();
		b105 = Box.createHorizontalBox();
		b106 = Box.createHorizontalBox();
		Box b107 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р10 
		GridBagConstraints c10 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass10 ="res/images/progress/"+map5.get(10)+".png";
		pr10 = new JLabel(new ImageIcon(prog_pass10));
		if(map5.get(10).equalsIgnoreCase("0")){
			pr10.setToolTipText("Прогресс слова: 0%.");
			prog10=1;
		}else if(map5.get(10).equalsIgnoreCase("1")){
			pr10.setToolTipText("Прогресс слова: 20%.");
			prog10=2;
		}else if(map5.get(10).equalsIgnoreCase("2")){
			pr10.setToolTipText("Прогресс слова: 40%.");
			prog10=3;
		}else if(map5.get(10).equalsIgnoreCase("3")){
			pr10.setToolTipText("Прогресс слова: 60%.");
			prog10=4;
		}else if(map5.get(10).equalsIgnoreCase("4")){
			pr10.setToolTipText("Прогресс слова: 100%.");
			prog10=5;
		}else if(map5.get(10).equalsIgnoreCase("5")){
			pr10.setToolTipText("Прогресс слова: 100%.");
			prog10=6;
		}
		pr10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr10.setVisible(true);
		
		String prog_pass101 ="res/images/progress/"+prog10+".png";
		pr101= new JLabel(new ImageIcon(prog_pass101));
		if(prog10==1){
			pr101.setToolTipText("Прогресс слова: 20%.");
		}else if(prog10==2){
			pr101.setToolTipText("Прогресс слова: 40%.");
		}else if(prog10==3){
			pr101.setToolTipText("Прогресс слова: 60%.");
		}else if(prog10==4){
			pr101.setToolTipText("Прогресс слова: 100%.");
		}else if(prog10==5){
			pr101.setToolTipText("Прогресс слова: 100%.");
		}    
		pr101.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr101.setVisible(false);
		
		t10.add(Box.createHorizontalStrut(200));
		t10.add(pr10);
		t10.add(pr101);
		c10.insets = new Insets(2, 0, 0, 0);
		c10.gridx = 0;
		c10.gridy = 0;
		p10.add(t10, c10);	
		
		// Words in english
		JLabel words10 = new JLabel(map2.get(10));
		words10.setFont(fontwords);
		b10.add(words10);
		c10.insets = new Insets(0, 0, 3, 0);
		c10.gridx = 0;
		c10.gridy = 1;
		p10.add(b10, c10);
		
		// Words in russian
		JLabel transl10 = new JLabel(map3.get(10));
		transl10.setFont(fonttranslate);
		b102.add(transl10);
		c10.insets = new Insets(0, 0, 5, 0);
		c10.gridx = 0;
		c10.gridy = 2;
		b102.setVisible(false);
		p10.add(b102, c10);
		
		// Audio for words
		JButton audio10 = new JButton(new ImageIcon("res/images/9.png"));
		audio10.setText("");
		audio10.setBorder(null);
		audio10.setBackground(null);
		audio10.setForeground(Color.white);
		audio10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(9);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b103.add(audio10);
		c10.insets = new Insets(0, 0, 0, 0);
		c10.gridx = 0;
		c10.gridy = 3;
		p10.add(b103, c10);
		
		// Transcription for words
		JLabel transc10 = new JLabel(map4.get(10));
		transc10.setFont(fonttranslate);
		b104.add(transc10);
		c10.gridx = 0;
		c10.gridy = 4;
		c10.insets = new Insets(8, 0, 5, 0);
		p10.add( b104, c10);
		
		JLabel good10 = new JLabel(new ImageIcon("res/images/ok.png"));
		b105.add(good10);
		b105.add(Box.createHorizontalStrut(40));
		
		JButton undo10 = new JButton("На повторения");
		undo10.setBorder(null);
		undo10.setBackground(null);
		undo10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr10.setVisible(true);
				pr101.setVisible(false);
				b106.setVisible(true);
				b105.setVisible(false);
				map1.put(10, 0);
			}
		});
		b105.add(undo10);
		c10.insets = new Insets(0, 0, 0, 0);
		c10.gridx = 0;
		c10.gridy = 5;
		b105.setVisible(false);
		p10.add(b105, c10);
		
		JLabel bad10 = new JLabel(new ImageIcon("res/images/bad.png"));
		b106.add(bad10);
		c10.insets = new Insets(0, 0, 0, 170);
		c10.gridx = 0;
		c10.gridy = 6;
		b106.setVisible(false);
		p10.add(b106, c10);
		
		n10 = new JButton("Не знаю");
		n10.setBackground(Color1);
		n10.setForeground(Color.white);
		n10.setFont(fontbuttons);
		n10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n10.setVisible(false);
				y10.setVisible(false);
				b102.setVisible(true);
				b106.setVisible(true);
				map1.put(10, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(9);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b107.add(n10);
		b107.add(Box.createHorizontalStrut(30));
				
		y10 = new JButton("Я знаю!");
		y10.setBackground(Color1);
		y10.setForeground(Color.white);
		y10.setFont(fontbuttons);
		y10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr10.setVisible(false);
				pr101.setVisible(true);
				count2++;
				n10.setVisible(false);
				y10.setVisible(false);
				b102.setVisible(true);
				b105.setVisible(true);
				map1.put(10, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(9);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b107.add(y10);
		c10.insets = new Insets(5, 0, 0, 0);
		c10.gridx = 0;
		c10.gridy = 7;
		p10.add(b107, c10);
				
		// ------------------------------------------------Панель 11
		JPanel p11 = new JPanel();
		GridBagLayout gbl11 = new GridBagLayout();
		p11.setLayout(gbl11);
		p11.setBackground(Color.white);
		p11.setVisible(visible[10]);
		p11.setPreferredSize(new Dimension(240, 170));
		
		Box t11 = Box.createHorizontalBox();
		Box b11 = Box.createHorizontalBox();
		b112 = Box.createHorizontalBox();
		Box b113 = Box.createHorizontalBox();
		Box b114 = Box.createHorizontalBox();
		b115 = Box.createHorizontalBox();
		b116 = Box.createHorizontalBox();
		Box b117 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р11 
		GridBagConstraints c11 = new GridBagConstraints();
			
		// Прогрес изученого слова
		String prog_pass11_1 ="res/images/progress/"+map5.get(11)+".png";
		pr11_1 = new JLabel(new ImageIcon(prog_pass11_1));
		if(map5.get(11).equalsIgnoreCase("0")){
			pr11_1.setToolTipText("Прогресс слова: 0%.");
			prog11=1;
		}else if(map5.get(11).equalsIgnoreCase("1")){
			pr11_1.setToolTipText("Прогресс слова: 20%.");
			prog11=2;
		}else if(map5.get(11).equalsIgnoreCase("2")){
			pr11_1.setToolTipText("Прогресс слова: 40%.");
			prog11=3;
		}else if(map5.get(11).equalsIgnoreCase("3")){
			pr11_1.setToolTipText("Прогресс слова: 60%.");
			prog11=4;
		}else if(map5.get(11).equalsIgnoreCase("4")){
			pr11_1.setToolTipText("Прогресс слова: 80%.");
			prog11=5;
		}else if(map5.get(11).equalsIgnoreCase("5")){
			pr11_1.setToolTipText("Прогресс слова: 100%.");
			prog11=6;
		}
		pr11_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr11_1.setVisible(true);
		
		String prog_pass11_2 ="res/images/progress/"+prog11+".png";
		pr11_2= new JLabel(new ImageIcon(prog_pass11_2));
		if(prog11==1){
			pr11_2.setToolTipText("Прогресс слова: 20%.");
		}else if(prog11==2){
			pr11_2.setToolTipText("Прогресс слова: 40%.");
		}else if(prog11==3){
			pr11_2.setToolTipText("Прогресс слова: 60%.");
		}else if(prog11==4){
			pr11_2.setToolTipText("Прогресс слова: 80%.");
		}else if(prog11==5){
			pr11_2.setToolTipText("Прогресс слова: 100%.");
		}    
		pr11_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr11_2.setVisible(false);
		
		t11.add(Box.createHorizontalStrut(200));
		t11.add(pr11_1);
		t11.add(pr11_2);
		c11.insets = new Insets(2, 0, 0, 0);
		c11.gridx = 0;
		c11.gridy = 0;
		p11.add(t11, c11);		
		
		// Words in english
		JLabel words11 = new JLabel(map2.get(11));
		words11.setFont(fontwords);
		b11.add(words11);
		c11.insets = new Insets(0, 0, 3, 0);
		c11.gridx = 0;
		c11.gridy = 1;
		p11.add(b11, c11);
		
		// Words in russian
		JLabel transl11 = new JLabel(map3.get(11));
		transl11.setFont(fonttranslate);
		b112.add(transl11);
		c11.insets = new Insets(0, 0, 5, 0);
		c11.gridx = 0;
		c11.gridy = 2;
		b112.setVisible(false);
		p11.add(b112, c11);
		
		// Audio for words
		JButton audio11 = new JButton(new ImageIcon("res/images/9.png"));
		audio11.setText("");
		audio11.setBorder(null);
		audio11.setBackground(null);
		audio11.setForeground(Color.white);
		audio11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(10);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		b113.add(audio11);
		c11.insets = new Insets(0, 0, 0, 0);
		c11.gridx = 0;
		c11.gridy = 3;
		p11.add(b113, c11);
		
		// Transcription for words
		JLabel transc11 = new JLabel(map4.get(11));
		transc11.setFont(fonttranslate);
		b114.add(transc11);
		c11.gridx = 0;
		c11.gridy = 4;
		c11.insets = new Insets(8, 0, 5, 0);
		p11.add( b114, c11);
		
		JLabel good11 = new JLabel(new ImageIcon("res/images/ok.png"));
		b115.add(good11);
		b115.add(Box.createHorizontalStrut(40));
		
		JButton undo11 = new JButton("На повторения");
		undo11.setBorder(null);
		undo11.setBackground(null);
		undo11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr11_1.setVisible(true);
				pr11_2.setVisible(false);
				b116.setVisible(true);
				b115.setVisible(false);
				map1.put(11, 0);
			}
		});
		b115.add(undo11);
		c11.insets = new Insets(0, 0, 0, 0);
		c11.gridx = 0;
		c11.gridy = 5;
		b115.setVisible(false);
		p11.add(b115, c11);
		
		JLabel bad11 = new JLabel(new ImageIcon("res/images/bad.png"));
		b116.add(bad11);
		c11.insets = new Insets(0, 0, 0, 170);
		c11.gridx = 0;
		c11.gridy = 6;
		b116.setVisible(false);
		p11.add(b116, c11);
		
		n11 = new JButton("Не знаю");
		n11.setBackground(Color1);
		n11.setForeground(Color.white);
		n11.setFont(fontbuttons);
		n11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n11.setVisible(false);
				y11.setVisible(false);
				b112.setVisible(true);
				b116.setVisible(true);
				map1.put(11, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(10);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b117.add(n11);
		b117.add(Box.createHorizontalStrut(30));
				
		y11 = new JButton("Я знаю!");
		y11.setBackground(Color1);
		y11.setForeground(Color.white);
		y11.setFont(fontbuttons);
		y11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr11_1.setVisible(false);
				pr11_2.setVisible(true);
				count2++;
				n11.setVisible(false);
				y11.setVisible(false);
				b112.setVisible(true);
				b115.setVisible(true);
				map1.put(11, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(10);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b117.add(y11);
		c11.insets = new Insets(5, 0, 0, 0);
		c11.gridx = 0;
		c11.gridy = 7;
		p11.add(b117, c11);
				
		// ------------------------------------------------Панель 12
		JPanel p12 = new JPanel();
		GridBagLayout gbl12 = new GridBagLayout();
		p12.setLayout(gbl12);
		p12.setBackground(Color.white);
		p12.setVisible(visible[11]);
		p12.setPreferredSize(new Dimension(240, 170));
		
		Box t12 = Box.createHorizontalBox();
		Box b12 = Box.createHorizontalBox();
		b122 = Box.createHorizontalBox();
		Box b123 = Box.createHorizontalBox();
		Box b124 = Box.createHorizontalBox();
		b125 = Box.createHorizontalBox();
		b126 = Box.createHorizontalBox();
		Box b127 = Box.createHorizontalBox();
		
		// Добавляем елементы на панель р12 
		GridBagConstraints c12 = new GridBagConstraints();
		
		// Прогрес изученого слова
		String prog_pass12 ="res/images/progress/"+map5.get(12)+".png";
		pr12 = new JLabel(new ImageIcon(prog_pass12));
		if(map5.get(12).equalsIgnoreCase("0")){
			pr12.setToolTipText("Прогресс слова: 0%.");
			prog12=1;
		}else if(map5.get(12).equalsIgnoreCase("1")){
			pr12.setToolTipText("Прогресс слова: 20%.");
			prog12=2;
		}else if(map5.get(12).equalsIgnoreCase("2")){
			pr12.setToolTipText("Прогресс слова: 40%.");
			prog12=3;
		}else if(map5.get(12).equalsIgnoreCase("3")){
			pr12.setToolTipText("Прогресс слова: 60%.");
			prog12=4;
		}else if(map5.get(12).equalsIgnoreCase("4")){
			pr12.setToolTipText("Прогресс слова: 120%.");
			prog12=5;
		}else if(map5.get(12).equalsIgnoreCase("5")){
			pr12.setToolTipText("Прогресс слова: 100%.");
			prog12=6;
		}
		pr12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr12.setVisible(true);
		
		String prog_pass121 ="res/images/progress/"+prog12+".png";
		pr121= new JLabel(new ImageIcon(prog_pass121));
		if(prog12==1){
			pr121.setToolTipText("Прогресс слова: 20%.");
		}else if(prog12==2){
			pr121.setToolTipText("Прогресс слова: 40%.");
		}else if(prog12==3){
			pr121.setToolTipText("Прогресс слова: 60%.");
		}else if(prog12==4){
			pr121.setToolTipText("Прогресс слова: 120%.");
		}else if(prog12==5){
			pr121.setToolTipText("Прогресс слова: 100%.");
		}    
		pr121.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr121.setVisible(false);
		
		t12.add(Box.createHorizontalStrut(200));
		t12.add(pr12);
		t12.add(pr121);
		c12.insets = new Insets(2, 0, 0, 0);
		c12.gridx = 0;
		c12.gridy = 0;
		p12.add(t12, c12);	
		
		// Words in english
		JLabel words12 = new JLabel(map2.get(12));
		words12.setFont(fontwords);
		b12.add(words12);
		c12.insets = new Insets(0, 0, 3, 0);
		c12.gridx = 0;
		c12.gridy = 1;
		p12.add(b12, c12);
		
		// Words in russian
		JLabel transl12 = new JLabel(map3.get(12));
		transl12.setFont(fonttranslate);
		b122.add(transl12);
		c12.insets = new Insets(0, 0, 5, 0);
		c12.gridx = 0;
		c12.gridy = 2;
		b122.setVisible(false);
		p12.add(b122, c12);
		
		// Audio for words
		JButton audio12 = new JButton(new ImageIcon("res/images/9.png"));
		audio12.setText("");
		audio12.setBorder(null);
		audio12.setBackground(null);
		audio12.setForeground(Color.white);
		audio12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					audioMP3(11);
					} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}					
			}});
		b123.add(audio12);
		c12.insets = new Insets(0, 0, 0, 0);
		c12.gridx = 0;
		c12.gridy = 3;
		p12.add(b123, c12);
		
		// Transcription for words
		JLabel transc12 = new JLabel(map4.get(12));
		transc12.setFont(fonttranslate);
		b124.add(transc12);
		c12.gridx = 0;
		c12.gridy = 4;
		c12.insets = new Insets(8, 0, 5, 0);
		p12.add( b124, c12);
		
		JLabel good12 = new JLabel(new ImageIcon("res/images/ok.png"));
		b125.add(good12);
		b125.add(Box.createHorizontalStrut(40));
		
		JButton undo12 = new JButton("На повторения");
		undo12.setBorder(null);
		undo12.setBackground(null);
		undo12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		undo12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr12.setVisible(true);
				pr121.setVisible(false);
				b126.setVisible(true);
				b125.setVisible(false);
				map1.put(12, 0);
			}
		});
		b125.add(undo12);
		c12.insets = new Insets(0, 0, 0, 0);
		c12.gridx = 0;
		c12.gridy = 5;
		b125.setVisible(false);
		p12.add(b125, c12);
		
		JLabel bad12 = new JLabel(new ImageIcon("res/images/bad.png"));
		b126.add(bad12);
		c12.insets = new Insets(0, 0, 0, 170);
		c12.gridx = 0;
		c12.gridy = 6;
		b126.setVisible(false);
		p12.add(b126, c12);
		
		n12 = new JButton("Не знаю");
		n12.setBackground(Color1);
		n12.setForeground(Color.white);
		n12.setFont(fontbuttons);
		n12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		n12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count1++;
				n12.setVisible(false);
				y12.setVisible(false);
				b122.setVisible(true);
				b126.setVisible(true);
				map1.put(12, 0);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(11);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b127.add(n12);
		b127.add(Box.createHorizontalStrut(30));
				
		y12 = new JButton("Я знаю!");
		y12.setBackground(Color1);
		y12.setForeground(Color.white);
		y12.setFont(fontbuttons);
		y12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		y12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pr12.setVisible(false);
				pr121.setVisible(true);
				count2++;
				n12.setVisible(false);
				y12.setVisible(false);
				b122.setVisible(true);
				b125.setVisible(true);
				map1.put(12, 1);
				if (count1+count2==map1.size()&& map1.size()==dl1){
					stop.setVisible(true);
				}
				try {
					audioMP3(11);
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		b127.add(y12);
		c12.insets = new Insets(5, 0, 0, 0);
		c12.gridx = 0;
		c12.gridy = 7;
		p12.add(b127, c12);
		
		// Button back
		JButton back = new JButton(new ImageIcon("res/images/back.png"));
		back.setToolTipText("Вернуться обратно к тренировкам");
		back.setBorder(null);
		back.setBackground(null);
		back.setForeground(Color.white);
		back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				Object[] options = {"Да, вернуться", "Остаться на странице"};
				int message = JOptionPane.showOptionDialog(null,
						"Если вы вернётесь к списку, результаты тренировки не будут сохранены."+
						"Вернуться к списку тренировок?",
						"Тренировка не закончена",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						new ImageIcon("res/images/warning.png"),     
						options,  //the titles of buttons
						options[1]); //default button title
				
				 if (message == JOptionPane.NO_OPTION) {
				     
				 } else if (message == JOptionPane.YES_OPTION) {
					 try {
							MainWindow m = new MainWindow();
							if(item==null){
								m.showTrainingPanel(item1);
								m.createAndShowGUI();
							}else{
								m.showTrainingPanelAll(item,item1);
								m.createAndShowGUI();
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} 
				 }
											
		}});
		box.add(Box.createHorizontalStrut(20));
		box.add(back);
		box.add(Box.createHorizontalStrut(300));
				
		Font fonttraining = new Font("Arial", Font.PLAIN, 24);
		JLabel nametraining = new JLabel("Словарные карточки");
		nametraining.setFont(fonttraining);
		box.add(nametraining);
		box.add(Box.createHorizontalStrut(130));
				
		stop = new JButton("Завершить тренировку");
		stop.setBackground(Color1);
		stop.setForeground(Color.white);
		stop.setFont(fontbuttons);
		stop.setVisible(false);
		stop.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow m;
				try {
					m = new MainWindow();
					m.showResultPanel(words, map, map1,item,item1,training);
					m.createAndShowGUI();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		box.add(Box.createHorizontalStrut(50));
		box.add(stop);
				
		p.add(box);
		pp.add(p1);
		pp.add(p2);
		pp.add(p3);
		pp.add(p4);
		if (dl>4){
			pp.add(box1);	
		}
		pp.add(p5);
		pp.add(p6);
		pp.add(p7);
		pp.add(p8);
		if (dl>4){
			pp.add(box2);	
		}
		pp.add(p9);
		pp.add(p10);
		pp.add(p11);
		pp.add(p12);
		
		add("North", p);
		add("Center", pp);
	}
	
	// Вызов озвучки к слову mp3
		public void audioMP3(int i) throws Throwable {
		
			FileInputStream fis = new FileInputStream(words[i][3]);
			final Player playMP3 = new Player(fis);
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						playMP3.play();
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}});
		}
}
