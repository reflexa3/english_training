package training;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import javazoom.jl.player.Player;
import windows.MainWindow;

public class Crossword extends JPanel {

	private static final long serialVersionUID = 4826216375579021293L;
	JButton[] numButtons = new JButton[500];
	int q; // счетчик которой идет по буквам в первом слове
	int ok2=0; //позиция пересечения первого и второго слова
	int ok3=0; //позиция пересечени третьего слова
	int ok4=0; //позиция пересечения четвертого слова
	int w; //последняя буква которая совпала во втором слове с первым словом, по этому индексу движемся вниз  
	int w1; //последняя буква которая совпала во втором слове с первым словом, по этому индексу движемся вверх
	int dl1,dl2,dl3,dl4; // длина 1..12
	String s1,s2,s3,s4; //words 1..12 
	int down2,up2,down3,up3,left3,right3,lim3,max3,down4,up4,left4,right4,lim4,max4;
	String temp3[];
	String temp4[];
	int stop=0;
	String[][] words; 
	Map<Integer, String> steck = new HashMap<Integer, String>();
	Color C1 = new Color(127,255,0);
	Color C2 = new Color(255,127,80);
	Color C3 = new Color(0,255,255);
	Color C4 = new Color(169,169,169);
	Color C5 = new Color(153,50,204);
	Color C6 = new Color(255,20,147);
	Color C7 = new Color(248,248,255);
	Color C8 = new Color(224,255,255);
	Color C9 = new Color(255,228,181);
	Color C10 = new Color(255,0,0);
	Color C11 = new Color(255,255,0);
	Color C12 = new Color(210,180,140);
		
	public Crossword(int k, String[][] data, Map<Integer, String> map, Map<Integer, Integer> map1, String item, String item1, int prog, int k1, String training) {
		
		words=data;
		
		System.out.println();
		System.out.println();
		
	for (int k15=0; k15<12; k15++){
//			System.out.println(k11+" "+words[k11][0]+" "+words[k11][1]+" "+words[k11][2]+" "+words[k11][3]+" "+words[k11][4]+" "+words[k11][5]);
		System.out.println(k15+" "+words[k15][0]);
	}
		
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1100, 605));
		Border line = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		
		// Область где будет прогрес тренровки (North p1)
		Color yellow = new Color(250, 194, 90); // yellow
		JPanel p1 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p1.setPreferredSize(new Dimension(1100, 50));
		p1.setLayout(fl);
		p1.setBackground(yellow);
		
		// Область где будет прогрес тренровки (Center p2)
		JPanel p2 = new JPanel();
		GridLayout gl = new GridLayout(20, 20, 0, 0);
		p2.setLayout(gl);
		p2.setBorder(line);
		
		// Область где будет прогрес тренровки (West p3)
		JPanel p3 = new JPanel();
		FlowLayout fl3 = new FlowLayout();
		p3.setPreferredSize(new Dimension(250, 565));
		p3.setLayout(fl3);
		p3.setBorder(line);
				
		Font fontbuttons = new Font("Tahoma", Font.PLAIN, 15);
		Color grey = new Color(221, 221, 221); // grey
		
		// Добавляем пустые кнопки
		for (int i = 0; i<400; i++){
			numButtons[i] = new JButton();
			numButtons[i].setBackground(grey);
			numButtons[i].setFont(fontbuttons);
			p2.add(numButtons[i]);
		}
		//--------------------------------------------------------------------FIRST WORDS--------
		
		s1 = words[0][0];// получаем первое слово и его длину
		dl1 = s1.length();
		System.out.println("1>>" +words[0][0]);
		
		if (dl1>12 || dl1<7){
			for (int wordslength=1; wordslength<words.length; wordslength++){
				if (dl1>12 || dl1<7){
					//System.out.println("ERROR FIRST WORDS (Big length)  "+dl1+" "+words[wordslength][0]);
					s1 = words[wordslength][0];// получаем повторно первое слово и его длину
					dl1 = s1.length();
					System.out.println("1>>" +words[wordslength][0]);
				}
				
			}
		}
		
		steck.put(1, s1);
		String temp1[] = new String[dl1];// разбиваем на буквы слово
		temp1 = s1.split("");
								
		q=1;
		for (int i = 148; i<148+dl1; i++){
			if (i==148){
				numButtons[i].setText(temp1[q]);
				numButtons[i].setBackground(C1);
			}else{
				numButtons[i].setText(temp1[q]);
				numButtons[i].setBackground(C1);
				}q++;
			}			
				
		//-------------------------------------------------------------------SECOND WORDS----------
		stop=0;
		for (int wordslength2=0; wordslength2<words.length; wordslength2++){
			if (ok2!=0 &&  stop==1){
				System.out.println("STOP");
				steck.put(2, s2);
				break;
			}
			System.out.println("2>> "+words[wordslength2][0]);
			s2 = words[wordslength2][0];// получаем слово и его длину
			dl2 = s2.length();
			
			if ( dl2<5){
				s2="stop";
				//System.out.println("Little length   "+dl2);
			}
			
			// проверка первого и второго слова по горизонтали
			if(ok2==0 && s1!=s2 && s2!="stop"){
				String temp2[] = new String[dl2];// разбиваем на буквы слово
				temp2 = s2.split("");
				System.out.println(">>Second words (1 vs 2) - "+words[wordslength2][0]);
				for (int i = 1; i<dl2; i++){
					for (int j = 148; j<148+dl1; j++){
						if (ok2!=0){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp2[i])){
							//numButtons[j].setBackground(Color.red);
							w=i;
							w1=i;
							ok2=j;
						}
					}
				}
				
				down2=ok2+((dl2-w)*20); // граница вниз по вертикали
				up2=ok2-((w1-1)*20); // граница вверх по вертикали
				if (ok2!=0 && up2<8 || down2>=400){
					ok2=0;
					System.out.println("Выход за пределы границы, Верх граница="+up2+", Низ граница="+down2);
				}
				
				//Проверка границ построения второго слова по вертикали
				if (ok2!=0){
					prov_vert(up2,down2,ok2,temp2,w,w1,C2);
				}
				if (stop!=1){
					ok2=0;
				}
			}
		}
		
		//-------------------------------------------------------------------THIRD WORDS----------
		stop=0;
		for (int wordslength3=0; wordslength3<words.length; wordslength3++){
			if (ok3!=0 && stop==1){ 
				System.out.println("STOP");
				steck.put(3, s3);
				break;
			}
			System.out.println("3>> "+words[wordslength3][0]);	
			s3 = words[wordslength3][0];// получаем слово и его длину
			
//			// проверка 1 vs 3 слова по горизонтали
//			if(ok3==0 && s1!=s3 && s2!=s3){
//				dl3 = s3.length();
//				temp3 = new String[dl3];// разбиваем на буквы слово
//				temp3 = s3.split("");
//				System.out.println(">>Third words (1 vs 3) - "+words[wordslength3][0]);
//				for (int i = 1; i<dl3; i++){
//					for (int j = 148; j<148+dl1; j++){
//						if (ok3!=0 && ok3!=ok2 && ok2+1!=ok3 && ok2-1!=ok3){
//							break;
//						}
//						if (numButtons[j].getText().equalsIgnoreCase(temp3[i])){
//							w=i;
//							w1=i;
//							int tmp=j;
//							if (tmp!=ok2 && ok2+1!=tmp && ok2-1!=tmp){
//								ok3=j;
//								//numButtons[j].setBackground(Color.pink);
//							}
//						}
//					}
//				}
//				
//				down3=ok3+((dl3-w)*20); // граница вниз по вертикали
//				up3=ok3-((w1-1)*20); // граница вверх по вертикали
//				if (ok3!=0 && up3<8 || down3>=400){
//					ok3=0;
//					System.out.println("Выход за пределы границы, Верх граница="+up3+", Низ граница="+down3);
//				}
//								
//				//Проверка границ построения третьего слова по вертикали
//				prov_vert(up3,down3,ok3,temp3,w,w1,C3);
//			}
			
			// проверка 2 vs 3 слова по вертикали
			if (ok3==0 && s1!=s3 && s2!=s3){
				dl3 = s3.length();
				temp3 = new String[dl3];
				temp3 = s3.split("");
				System.out.println(">>Third words (2 vs 3) - "+words[wordslength3][0]);
				for (int i = 1; i<dl3; i++){
					for (int j = up2; j<=down2;){
						if (ok3!=0 && ok3!=ok2 && ok2+20!=ok3 && ok2-20!=ok3){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp3[i])){
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok2 && ok2+20!=tmp && ok2-20!=tmp){
								ok3=j;
								//numButtons[j].setBackground(Color.pink);
							}
						}j=j+20;
					}
				}
				int a_temp3=w-1;
				left3= ok3-a_temp3; // граница влево по горизонтали
				right3= ok3+(dl3-w1); // граница вправо по горизонтали
				
				if (ok3!=0){
					for(int j=0; j<400;){
						if(left3>=j && right3<=j+19){
							stop=1;
							lim3=j;
							max3=j+19;
							System.out.println("stop="+stop+" j="+j);
							break;
						}
					j=j+20;
					}
				}
				
				if (ok3!=0 && stop==0){
					System.out.println("BIG LENGTH  left3="+left3+" right3="+right3);
					ok3=0;
				}

				//Проверка границ построения третьего слова по горизонтали
				prov_horiz(left3,right3,lim3,max3,ok3,temp3,w,w1,C3);
				if (stop!=1){
					ok3=0;
				}
			}
		}
				
		//-------------------------------------------------------------------FOURTH WORDS----------
		stop=0;
		for (int wordslength4=0; wordslength4<words.length; wordslength4++){
			if (ok4!=0 && stop==1){ 
				System.out.println("STOP");
				steck.put(4, s4);
				break;
			}
			System.out.println("4>> "+words[wordslength4][0]);	
			s4 = words[wordslength4][0];
			
			// проверка 1 vs 4 слова по горизонтали
			if(ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
				dl4 = s4.length();
				temp4 = new String[dl4];
				temp4 = s4.split("");
				System.out.println(">>Fourth words (1 vs 4) - "+words[wordslength4][0]);
				for (int i = 1; i<dl4; i++){
					for (int j = 148; j<148+dl1; j++){
						if (ok4!=0 && ok4!=ok2 && ok2+1!=ok4 && ok2-1!=ok4 && ok3!=ok4 && ok3+1!=ok4 && ok3-1!=ok4){
							break;
						}
						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
							w=i;
							w1=i;
							int tmp=j;
							if (tmp!=ok2 && ok2+1!=tmp && ok2-1!=tmp && tmp!=ok3 && ok3+1!=tmp && ok3-1!=tmp){
								ok4=j;
								//numButtons[j].setBackground(Color.yellow);
							}
						}
					}
				}
				
				down4=ok4+((dl4-w)*20); // граница вниз по вертикали
				up4=ok4-((w1-1)*20); // граница вверх по вертикали
				if (ok4!=0 && up4<0 || down4>=400){
					ok4=0;
					System.out.println("Выход за пределы границы, Верх граница="+up4+", Низ граница="+down4);
				}

				//Проверка границ построения 4 слова по вертикали
				if (ok4!=0){
					prov_vert(up4,down4,ok4,temp4,w,w1,C4);
				}
				if (stop!=1){
					ok4=0;
				}
			}
			
		    //проверка 2 vs 4 слова по вертикали
//			if (ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
//				dl4 = s4.length();
//				temp4 = new String[dl4];
//				temp4 = s4.split("");
//				System.out.println(">>Fourth words (2 vs 4) - "+words[wordslength4][0]);
//				for (int i = 1; i<dl4; i++){
//					for (int j = up2; j<=down2;){
//						if (ok4!=0 && ok4!=ok2 && ok2+20!=ok4 && ok2-20!=ok4){
//							break;
//						}
//						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
//							w=i;
//							w1=i;
//							int tmp=j;
//							if (tmp!=ok2 && ok2+20!=tmp && ok2-20!=tmp){
//								ok4=j;
//								//numButtons[j].setBackground(Color.yellow);
//							}
//						}j=j+20;
//					}
//				}
//				int a_temp4=w-1;
//				left4= ok4-a_temp4; // граница влево по горизонтали
//				right4= ok4+(dl4-w1); // граница вправо по горизонтали
//				
//				//System.out.println("left4="+left4+" right4="+right4+"  up2="+up2+" down2="+down2+"  ok4="+ok4);
//				if (ok4!=0){
//					for(int j=0; j<400;){
//						if(left4>=j && right4<=j+19){
//							stop=1;
//							lim4=j;
//							max4=j+19;
//							System.out.println("stop="+stop+" j="+j);
//							break;
//						}
//					j=j+20;
//					}
//				}
//								
//				if (ok4!=0 && stop==0){
//					System.out.println("BIG LENGTH  left4="+left4+" right4="+right4);
//					ok4=0;
//				}
//
//				//Проверка границ построения третьего слова по горизонтали
//				prov_horiz(left4,right4,lim4,max4,ok4,temp4,w,w1,C4);
//				if (stop!=1){
//					ok4=0;
//				}
//			}
			
//			// проверка 3 vs 4 слова по вертикали
//			if (ok4==0 && s1!=s4 && s2!=s4 && s3!=s4){
//				dl4 = s4.length();
//				temp4 = new String[dl4];
//				temp4 = s4.split("");
//				System.out.println(">>Fourth words (3 vs 4) - "+words[wordslength4][0]);
//				for (int i = 1; i<dl4; i++){
//					for (int j = up3; j<=down3;){
//						if (ok4!=0 && ok4!=ok3 && ok3+20!=ok4 && ok3-20!=ok4){
//							break;
//						}
//						if (numButtons[j].getText().equalsIgnoreCase(temp4[i])){
//							w=i;
//							w1=i;
//							int tmp=j;
//							if (tmp!=ok3 && ok3+20!=tmp && ok3-20!=tmp){
//								ok4=j;
//								//numButtons[j].setBackground(Color.yellow);
//							}
//						}j=j+20;
//					}
//				}
//				int a_temp4=w-1;
//				left4= ok4-a_temp4; // граница влево по горизонтали
//				right4= ok4+(dl4-w1); // граница вправо по горизонтали
//				
//				//System.out.println("left4="+left4+" right4="+right4+"  up2="+up2+" down2="+down2+"  ok4="+ok4);
//				if (ok4!=0){
//					for(int j=0; j<400;){
//						if(left4>=j && right4<=j+19){
//							stop=1;
//							lim4=j;
//							max4=j+19;
//							System.out.println("stop="+stop+" j="+j);
//							break;
//						}
//					j=j+20;
//					}
//				}
//				
//				
//				if (ok4!=0 && stop==0){
//					System.out.println("BIG LENGTH  left4="+left4+" right4="+right4);
//					ok4=0;
//				}
//
//				//Проверка границ построения третьего слова по горизонтали
//				prov_horiz(left4,right4,lim4,max4,ok4,temp4,w,w1,C4);
//				if (stop!=1){
//					ok4=0;
//				}
//			}
		}
		
		add("North",p1);
		add("Center",p2);
		add("West",p3);
	}
	
	//Проверка границ построения по горизонтали
	public void prov_horiz(int left, int right, int lim, int max, int ok, String word[], int a, int b, Color color){

		ArrayList<Integer> steck1 = new ArrayList<Integer>();
		int c=0;
		int ok1=0;
		int ok2=0;
		for (int j = lim; j<=max; j++){//Поиск ban ячееек
			if (true != numButtons[j].getText().isEmpty()){
				c=j;
				ok2++;
				//System.out.println("c="+c);		
				if (ok!=c){
					steck1.add(c-1);
					steck1.add(c+1);
				}
			}
		}
//		for (int i=0; i<steck1.size(); i++){
//			System.out.println(steck1.get(i));
//		}
		
		int i=1;
		if (ok!=0){//Проверка на пересечения из другими словами
			for (int j = left; j<=right; j++){
				if (numButtons[j].getText().equalsIgnoreCase(word[i])){
					ok1++;
				}
				else if (numButtons[j].getText().isEmpty()){}
				else {
					System.out.println("Ne pidiyshlo slovo po goruzontali");
					ok=0;
					stop=0;
				}
				i++;
			}
		}
		
		int p;
		if(ok!=0 && ok1!=ok2){
			for ( p=0; p<steck1.size(); p++){
				if(steck1.get(p)==left | steck1.get(p)==right){
					ok=0;
					stop=0;
					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>"+steck1.get(p)+"  ok1="+ok1+"  ok2="+ok2);
				}
			}
		}
		if (left>=0 && right<400 && ok!=0){
			System.out.println("Точка пересечения="+ok+", Левая граница="+left+", Правая граница="+right+" w="+a);
			for (int j = ok; j<=right; j++){
				numButtons[j].setText(word[a]);
				numButtons[j].setBackground(color);
				a++;
			}
			for (int j = ok; j>=left; j--){
				 numButtons[j].setText(word[b]);
				 numButtons[j].setBackground(color);
				 b--;
			}
		}
	}
 
	//Проверка границ построения по вертикали
	public  void prov_vert(int up, int down, int ok, String word[], int a, int b, Color color){
		
		int ok_left=0;
		int ok_right=0;
		int ok1=0;
		int next=0;
		
		if (ok!=0){
//			int verx_l=0;
//			int verx_r=0;
//			int nuz_l=0;
//			int nuz_r=0;
			
			if (up>20 && down <379){
				System.out.println("1>>> up="+up+" down="+down);
				for (int j=up-21; j<=down+21;){
					//System.out.println("j_l="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_left++;
					}
					j=j+20;
				}
				for (int j=up-19; j<=down+23;){
					//System.out.println("j_r="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_right++;
					}
					j=j+20;
				}
			}
			if (up>=20 && down <=360){//left
				System.out.println("2>>> up="+up+" down="+down);
				for (int j=up-19; j<=down+23;){
					//System.out.println("j_r="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_right++;
					}
					j=j+20;
				}
			}
			}
			if (up>=20 && down <=360){//right
				System.out.println("3>>> up="+up+" down="+down);
				for (int j=up-21; j<=down+21;){
					//System.out.println("j_l="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_left++;
					}
					j=j+20;
				}
			}
			if (up==0 || down==380){
				System.out.println("4>>> up="+up+" down="+down);
				for (int j=up+1; j<=down+1;){
					//System.out.println("j_r="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_right++;
					}
					j=j+20;
				}				
			}
			if (up==19 || down==399){
				System.out.println("5>>> up="+up+" down="+down);
				for (int j=up-1; j<=down-1;){
					//System.out.println("j_l="+j);
					if (true != numButtons[j].getText().isEmpty()){
						ok_left++;
					}
					j=j+20;
				}
//			}
//						
//				for (int j=verx_l; j<=nuz_l;){
//					//System.out.println("j_l="+j);
//					if (true != numButtons[j].getText().isEmpty()){
//						ok_left++;
//					}
//					j=j+20;
//				}
//					
//				for (int j=verx_r; j<=nuz_r;){
//					//System.out.println("j_r="+j);
//					if (true != numButtons[j].getText().isEmpty()){
//						ok_right++;
//					}
//					j=j+20;
//				}
			
		}
					
		int i=1;
		if (ok!=0){//Проверка на пересечения из другими словами
			for (int j = up; j<=down;){
				if (numButtons[j].getText().equalsIgnoreCase(word[i])){
					ok1++;
				}
				else if (numButtons[j].getText().isEmpty()){
				}else {
					ok=0;
					stop=0;
				}
				i++;
				j=j+20;
			}
		}
	
		if (ok!=0){
			if(ok_left==0 && ok1==1 && ok_right==1){
				System.out.println("Vunrtok iz pravul");
			}
			else if (ok_left==ok1 && ok_right==ok1){
				//System.out.println("pidiyshlo"+" ok_left="+ok_left+" ok_right="+ok_right+" ok1="+ok1);
			}
			else{
				System.out.println("Ne pidiyshlo slovo po vertukali "+" ok_left="+ok_left+" ok_right="+ok_right+" ok1="+ok1);
				ok=0;
				stop=0;
			}
		}
		
		if (up>=0 && down<400 && ok!=0){ 
			stop=1;
			System.out.println("Точка пересечения="+ok+", Верх граница="+up+", Низ граница="+down);
			for (int j = ok; j<down;){
				numButtons[j=j+20].setText(word[a+1]);
				numButtons[j].setBackground(color);
				a++;
			}
			for (int j = ok; j>up;){
				 numButtons[j=j-20].setText(word[b-1]);
				 numButtons[j].setBackground(color);
				 b--;
			}
		}
	}
	
	// Вызов озвучки к слову mp3
	public void audioMP3(int i) throws Throwable {
			
	FileInputStream fis = new FileInputStream(words[i][3]);
	final Player playMP3 = new Player(fis);
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			try {
				playMP3.play();
			} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}});
	}
}



