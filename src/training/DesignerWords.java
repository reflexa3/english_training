package training;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.border.Border;

import data.TakeProgresWords;
import windows.MainWindow;
import javazoom.jl.player.Player;

public class DesignerWords extends JPanel implements KeyListener,  ActionListener{

	private static final long serialVersionUID = 1L;
	JPanel p1,p2,p3;
	JLabel pr,pr1, word, trans, message, image, enter;
	JButton audio, unknown, further;
	Box box1, box2, box3, box4, box5, box6;
	JButton[] numButtons;
	JButton[] numButtons1;
	String[] buttonString; // Слово в правильном порядке
	String[] buttonStringRandom; // Слово в рандомном порядке
	byte[] buttonString3; // Слово в юникоде
	ArrayList<String> buttonString2 = new ArrayList<String>(); // Массив счетчик
	Color Color1 = new Color(146, 243, 102); // green
	Color Color2 = new Color(250, 194, 90); // yellow
	Color Color3 = new Color(255, 91, 71); // red
	Color Color4 = new Color(89, 186, 241); // blue
	Color Color5 = new Color(221, 221, 221); // grey
	Color Color6 = new Color(246, 246, 246); // p3
	Color Color7 = new Color(75, 142, 197); // btn
	int q = 0; // счетчик кнопки (красная или желтая?)
	int j=1; // on/off audio
	int k;// счетчик слов берем из а
	String item,item1,training;
	String[][] words;
	Map<Integer, String> map = new HashMap<Integer, String>();
	Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
	int prog,prog1,x,x1;
	MainWindow m1 = new MainWindow();
	
	public DesignerWords(int a, String [][] data, Map<Integer, String> p, Map<Integer, Integer> o, String t, String q1, int q2, int q3, String q4) throws Exception {

		// ---------------------------Присваеваем значения всем масивам
		map=p;
		map1=o;
		words=data;
		k=a;
		item=t;
		item1=q1;
		training=q4;
		prog=q2;//Прогрес постепенно будет увеличиваться на х1 при каждом вызове конструктора
		x1=q3; // Одинаковое значения
				
		//System.out.println(k+" "+words[k][0]+" "+words[k][1]+" "+words[k][2]+" "+words[k][3]+" "+words[k][4]+" "+words[k][5]);
	    //System.out.println(k+1+">>>"+words[k+1][0]+" "+words[k+1][1]+" "+words[k+1][2]+" "+words[k+1][3]+" "+words[k+1][4]+" "+words[k+1][5]);
				
		String s = words[k][0];// получаем слово и его длину
		int dl = s.length();

		numButtons = new JButton[dl];// задаем длину для масивов кнопок
		numButtons1 = new JButton[dl];

		String temp[] = new String[dl];// разбиваем на буквы строку
		temp = s.split("");

		buttonString = new String[dl];// копируем масив кроме первого пробела
		System.arraycopy(temp, 1, buttonString, 0, dl);

		buttonStringRandom = new String[dl];
		System.arraycopy(buttonString, 0, buttonStringRandom, 0, dl);

		String s1 = s.toUpperCase();
		buttonString3 = s1.getBytes("ascii");
		
		
		// ------------------------------------------------------

		/*------------------------Создаем панели---------------------------------------*/

		// Создаем основную панель Border Layout
		setLayout(new BorderLayout());
		addKeyListener(this);
		setFocusable(true);
				
		// Область где будет прогрес тренровки (North p1)
		p1 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p1.setLayout(fl);
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		// p1.setBorder(Line1);
		p1.setBackground(Color2);

		// Создаем панель для самого конструктора слов (Center p2)
		p2 = new JPanel();
		GridBagLayout gbl = new GridBagLayout();
		p2.setLayout(gbl);
		// p2.setBorder(Line1);
		p2.addKeyListener(this);
		p2.setFocusable(true);
		p2.setPreferredSize(new Dimension(780, 250));
		p2.setBackground(Color.white);

		// Создаем панель для ассоциативной картинки и кнопок управления (South p3)
		p3 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p3.setLayout(gbl1);
		// p3.setBorder(Line1);
		p3.setBackground(Color6);
		p3.setPreferredSize(new Dimension(780, 220));

		/*-----------------------------------------------------------------------------------------------*/

		/*-------------------------------Добавляем елементы на панели------------------------------------*/

		// ------------------------------Добавляем елементы на панель p1
		// Button back
		Box box = Box.createHorizontalBox();
		JButton back = new JButton(new ImageIcon("res/images/back.png"));
		back.setToolTipText("Вернуться обратно к тренировкам");
		back.setBorder(null);
		back.setBackground(null);
		back.setForeground(Color.white);
		back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MainWindow m = new MainWindow();
					TakeProgresWords s = new TakeProgresWords();
					if (map.get(1)!="test"){
						s.ChangeProgressWords(training, map, map1);	
					}
					if(item==null){
						m.showTrainingPanel(item1);
						m.createAndShowGUI();
					}else{
						m.showTrainingPanelAll(item,item1);
						m.createAndShowGUI();
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}});
		box.add(back);
		box.add(Box.createHorizontalStrut(130));
		p1.add(box);
		
		Font fonttraining = new Font("Arial", Font.PLAIN, 24);
		JLabel nametraining = new JLabel("Конструктор слов");
		nametraining.setFont(fonttraining);
		box.add(nametraining);
		box.add(Box.createHorizontalStrut(110));
		p1.add(box);
						
		UIManager.put("ProgressBar.selectionForeground", Color.black);
		UIManager.put("ProgressBar.selectionBackground", Color.black);	
		JProgressBar progressBar = new JProgressBar();
		progressBar.setPreferredSize(new Dimension(250, 20));
		progressBar.setStringPainted(true);
		progressBar.setBackground(Color.white);
		progressBar.setForeground(Color.green);
		progressBar.setMinimum(0);
		progressBar.setMaximum(100);
		progressBar.setValue(prog);
		box.add(progressBar);
		p1.add(box);
		
		// ------------------------------Добавляем елементы на панель p2
		box1 = Box.createHorizontalBox(); // Прогресс слова - box1
		// box1.setBorder(Line2);
		box2 = Box.createHorizontalBox(); // Слово на руском - box2
		// box2.setBorder(Line2);
		box3 = Box.createHorizontalBox(); // Место куда должны стать буквы -
											// box3
		// box3.setBorder(Line2);
		box4 = Box.createHorizontalBox();// Набор разбросанных букв - box4
		// box4.setBorder(Line2);
		box5 = Box.createHorizontalBox();// Сообщения что можно ввсести буквы с
											// клавиатуры - box5
		// box5.setBorder(Line2);
		box6 = Box.createHorizontalBox();// Озвучка к слову - box6
		// box6.setBorder(Line2);

		GridBagConstraints c = new GridBagConstraints();

		Font fontword = new Font("Arial", Font.PLAIN, 24);
		Font fontbuttons = new Font("Arial", Font.PLAIN, 20);
		Font fonttrans = new Font("Arial", Font.PLAIN, 14);
		
		// Прогрес изученого слова
		String prog_pass1 ="res/images/progress/"+words[k][6]+".png";
		pr = new JLabel(new ImageIcon(prog_pass1));
		if(words[k][6].equalsIgnoreCase("0")){
			pr.setToolTipText("Прогресс слова: 0%.");
			prog1=1;
		}else if(words[k][6].equalsIgnoreCase("1")){
			pr.setToolTipText("Прогресс слова: 20%.");
			prog1=2;
		}else if(words[k][6].equalsIgnoreCase("2")){
			pr.setToolTipText("Прогресс слова: 40%.");
			prog1=3;
		}else if(words[k][6].equalsIgnoreCase("3")){
			pr.setToolTipText("Прогресс слова: 60%.");
			prog1=4;
		}else if(words[k][6].equalsIgnoreCase("4")){
			pr.setToolTipText("Прогресс слова: 80%.");
			prog1=5;
		}else if(words[k][6].equalsIgnoreCase("5")){
			pr.setToolTipText("Прогресс слова: 100%.");
			prog1=6;
		}
		pr.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr.setVisible(true);
	
		String prog_pass11 ="res/images/progress/"+prog1+".png";
		pr1 = new JLabel(new ImageIcon(prog_pass11));
		if(prog1==1){
			pr1.setToolTipText("Прогресс слова: 20%.");
		}else if(prog1==2){
			pr1.setToolTipText("Прогресс слова: 40%.");
		}else if(prog1==3){
			pr1.setToolTipText("Прогресс слова: 60%.");
		}else if(prog1==4){
			pr1.setToolTipText("Прогресс слова: 80%.");
		}else if(prog1==5){
			pr1.setToolTipText("Прогресс слова: 100%.");
		}    
		pr1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		pr1.setVisible(false);
		
		box1.add(Box.createHorizontalStrut(640));
		box1.add(pr);
		box1.add(pr1);
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 0;
		// progres.setBorder(Line2);
		p2.add(box1, c);
		

		// Слово на русском
		word = new JLabel(words[k][1]);
		box2.add(word);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 10, 0);
		word.setFont(fontword);
		p2.add(box2, c);

		// Добавляем пустые кнопки в box3
		for (int i = 0; i < buttonString.length; i++) {
			numButtons[i] = new JButton("  ");
			numButtons[i].setBackground(Color5);
			numButtons[i].setFont(fontbuttons);
			box3.add(Box.createHorizontalStrut(5));
			box3.add(numButtons[i]);
			c.anchor = GridBagConstraints.CENTER;
			c.insets = new Insets(10, 0, 10, 0);
			c.gridx = 0;
			c.gridy = 2;
			p2.add(box3, c);
		}

		// Добавляем кнопки в box4 (буквы разбросаны рандомно)
		shuffleArray(buttonStringRandom);
		for (int i = 0; i < buttonStringRandom.length; i++) {
			numButtons1[i] = new JButton(buttonStringRandom[i]);
			numButtons1[i].setCursor(Cursor
					.getPredefinedCursor(Cursor.HAND_CURSOR));
			numButtons1[i].addActionListener(this);
			numButtons1[i].setBackground(Color4);
			numButtons1[i].setForeground(Color.white);
			numButtons1[i].setFont(fontbuttons);
			box4.add(Box.createHorizontalStrut(5));
			box4.add(numButtons1[i]);
			c.anchor = GridBagConstraints.CENTER;
			c.gridx = 0;
			c.gridy = 3;
			p2.add(box4, c);
		}

		// Сообщаем что можно вводить слово из клавиатуры в box5
		message = new JLabel(new ImageIcon("res/images/1.gif"));
		message.setText("Вы можете вводить слова с клавиатуры");
		box5.add(message);
		box5.setVisible(true);
		c.anchor = GridBagConstraints.WEST;
		// c.insets = new Insets(0, 0, 0, 150);
		c.gridx = 0;
		c.gridy = 4;
		p2.add(box5, c);

		// транскрипцыя слова
		trans = new JLabel(words[k][2]);
		trans.setVisible(false);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 0);
		trans.setFont(fonttrans);
		p2.add(trans, c);

		// Здесь будет иконка со звуком box6
		audio = new JButton(new ImageIcon("res/images/3.png"));
		audio.setText("");
		audio.setBorder(null);
		audio.setBackground(null);
		audio.setForeground(Color.white);
		audio.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		audio.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							audioMP3();
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});
		box6.add(audio);
		box6.setVisible(false);
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(10, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 6;
		p2.add(box6, c);

		// ---------------------------------Добавляем елементы на панель p3

		GridBagConstraints c1 = new GridBagConstraints();

		Font fontbuttons1 = new Font("Arial", Font.PLAIN, 14);

		// Ассоциативная картинка
		image = new JLabel(new ImageIcon("res/images/4.png"));
		c1.insets = new Insets(0, 120, 0, 0);
		c1.gridheight=2;
		c1.gridx = 0;
		c1.gridy = 0;
		p3.add(image, c1);

		enter = new JLabel("Enter");
		c1.anchor = GridBagConstraints.FIRST_LINE_START;
		c1.insets = new Insets(0, 55, 0, 0);
		c1.gridx = 1;
		c1.gridy = 0;
		p3.add(enter, c1);

		unknown = new JButton("Не знаю");
		unknown.setPreferredSize(new Dimension(100, 35));
		unknown.setBackground(Color7);
		unknown.setForeground(Color.white);
		unknown.setFont(fontbuttons1);
		unknown.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		unknown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				actionClickNotKnow();

			}
		});
		c1.anchor = GridBagConstraints.FIRST_LINE_START;
		c1.insets = new Insets(20, 30, 0, 0);
		c1.gridx = 1;
		c1.gridy = 1;
		p3.add(unknown, c1);

		further = new JButton("Дальше");
		further.setPreferredSize(new Dimension(100, 35));
		further.setBackground(Color7);
		further.setForeground(Color.white);
		further.setFont(fontbuttons1);
		further.setVisible(false);
		further.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		further.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					k++;
					actionClickNextButton();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		c1.anchor = GridBagConstraints.FIRST_LINE_START;
		c1.insets = new Insets(20, 30, 0, 0);
		c1.gridx = 1;
		c1.gridy = 1;
		p3.add(further, c1);

		/*-----------------------------------------------------------------------------------------------*/
		//KeyboardFocusManager.getCurrentKeyboardFocusManager().setGlobalFocusOwner(p2);
		add("North", p1);
		add("Center", p2);
		add("South", p3);
				
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);  
		
		System.out.println("buttonString  "+Arrays.asList(buttonString));
		for (int j = 0; j < numButtons.length; j++) {
			System.out.println("numButtons="+j+"["+numButtons[j].getText()+"]");
		}
		System.out.println("Words"+k);
				
	}
	
	
	
	KeyEventDispatcher keyEventDispatcher = new KeyEventDispatcher() {

		public boolean dispatchKeyEvent(KeyEvent e) {
			// TODO Auto-generated method stub
			
			 if (e.getID() == KeyEvent.KEY_PRESSED) {
				 keyPressed(e);
			 }
			return e != null;
		}
	};
	
	// Обработчик событий нажатий на кнопки
	public void actionPerformed(ActionEvent e) {

		JButton clickedButton = (JButton) e.getSource();
		String clickedButtonLabel = clickedButton.getText();

		// Пустой массив который будет использоваться как счетчик для сравнения
		// с массивом buttonString
		int i = buttonString2.size();

		// Записываем в пустой массив по одной букве з текстом нажатой
		// кнопки и проверяем не последняя ли это буква
		if (buttonString[i].equalsIgnoreCase(clickedButtonLabel) && i < buttonString.length - 1) {
			// System.out.println("buttonString[i]="+buttonString[i]+" "+"i="+i+" "+"clickedButtonLabel="+clickedButtonLabel);
			buttonString2.add(clickedButtonLabel);
			numButtons[i].setText(clickedButtonLabel);
			numButtons[i].setBackground(Color1);
			numButtons[i].setForeground(Color.black);
			clickedButton.setVisible(false);

			// Активируем картинку когда пригает последняя буква
		} else if (buttonString[i].equalsIgnoreCase(clickedButtonLabel)) {
			buttonString2.add(clickedButtonLabel);
			numButtons[i].setText(clickedButtonLabel);
			numButtons[i].setBackground(Color1);
			numButtons[i].setForeground(Color.black);
			clickedButton.setVisible(false);
			box4.setVisible(false);
			box5.setVisible(false);
			unknown.setVisible(false);
			trans.setVisible(true);
			image.setIcon(new ImageIcon(words[k][4]));
			further.setVisible(true);
			box6.setVisible(true);
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						audioMP3();
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});

			if(q==0){
				pr.setVisible(false);
				pr1.setVisible(true);
			}
		}	
		
		// Если ошыбка то кнопка временно должна стать желтой и вернуться в
		// исходный цвет, если еще раз ошыбка то красной
		else if (buttonString[i] != clickedButtonLabel && q == 0) {
			clickedButton.setBackground(Color2);
			q++;
			timer.schedule(task, 700, 700);

		} else {
			clickedButton.setBackground(Color3);
		}
		
	}

	//Кнопка Дальше + записываем результат
	private void actionClickNextButton() throws Exception {
	
		//MainWindow m1 = new MainWindow();
		if (k==1){
			x1=prog; //Первое значения прогреса 
			x=prog+x1; 
		} else{
			x=prog+x1; // Добавляем к прогресу который уже есть первое значения
		}
		
		int d = words.length;
		//System.out.println(k+"   >>>");	
		if (q==0 && k==d){
			map.put(k, words[k-1][5]);
			map1.put(k, 1);
			m1.showResultPanel(words, map, map1,item,item1,training);
			m1.createAndShowGUI();
		} else if (q!=0 && k==d){
			map.put(k, words[k-1][5]);
			map1.put(k, 0);
			m1.showResultPanel(words, map, map1,item,item1,training);
			m1.createAndShowGUI();
		 
		}else if (q==0 && k<12){
			map.put(k, words[k-1][5]);
			map1.put(k, 1);
			removeAll();
			add(new DesignerWords(k, words, map, map1,item,item1,x,x1,training));
			validate();
			repaint();
		} else if (q!=0 && k<12){
			map.put(k, words[k-1][5]);
			map1.put(k, 0);
			removeAll();
			add(new DesignerWords(k, words, map, map1,item,item1,x,x1,training));
			validate();
			repaint();
			
		} else if (q==0 && k>=12){
			map.put(k, words[k-1][5]);
			map1.put(k, 1);
			m1.showResultPanel(words, map, map1,item,item1,training);
			m1.createAndShowGUI();
		} else if (q!=0 && k>=12){
			map.put(k, words[k-1][5]);
			map1.put(k, 0);
			m1.showResultPanel(words, map, map1,item,item1,training);
			m1.createAndShowGUI();
		}
	}
		
	//Кнопка Не знаю
	public void actionClickNotKnow() {
		for (int j = 0; j < buttonString.length; j++) {
			numButtons[j].setText(buttonString[j]);
			numButtons[j].setBackground(Color1);
			numButtons[j].setForeground(Color.black);
			numButtons1[j].setVisible(false);
			box4.setVisible(false);
			box5.setVisible(false);
			unknown.setVisible(false);
			trans.setVisible(true);
			image.setIcon(new ImageIcon(words[k][4]));
			further.setVisible(true);
			box6.setVisible(true);
		}
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					audioMP3();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});q++;
	}

	// Вызов озвучки к слову
	/*public void audio() {

		if (j == 1) {

			try {
				File soundFile = new File(words[k][3]);
				AudioInputStream ais = AudioSystem
						.getAudioInputStream(soundFile);

				Clip clip = AudioSystem.getClip();
				clip.open(ais);
				clip.setFramePosition(0);
				clip.start();

			} catch (IOException | UnsupportedAudioFileException
					| LineUnavailableException exc) {
				exc.printStackTrace();
			}
		}
	}*/
	
	// Вызов озвучки к слову mp3
	public void audioMP3() throws Throwable {

		if (j == 1) {
			FileInputStream fis = new FileInputStream(words[k][3]);
		    Player playMP3 = new Player(fis);
		    playMP3.play();
		}
	}
	
	// Алгоритм перемешивания букв в слове рандомно
	public static String shuffleArray(String[] b) {
		int n = b.length;
		Random random = new Random();
		random.nextInt();
		for (int i = 0; i < n; i++) {
			int change = i + random.nextInt(n - i);
			swap(b, i, change);
		}
		return null;
	}
	
	private static void swap(String[] b, int i, int change) {
		// TODO Auto-generated method stub
		String temp = b[i];
		b[i] = b[change];
		b[change] = temp;
	}

	/*---------------------- Обработчик событий нажатий на клавиатуре----------------------*/
		
	public void keyTyped(KeyEvent e) {
	}
	
	public void keyPressed(KeyEvent e) {
				
		int b = e.getKeyCode();
		int i = buttonString2.size(); // cчетчик масив
		
		//System.out.println("i= "+i+" "+buttonString[i]);
		System.out.println("b= "+b);
		
		for (int j = 0; j < numButtons.length; j++) {
			System.out.println(">>>>numButtons="+j+"["+numButtons[j].getText()+"]");
		}
		System.out.println();
		// Если нажать кнопку Enter, то буквы сами станут в правильном порядке
		if (b == 10 && numButtons[0].getText() != buttonString[0]) {
						
			for (int j = 0; j < buttonString.length; j++) {
				numButtons[j].setText(buttonString[j]);
				numButtons[j].setBackground(Color1);
				numButtons[j].setForeground(Color.black);
				numButtons1[j].setVisible(false);
				box4.setVisible(false);
				box5.setVisible(false);
				unknown.setVisible(false);
				trans.setVisible(true);
				box6.setVisible(true);
				image.setIcon(new ImageIcon(words[k][4]));
				further.setVisible(true);
				System.out.println("++++numButtons="+j+"["+numButtons[j].getText()+"]");
			}
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						audioMP3();
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});q++;
			
			// Если нажать Далее то окно закроется
		} else if (b == 10 && numButtons[i].getText() == buttonString[i]) {
						
			try {
				System.out.println("Next  "+numButtons[i].getText()+" "+ buttonString[i]);
				k++;
				actionClickNextButton();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Записываем в пустой массив по одной букве з текстом введенной
			// кнопки и проверяем не последняя ли это буква
		} else if (buttonString3[i] == b && i < buttonString.length - 1) {

			buttonString2.add(buttonString[i]);
			numButtons[i].setText(buttonString[i]);
			numButtons[i].setBackground(Color1);
			numButtons[i].setForeground(Color.black);

			for (int p = 0; p < numButtons1.length; p++) {
				if (numButtons1[p].getText() == buttonString[i]) {
					numButtons1[p].setVisible(false);
					numButtons1[p].setText("");
					break;
				}
			}

			// Активируем картинку когда пригает последняя буква
		} else if (buttonString3[i] == b) {
			buttonString2.add(buttonString[i]);
			numButtons[i].setText(buttonString[i]);
			numButtons[i].setBackground(Color1);
			numButtons[i].setForeground(Color.black);
			box4.setVisible(false);
			box5.setVisible(false);
			unknown.setVisible(false);
			trans.setVisible(true);
			box6.setVisible(true);
			image.setIcon(new ImageIcon(words[k][4]));
			further.setVisible(true);
			
			for (int p = 0; p < numButtons1.length; p++) {
				if (numButtons1[p].getText() == buttonString[i]) {
					numButtons1[p].setVisible(false);
				}
			}

			// Если ошыбка то кнопка временно должна стать желтой и вернуться в
			// исходный цвет, если еще раз ошыбка то красной
		} else if (buttonString3[i] != b && q == 0) {

			for (int p = 0; p < buttonString3.length; p++) {
				if (buttonString3[p] == b) {

					for (int p1 = 0; p1 < buttonString3.length; p1++) {
						if (numButtons1[p1].getText() == buttonString[p]) {
							numButtons1[p1].setBackground(Color2);
							q++;
							timer.schedule(task, 700, 700);
						}
					}
				}
			}

		} else if (buttonString3[i] != b && q > 0) {

			for (int p = 0; p < buttonString3.length; p++) {
				if (buttonString3[p] == b) {

					for (int p1 = 0; p1 < buttonString3.length; p1++) {
						if (numButtons1[p1].getText() == buttonString[p]) {
							numButtons1[p1].setBackground(Color3);
						}
					}
				}
			}
		}
		//System.out.println("Key Pressed: " + b);
		//System.out.println("Key Pressed: " + numButtons1[i].getText());
	}
	
	public void keyReleased(KeyEvent e) {
	}
	/*---------------------- Конец обработчика событий нажатий на клавиатуре----------------------*/

	/* ____________________________Таймер__________________________________ */
	Timer timer = new Timer();
	TimerTask task = new TimerTask() {

		public void run() {
			for (int w = 0; w < buttonStringRandom.length; w++) {
				numButtons1[w].setBackground(Color4);
			}
		}
	};
	/* ____________________________Конец таймера____________________________ */
	
}