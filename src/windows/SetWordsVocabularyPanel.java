package windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import data.TakeWordsVocabluary;

public class SetWordsVocabularyPanel extends JPanel {

	private static final long serialVersionUID = -1772362933820173815L;
	JPanel flp;
	Border Line1;
	Color color1,color2;
	Font fonttext,fontbtn,fontcount; 
	JTable table;
	TakeWordsVocabluary s = new TakeWordsVocabluary();
	Object rowData[][];
	public SetWordsVocabularyPanel(int a) throws Exception {
		
		rowData=s.getSetOfWords(a);
		
		color1 = new Color(245,242,233); // grey
		color2 = new Color(75, 142, 197); // btn
		
		Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		
		fonttext = new Font("Arial", Font.PLAIN, 14);// Текст к названию набора
		fontbtn = new Font("Arial", Font.PLAIN, 14); // Кнопка еще + тренровать слова
		fontcount = new Font("Arial", Font.PLAIN, 12);// Счетчик слов
		
		// Создаем основную панель Border Layout (1 cлой)
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1349, 530));//530
		
		//Данные таблицы
		//rowData[][] = { words[0][0]+words[0][]};
		
		Object columnNames[] = {"Слово", "Перевод", "Транскрипцыя", "Звук", "Набор", "Изображения"};
		
		table = new JTable(rowData, columnNames);
		table.setFont(fonttext);
		table.setPreferredSize(new Dimension(1150, 1800));
				
		//(2 cлой) прокрутка 
		JScrollPane jScrollPane = new JScrollPane(table); 
		jScrollPane.getVerticalScrollBar().setUnitIncrement(25);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		add("Center", jScrollPane);
	}
	
}
