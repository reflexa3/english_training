package windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import data.TakeWordsVocabluary;

public class VocabularyPanel extends JPanel {

	private static final long serialVersionUID = -1772362933820173815L;
	TakeWordsVocabluary s = new TakeWordsVocabluary();
	String [][] words;
	
	public VocabularyPanel() throws Exception {
		
		words=s.getWords();
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		Font fonttext = new Font("Arial", Font.PLAIN, 14);// Текст к названию набора
		
		// Создаем основную панель Border Layout (1 cлой)
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1349, 530));//530
						
		TableModel model= new VocabluaryTableModel();
		JTable table = new JTable(model);
		table.setFont(fonttext);
		table.setRowHeight(50);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setPreferredWidth(150);
		table.getColumnModel().getColumn(4).setPreferredWidth(200);
		table.getColumnModel().getColumn(6).setPreferredWidth(300);
				
		table.addMouseListener(new MouseAdapter() {
			  public void mouseClicked(MouseEvent e) {
			  
			      JTable target = (JTable)e.getSource();
			      int row = target.getSelectedRow();
			      int column = target.getSelectedColumn();
			      try {
					audioMP3(row);
			      } catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
			      }
			   }});
								
		for(int i=0; i<words.length; i++){
			table.getColumnModel().getColumn(6).setCellRenderer(new ImageRenderer());
			table.getColumnModel().getColumn(5).setCellRenderer(new ImageRenderer());
			table.getColumnModel().getColumn(3).setCellRenderer(new ButtonRenderer());
		}
		
		//(2 cлой) прокрутка 
		JScrollPane jScrollPane = new JScrollPane(table); 
		jScrollPane.getVerticalScrollBar().setUnitIncrement(25);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		add("Center", jScrollPane);
	}
	
	public void audioMP3(int i) throws Throwable {
		
		FileInputStream fis = new FileInputStream(words[i][3]);
		final Player playMP3 = new Player(fis);
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					playMP3.play();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}});
	}
}

//Class for Table data
 class VocabluaryTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;
	Object [][] cells1;
	String [][] words;
	int dl;
	private String columnNames[] = {"Слово", "Перевод", "Транскрипцыя", "Звук", "Набор", "Прогресс", "Изображения"};
	private Object[][] cells;
	
	public VocabluaryTableModel(){
		TakeWordsVocabluary s = new TakeWordsVocabluary();
		try {
			words=s.getWords();
		} catch (Exception e) {
			e.printStackTrace();
		}
		dl=words.length;
		cells1 = new Object [dl][7];
		for(int i=0; i<dl; i++){
			cells1[i] = new Object []{words[i][0], words[i][1], words[i][2], new JButton(new ImageIcon("res/images/9.png")), words[i][4], new ImageIcon("res/images/progress/"+words[i][6]+".png"), new ImageIcon(words[i][5])};
		}
		cells=cells1;
	}
	
	public String getColumnName(int c) {
		return columnNames[c];
	}
	
	public Class<?> getColumnClass(int r, int c){
		return cells[r][c].getClass();
	}
	
	public int getColumnCount() {
		return 7;
	}
	
	public int getRowCount() {
		return dl;
	}
	
	public Object getValueAt(int r, int c) {
		return cells[r][c];
	} 
	
	public void setValueAt(Object obj, int r, int c) {
		cells[r][c] = obj;
	}

	public boolean isCellEditable(int r, int c) {
		return false;
	}
	
	public void addTableModelListener(TableModelListener l) {
	}

	public void removeTableModelListener(TableModelListener l) {
	}
}

 
 class ImageRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 4812927209624419793L;
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
	    setIcon((Icon)value);
	    return this;
	  }
	}

 
 class ButtonRenderer extends DefaultTableCellRenderer {
		
	private static final long serialVersionUID = 4812927209624419793L;
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
	  JButton audio=((JButton) value);
	    return  (JButton) value;
	  }
	}

 