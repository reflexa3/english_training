package windows;

/* Панель Обзор
Содержит информацию о новых наборах которые пользователь может добавить на изучения
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import data.Set;

public class WordsSetAddPanel extends JPanel{

	private static final long serialVersionUID = -1765432244713700759L;
	JPanel flp,gl1,gl2,gl3,gl4,gl5,gl6,gl7,gl8,gl9,gl10,gl11,gl12,gl13,gl14,gl15;
	Box box13,box14, box23,box24,box25,box26,box27,box28,box29,box30,box53,box54,box55,box56,box57,box58,box59,box60,box63,box64; 
	Color color1,color2;
	Font fonttext,fontbtn,fontcount; 
	Border Line1;
	int click=0;
	
	Set q = new Set();
	String [] set;     // Набор + количество поднаборов 
	String [][] count; // Поднабор + подсчет слов
	Boolean [] visible;     // Проверка при добавлении набора 
	 ImagePanel pp;
	public WordsSetAddPanel() throws Exception {
	       
		set = q.getSetOfSet();
		count = q.getCountWords();
		visible = q.getSetVisible();
		
		color1 = new Color(245,242,233); // grey
		color2 = new Color(75, 142, 197); // btn
				
		fonttext = new Font("Arial", Font.PLAIN, 20);// Текст к названию набора
		fontbtn = new Font("Arial", Font.PLAIN, 14); // Кнопка еще + тренровать слова
		fontcount = new Font("Arial", Font.PLAIN, 12);// Счетчик слов
		
		Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize ();
	    int vert = sSize.height-240;
	    int hor  = sSize.width-25; 
	     
		// Создаем основную панель Border Layout (1 cлой)
		setLayout(new BorderLayout());
		setOpaque(false);
		setPreferredSize(new Dimension(1349, vert));
				
		// Панель FlowLayout (3 cлой)
		flp = new JPanel();
		FlowLayout fl = new FlowLayout();
		flp.setLayout(fl);
		Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		flp.setPreferredSize(new Dimension(1349, 2800));
		flp.setOpaque(false);
		
		set1(); //Набор Популярное
		set2(); //Набор О Человеке
		set3(); //Набор Хобби
		set4(); //Набор Разное
		set5(); //Набор Части речи, фразовые глаголы и идиомы
		set6(); //Литература и Кино
		
		//(2 cлой) прокрутка gl панелей
		JScrollPane jScrollPane = new JScrollPane(flp);
		jScrollPane.setBorder(null);
		jScrollPane .getViewport().setOpaque( false );  
		jScrollPane .setOpaque( false );  
		jScrollPane.getVerticalScrollBar().setUnitIncrement(25);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		add("Center", jScrollPane);
		
	}
	
	//******************    ДОБАВЛЕНИЯ НАБОРОВ 4 cлой **********************//
	// Набор ПОПУЛЯРНОЕ (8 подпанелей) 
	private void set1(){
		
	//Информация к набору
	Box box1 = Box.createHorizontalBox();
	box1.setPreferredSize(new Dimension(973, 50));
					
	Box box12 = Box.createHorizontalBox();
	box12.setPreferredSize(new Dimension(1150, 250));
	//box12.setBorder(Line1);
					
	box13 = Box.createHorizontalBox();// Отступ между боксами
	box13.setPreferredSize(new Dimension(1150, 40));
	//box13.setBorder(Line1);
	box13.setVisible(false);
						
	box14 = Box.createHorizontalBox();
	box14.setPreferredSize(new Dimension(1150, 250));
	//box14.setBorder(Line1);
	box14.setVisible(false);
					
	JButton btn1 = new JButton(set[1]);
	btn1.setFont(fonttext);
	//btn1.setContentAreaFilled(false);
	//btn1.setBorder(null);
	btn1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	btn1.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		actionClickBtn1();
	}
	});
	box1.add(btn1);
	box1.add(Box.createHorizontalGlue());
						
	JButton btn12 = new JButton("еще");
	btn12.setFont(fontbtn);
	btn12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	btn12.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		actionClickBtn1();
	}
	});
	box1.add(btn12);
						
	//Главная панель набора gl1
	gl1 = new JPanel();
	FlowLayout glp = new FlowLayout();
	gl1.setPreferredSize(new Dimension(1165, 315));
	gl1.setLayout(glp);
	//gl1.setBackground(color2);
	gl1.setOpaque(false);
	

						
	// ------------------------------------------------------------Панель ЦВЕТА
	JPanel p1 = new JPanel();
	GridBagLayout gbl1 = new GridBagLayout();
	p1.setLayout(gbl1);
	p1.setPreferredSize(new Dimension(250, 250));
	p1.setBackground(Color.white);
	p1.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(1);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
	// Добавляем елементы на панель р1 
	GridBagConstraints c1 = new GridBagConstraints();
						
	JLabel image1 = new JLabel(new ImageIcon("files/popular/colour/image.jpeg")); //250*167
	c1.insets = new Insets(0, 0, 0, 0);
	c1.gridx = 0;
	c1.gridy = 0;
	c1.gridwidth = 2;
	p1.add(image1, c1);
						
	JLabel text1 = new JLabel(count[1][0]);
	text1.setFont(fonttext);
	c1.gridx = 0;
	c1.gridy = 1;
	c1.insets = new Insets(5, 0, 5, 0);
	p1.add(text1, c1);
				
	JLabel count1 = new JLabel(count[1][1]);
	count1.setFont(fontcount);
	count1.setBackground(color1);
	c1.gridx = 0;
	c1.gridy = 2;
	c1.insets = new Insets(0, 0, 0, 0);
	p1.add(count1, c1);
				
	JButton b1 = new JButton("Добавить набор");
	b1.setBackground(color2);
	b1.setForeground(Color.white);
	b1.setFont(fontbtn);
	b1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b1.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (visible[1]!=true){
				q.addSetOfSet(1);
				repaintWindow();
			}else{
				JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[1][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	});
	c1.insets = new Insets(2, 0, 5, 0);
	c1.gridx = 0;
	c1.gridy = 3;
	p1.add(b1, c1);
	
	JButton bt1 = new JButton(new ImageIcon("res/images/8.png"));
	bt1.setVisible(visible[1]);
	bt1.setToolTipText("На изучении");
	bt1.setContentAreaFilled(false);
	bt1.setBorder(null);
	bt1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c1.anchor = GridBagConstraints.LAST_LINE_END;
	c1.gridx = 1;
	c1.gridy = 3;
	p1.add(bt1, c1);
	
	//------------------------------------------------------Панель Дом и быт
	JPanel p2 = new JPanel();
	GridBagLayout gbl2 = new GridBagLayout();
	p2.setLayout(gbl2);
	p2.setPreferredSize(new Dimension(250, 250));
	p2.setBackground(Color.white);
	p2.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(2);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р2
	GridBagConstraints c2 = new GridBagConstraints();
			
	JLabel image2 = new JLabel(new ImageIcon("files/popular/house and life/image.jpeg")); //250*167
	c2.insets = new Insets(0, 0, 0, 0);
	c2.gridx = 0;
	c2.gridy = 0;
	c2.gridwidth = 2;
	p2.add(image2, c2);
						
	JLabel text2 = new JLabel(count[2][0]);
	text2.setFont(fonttext);
	c2.gridx = 0;
	c2.gridy = 1;
	c2.insets = new Insets(5, 0, 5, 0);
	p2.add(text2, c2);
		
	JLabel count2 = new JLabel(count[2][1]);
	count2.setFont(fontcount);
	count2.setBackground(color1);
	c2.gridx = 0;
	c2.gridy = 2;
	c2.insets = new Insets(0, 0, 0, 0);
	p2.add(count2, c2);
				
	JButton b2 = new JButton("Добавить набор");
	b2.setBackground(color2);
	b2.setForeground(Color.white);
	b2.setFont(fontbtn);
	b2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b2.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (visible[2]!=true){
				q.addSetOfSet(2);
				repaintWindow();
			}else{
				JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[2][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	});
	c2.insets = new Insets(2, 0, 5, 0);
	c2.gridx = 0;
	c2.gridy = 3;
	p2.add(b2, c2);
	
	JButton bt2 = new JButton(new ImageIcon("res/images/8.png"));
	bt2.setVisible(visible[2]);
	bt2.setToolTipText("На изучении");
	bt2.setContentAreaFilled(false);
	bt2.setBorder(null);
	bt2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c2.anchor = GridBagConstraints.LAST_LINE_END;
	c2.gridx = 1;
	c2.gridy = 3;
	p2.add(bt2, c2);
	
	//---------------------------------------------------------Панель Строение человека
	JPanel p3 = new JPanel();
	GridBagLayout gbl3 = new GridBagLayout();
	p3.setLayout(gbl3);
	p3.setPreferredSize(new Dimension(250, 250));
	p3.setBackground(Color.white);
	p3.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(3);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р3
	GridBagConstraints c3 = new GridBagConstraints();
						
	JLabel image3 = new JLabel(new ImageIcon("files/popular/structure of human/image.jpeg")); //250*167
	c3.insets = new Insets(0, 0, 0, 0);
	c3.gridwidth=2;
	c3.gridx = 0;
	c3.gridy = 0;
	p3.add(image3, c3);
								
	JLabel text3 = new JLabel(count[3][0]);
	text3.setFont(fonttext);
	c3.gridx = 0;
	c3.gridy = 1;
	c3.insets = new Insets(5, 0, 5, 0);
	p3.add(text3, c3);
						
	JLabel count3 = new JLabel(count[3][1]);
	count3.setFont(fontcount);
	count3.setBackground(color1);
	c3.gridx = 0;
	c3.gridy = 2;
	c3.insets = new Insets(0, 0, 0, 0);
	p3.add(count3, c3);
						
	JButton b3 = new JButton("Добавить набор");
	b3.setBackground(color2);
	b3.setForeground(Color.white);
	b3.setFont(fontbtn);
	b3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b3.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[3]!=true){
					q.addSetOfSet(3);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[3][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c3.insets = new Insets(2, 0, 5, 0);
	c3.gridx = 0;
	c3.gridy = 3;
	p3.add(b3, c3);		
	
	JButton bt3 = new JButton(new ImageIcon("res/images/8.png"));
	bt3.setVisible(visible[3]);
	bt3.setToolTipText("На изучении");
	bt3.setContentAreaFilled(false);
	bt3.setBorder(null);
	bt3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c3.anchor = GridBagConstraints.LAST_LINE_END;
	c3.gridx = 1;
	c3.gridy = 3;
	p3.add(bt3, c3);
	
	//----------------------------------------------------------Панель Члены семьи
	JPanel p4 = new JPanel();
	GridBagLayout gbl4 = new GridBagLayout();
	p4.setLayout(gbl4);
	p4.setPreferredSize(new Dimension(250, 250));
	p4.setBackground(Color.white);
	p4.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(4);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р4
	GridBagConstraints c4 = new GridBagConstraints();
								
	JLabel image4 = new JLabel(new ImageIcon("files/popular/family members/image.jpeg")); //250*167
	c4.insets = new Insets(0, 0, 0, 0);
	c4.gridx = 0;
	c4.gridy = 0;
	c4.gridwidth=2;
	p4.add(image4, c4);
										
	JLabel text4 = new JLabel(count[4][0]);
	text4.setFont(fonttext);
	c4.gridx = 0;
	c4.gridy = 1;
	c4.insets = new Insets(5, 0, 5, 0);
	p4.add(text4, c4);
								
	JLabel count4 = new JLabel(count[4][1]);
	count4.setFont(fontcount);
	count4.setBackground(color1);
	c4.gridx = 0;
	c4.gridy = 2;
	c4.insets = new Insets(0, 0, 0, 0);
	p4.add(count4, c4);
								
	JButton b4 = new JButton("Добавить набор");
	b4.setBackground(color2);
	b4.setForeground(Color.white);
	b4.setFont(fontbtn);
	b4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b4.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[4]!=true){
					q.addSetOfSet(4);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[4][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c4.insets = new Insets(2, 0, 5, 0);
	c4.gridx = 0;
	c4.gridy = 3;
	p4.add(b4, c4);	
	
	JButton bt4 = new JButton(new ImageIcon("res/images/8.png"));
	bt4.setVisible(visible[4]);
	bt4.setToolTipText("На изучении");
	bt4.setContentAreaFilled(false);
	bt4.setBorder(null);
	bt4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c4.anchor = GridBagConstraints.LAST_LINE_END;
	c4.gridx = 1;
	c4.gridy = 3;
	p4.add(bt4, c4);
	
	//----------------------------------------------------Панель Чувства и эмоции
	JPanel p5 = new JPanel();
	GridBagLayout gbl5 = new GridBagLayout();
	p5.setLayout(gbl5);
	p5.setPreferredSize(new Dimension(250, 250));
	p5.setBackground(Color.white);
	p5.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(5);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р5
	GridBagConstraints c5 = new GridBagConstraints();
										
	JLabel image5 = new JLabel(new ImageIcon("files/popular/feelings and emotions/image.jpeg")); //250*167
	c5.insets = new Insets(0, 0, 0, 0);
	c5.gridx = 0;
	c5.gridy = 0;
	c5.gridwidth=2;
	p5.add(image5, c5);
										
	JLabel text5 = new JLabel(count[5][0]);
	text5.setFont(fonttext);
	c5.gridx = 0;
	c5.gridy = 1;
	c5.insets = new Insets(5, 0, 5, 0);
	p5.add(text5, c5);
								
	JLabel count5 = new JLabel(count[5][1]);
	count5.setFont(fontcount);
	count5.setBackground(color1);
	c5.gridx = 0;
	c5.gridy = 2;
	c5.insets = new Insets(0, 0, 0, 0);
	p5.add(count5, c5);
								
	JButton b5 = new JButton("Добавить набор");
	b5.setBackground(color2);
	b5.setForeground(Color.white);
	b5.setFont(fontbtn);
	b5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b5.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[5]!=true){
					q.addSetOfSet(5);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[5][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c5.insets = new Insets(2, 0, 5, 0);
	c5.gridx = 0;
	c5.gridy = 3;
	p5.add(b5, c5);	
	
	JButton bt5 = new JButton(new ImageIcon("res/images/8.png"));
	bt5.setVisible(visible[5]);
	bt5.setToolTipText("На изучении");
	bt5.setContentAreaFilled(false);
	bt5.setBorder(null);
	bt5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c5.anchor = GridBagConstraints.LAST_LINE_END;
	c5.gridx = 1;
	c5.gridy = 3;
	p5.add(bt5, c5);
	
	//--------------------------------------------------------------Панель ТОП 100 существительных
	JPanel p6 = new JPanel();
	GridBagLayout gbl6 = new GridBagLayout();
	p6.setLayout(gbl6);
	p6.setPreferredSize(new Dimension(250, 250));
	p6.setBackground(Color.white);
	p6.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(6);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р6
	GridBagConstraints c6 = new GridBagConstraints();
							
	JLabel image6 = new JLabel(new ImageIcon("files/popular/TOP 100 nouns/image.jpeg")); //250*167
	c6.insets = new Insets(0, 0, 0, 0);
	c6.gridx = 0;
	c6.gridy = 0;
	c6.gridwidth=2;
	p6.add(image6, c6);
										
	JLabel text6 = new JLabel(count[6][0]);
	text6.setFont(fonttext);
	c6.gridx = 0;
	c6.gridy = 1;
	c6.insets = new Insets(5, 0, 5, 0);
	p6.add(text6, c6);
								
	JLabel count6 = new JLabel(count[6][1]);
	count6.setFont(fontcount);
	count6.setBackground(color1);
	c6.gridx = 0;
	c6.gridy = 2;
	c6.insets = new Insets(0, 0, 0, 0);
	p6.add(count6, c6);
								
	JButton b6 = new JButton("Добавить набор");
	b6.setBackground(color2);
	b6.setForeground(Color.white);
	b6.setFont(fontbtn);
	b6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b6.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[6]!=true){
					q.addSetOfSet(6);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[6][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c6.insets = new Insets(2, 0, 5, 0);
	c6.gridx = 0;
	c6.gridy = 3;
	p6.add(b6, c6);	
	
	JButton bt6 = new JButton(new ImageIcon("res/images/8.png"));
	bt6.setVisible(visible[6]);
	bt6.setToolTipText("На изучении");
	bt6.setContentAreaFilled(false);
	bt6.setBorder(null);
	bt6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c6.anchor = GridBagConstraints.LAST_LINE_END;
	c6.gridx = 1;
	c6.gridy = 3;
	p6.add(bt6, c6);
	
	//------------------------------------------------------------------Панель Предлоги
	JPanel p7 = new JPanel();
	GridBagLayout gbl7 = new GridBagLayout();
	p7.setLayout(gbl7);
	p7.setPreferredSize(new Dimension(250, 250));
	p7.setBackground(Color.white);
	p7.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(7);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р7
	GridBagConstraints c7 = new GridBagConstraints();
								
	JLabel image7 = new JLabel(new ImageIcon("files/popular/prepositions/image.jpeg")); //250*167
	c7.insets = new Insets(0, 0, 0, 0);
	c7.gridx = 0;
	c7.gridy = 0;
	c7.gridwidth=2;
	p7.add(image7, c7);
										
	JLabel text7 = new JLabel(count[7][0]);
	text7.setFont(fonttext);
	c7.gridx = 0;
	c7.gridy = 1;
	c7.insets = new Insets(5, 0, 5, 0);
	p7.add(text7, c7);
								
	JLabel count7 = new JLabel(count[7][1]);
	count7.setFont(fontcount);
	count7.setBackground(color1);
	c7.gridx = 0;
	c7.gridy = 2;
	c7.insets = new Insets(0, 0, 0, 0);
	p7.add(count7, c7);
								
	JButton b7 = new JButton("Добавить набор");
	b7.setBackground(color2);
	b7.setForeground(Color.white);
	b7.setFont(fontbtn);
	b7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b7.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[7]!=true){
					q.addSetOfSet(7);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[7][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c7.insets = new Insets(2, 0, 5, 0);
	c7.gridx = 0;
	c7.gridy = 3;
	p7.add(b7, c7);	
	
	JButton bt7 = new JButton(new ImageIcon("res/images/8.png"));
	bt7.setVisible(visible[7]);
	bt7.setToolTipText("На изучении");
	bt7.setContentAreaFilled(false);
	bt7.setBorder(null);
	bt7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c7.anchor = GridBagConstraints.LAST_LINE_END;
	c7.gridx = 1;
	c7.gridy = 3;
	p7.add(bt7, c7);
	
	//----------------------------------------------------------------Панель Местоимения
	JPanel p8 = new JPanel();
	GridBagLayout gbl8 = new GridBagLayout();
	p8.setLayout(gbl8);
	p8.setPreferredSize(new Dimension(250, 250));
	p8.setBackground(Color.white);
	p8.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(8);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р8
	GridBagConstraints c8 = new GridBagConstraints();
									
	JLabel image8 = new JLabel(new ImageIcon("files/popular/pronouns/image.jpeg")); //250*167
	c8.insets = new Insets(0, 0, 0, 0);
	c8.gridx = 0;
	c8.gridy = 0;
	c8.gridwidth=2;
	p8.add(image8, c8);
										
	JLabel text8 = new JLabel(count[8][0]);
	text8.setFont(fonttext);
	c8.gridx = 0;
	c8.gridy = 1;
	c8.insets = new Insets(5, 0, 5, 0);
	p8.add(text8, c8);
								
	JLabel count8 = new JLabel(count[8][1]);
	count8.setFont(fontcount);
	count8.setBackground(color1);
	c8.gridx = 0;
	c8.gridy = 2;
	c8.insets = new Insets(0, 0, 0, 0);
	p8.add(count8, c8);
								
	JButton b8 = new JButton("Добавить набор");
	b8.setBackground(color2);
	b8.setForeground(Color.white);
	b8.setFont(fontbtn);
	b8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b8.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[8]!=true){
					q.addSetOfSet(8);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[8][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c8.insets = new Insets(2, 0, 5, 0);
	c8.gridx = 0;
	c8.gridy = 3;
	p8.add(b8, c8);	
			
	JButton bt8 = new JButton(new ImageIcon("res/images/8.png"));
	bt8.setVisible(visible[8]);
	bt8.setToolTipText("На изучении");
	bt8.setContentAreaFilled(false);
	bt8.setBorder(null);
	bt8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c8.anchor = GridBagConstraints.LAST_LINE_END;
	c8.gridx = 1;
	c8.gridy = 3;
	p8.add(bt8, c8);
	
	gl1.add(box1);//Инормационный блок
	gl1.add(box12);//Блок 1
	box12.add(p1);
	box12.add(Box.createHorizontalStrut(50));
	box12.add(p2);
	box12.add(Box.createHorizontalStrut(50));
	box12.add(p3);
	box12.add(Box.createHorizontalStrut(50));
	box12.add(p4);
				
	gl1.add(box13);//Отсуп между блоками
				
	gl1.add(box14);//Блок 2
	box14.add(p5);
	box14.add(Box.createHorizontalStrut(50));
	box14.add(p6);
	box14.add(Box.createHorizontalStrut(50));
	box14.add(p7);
	box14.add(Box.createHorizontalStrut(50));
	box14.add(p8);
	flp.add(gl1);
	//--------------------------------------------------------Конец набора ПОПУЛЯРНОЕ
	}
	//Разворачивания сворачивания набора
	private void actionClickBtn1() {
		if(click==0){
			click=1;
			box13.setVisible(true);
			box14.setVisible(true);
			gl1.setPreferredSize(new Dimension(1165, 617));
		}else{
			click=0;
			gl1.setPreferredSize(new Dimension(1165, 315));
			box13.setVisible(false);
			box14.setVisible(false);
		}
	}
			
	// Набор О ЧЕЛОВЕКЕ (18 подпанелей)
	private void set2(){
	
	//Информация к набору
	Box box21 = Box.createHorizontalBox();
	box21.setPreferredSize(new Dimension(973, 50));
	//box21.setBorder(Line1);
	
	Box	box22 = Box.createHorizontalBox();
	box22.setPreferredSize(new Dimension(1150, 250));
						
	box23 = Box.createHorizontalBox();// Отступ между боксами
	box23.setPreferredSize(new Dimension(1150, 40));
	box23.setVisible(false);
						
	box24 = Box.createHorizontalBox();
	box24.setPreferredSize(new Dimension(1150, 250));
	box24.setVisible(false);
	
	box25 = Box.createHorizontalBox();// Отступ между боксами
	box25.setPreferredSize(new Dimension(1150, 40));
	box25.setVisible(false);
						
	box26 = Box.createHorizontalBox();
	box26.setPreferredSize(new Dimension(1150, 250));
	box26.setVisible(false);
	
	box27 = Box.createHorizontalBox();// Отступ между боксами
	box27.setPreferredSize(new Dimension(1150, 40));
	box27.setVisible(false);
						
	box28 = Box.createHorizontalBox();
	box28.setPreferredSize(new Dimension(1150, 250));
	box28.setVisible(false);
	
	box29 = Box.createHorizontalBox();// Отступ между боксами
	box29.setPreferredSize(new Dimension(1150, 40));
	box29.setVisible(false);
						
	box30 = Box.createHorizontalBox();
	box30.setPreferredSize(new Dimension(1150, 250));
	box30.setVisible(false);
	
	JButton btn2 = new JButton(set[2]);
	btn2.setFont(fonttext);
	//btn21.setContentAreaFilled(false);
	//btn21.setBorder(null);
	btn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	btn2.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		actionClickBtn2();
	}
	});
	box21.add(btn2);
	box21.add(Box.createHorizontalGlue());
				
	JButton btn21 = new JButton("еще");
	btn21.setFont(fontbtn);
	btn21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	btn21.addActionListener(new ActionListener() {

	@Override
	public void actionPerformed(ActionEvent e) {
		actionClickBtn2();
	}
	});
	box21.add(btn21);
				
	//Главная панель набора gl2
	gl2 = new JPanel();
	FlowLayout glp2 = new FlowLayout();
	gl2.setPreferredSize(new Dimension(1165, 315));//315
	gl2.setLayout(glp2);
	//gl2.setBorder(Line1);
	gl2.setOpaque(false);
				
	// ------------------------------------------------------------Панель Эмоции (экспертный уровень)
	JPanel p9 = new JPanel();
	GridBagLayout gbl9 = new GridBagLayout();
	p9.setLayout(gbl9);
	p9.setPreferredSize(new Dimension(250, 250));
	p9.setBackground(Color.white);
	p9.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(9);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р9
	GridBagConstraints c9 = new GridBagConstraints();
				
	JLabel image9 = new JLabel(new ImageIcon("files/about the person/emotion expert/image.jpeg")); //250*167
	c9.gridwidth=2;
	c9.gridx = 0;
	c9.gridy = 0;
	p9.add(image9, c9);
	
	JLabel text9 = new JLabel(count[9][0]);
	text9.setFont(fonttext);
	c9.gridx = 0;
	c9.gridy = 1;
	c9.insets = new Insets(5, 0, 5, 0);
	p9.add(text9, c9);
		
	JLabel count9 = new JLabel(count[9][1]);
	count9.setFont(fontcount);
	c9.insets = new Insets(0, 0, 0, 0);
	c9.gridx = 0;
	c9.gridy = 2;
	p9.add(count9, c9);
		
	JButton b9 = new JButton("Добавить набор");
	b9.setBackground(color2);
	b9.setForeground(Color.white);
	b9.setFont(fontbtn);
	b9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b9.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[9]!=true){
					q.addSetOfSet(9);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[9][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c9.insets = new Insets(2, 0, 5, 0);
	c9.gridx = 0;
	c9.gridy = 3;
	p9.add(b9, c9);
	
	JButton bt9 = new JButton(new ImageIcon("res/images/8.png"));
	bt9.setVisible(visible[9]);
	bt9.setToolTipText("На изучении");
	bt9.setContentAreaFilled(false);
	bt9.setBorder(null);
	bt9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c9.anchor = GridBagConstraints.LAST_LINE_END;
	c9.gridx = 1;
	c9.gridy = 3;
	p9.add(bt9, c9);
	
	//------------------------------------------------------Панель Эмоции (продвинутый уровень)
	JPanel p10 = new JPanel();
	GridBagLayout gbl10 = new GridBagLayout();
	p10.setLayout(gbl10);
	p10.setPreferredSize(new Dimension(250, 250));
	p10.setBackground(Color.white);
	p10.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(10);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р9
	GridBagConstraints c10 = new GridBagConstraints();
				
	JLabel image10 = new JLabel(new ImageIcon("files/about the person/emotion pro/image.jpeg")); //250*167
	c10.gridwidth=2;
	c10.gridx = 0;
	c10.gridy = 0;
	p10.add(image10, c10);
	
	JLabel text10 = new JLabel(count[10][0]);
	text10.setFont(fonttext);
	c10.gridx = 0;
	c10.gridy = 1;
	c10.insets = new Insets(5, 0, 5, 0);
	p10.add(text10, c10);
		
	JLabel count10 = new JLabel(count[10][1]);
	count10.setFont(fontcount);
	c10.insets = new Insets(0, 0, 0, 0);
	c10.gridx = 0;
	c10.gridy = 2;
	p10.add(count10, c10);
		
	JButton b10 = new JButton("Добавить набор");
	b10.setBackground(color2);
	b10.setForeground(Color.white);
	b10.setFont(fontbtn);
	b10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b10.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[10]!=true){
					q.addSetOfSet(10);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[10][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c10.insets = new Insets(2, 0, 5, 0);
	c10.gridx = 0;
	c10.gridy = 3;
	p10.add(b10, c10);
	
	JButton bt10 = new JButton(new ImageIcon("res/images/8.png"));
	bt10.setVisible(visible[10]);
	bt10.setToolTipText("На изучении");
	bt10.setContentAreaFilled(false);
	bt10.setBorder(null);
	bt10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c10.anchor = GridBagConstraints.LAST_LINE_END;
	c10.gridx = 1;
	c10.gridy = 3;
	p10.add(bt10, c10);
		
	//---------------------------------------------------------Панель Эмоции (для начинающих)
	JPanel p11 = new JPanel();
	GridBagLayout gbl11 = new GridBagLayout();
	p11.setLayout(gbl11);
	p11.setPreferredSize(new Dimension(250, 250));
	p11.setBackground(Color.white);
	p11.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(11);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р11
	GridBagConstraints c11 = new GridBagConstraints();
				
	JLabel image11 = new JLabel(new ImageIcon("files/about the person/emotion new/image.jpeg")); //250*167
	c11.gridwidth=2;
	c11.gridx = 0;
	c11.gridy = 0;
	p11.add(image11, c11);
	
	JLabel text11 = new JLabel(count[11][0]);
	text11.setFont(fonttext);
	c11.gridx = 0;
	c11.gridy = 1;
	c11.insets = new Insets(5, 0, 5, 0);
	p11.add(text11, c11);
		
	JLabel count11 = new JLabel(count[11][1]);
	count11.setFont(fontcount);
	c11.insets = new Insets(0, 0, 0, 0);
	c11.gridx = 0;
	c11.gridy = 2;
	p11.add(count11, c11);
		
	JButton b11 = new JButton("Добавить набор");
	b11.setBackground(color2);
	b11.setForeground(Color.white);
	b11.setFont(fontbtn);
	b11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b11.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[11]!=true){
					q.addSetOfSet(11);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[11][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c11.insets = new Insets(2, 0, 5, 0);
	c11.gridx = 0;
	c11.gridy = 3;
	p11.add(b11, c11);
	
	JButton bt11 = new JButton(new ImageIcon("res/images/8.png"));
	bt11.setVisible(visible[11]);
	bt11.setToolTipText("На изучении");
	bt11.setContentAreaFilled(false);
	bt11.setBorder(null);
	bt11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c11.anchor = GridBagConstraints.LAST_LINE_END;
	c11.gridx = 1;
	c11.gridy = 3;
	p11.add(bt11, c11);	
		
	//----------------------------------------------------------Панель Все для дома 
	JPanel p12 = new JPanel();
	GridBagLayout gbl12 = new GridBagLayout();
	p12.setLayout(gbl12);
	p12.setPreferredSize(new Dimension(250, 250));
	p12.setBackground(Color.white);
	p12.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(12);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р12
	GridBagConstraints c12 = new GridBagConstraints();
				
	JLabel image12 = new JLabel(new ImageIcon("files/about the person/housing/image.jpeg")); //250*167
	c12.gridwidth=2;
	c12.gridx = 0;
	c12.gridy = 0;
	p12.add(image12, c12);
	
	JLabel text12 = new JLabel(count[12][0]);
	text12.setFont(fonttext);
	c12.gridx = 0;
	c12.gridy = 1;
	c12.insets = new Insets(5, 0, 5, 0);
	p12.add(text12, c12);
		
	JLabel count12 = new JLabel(count[12][1]);
	count12.setFont(fontcount);
	c12.insets = new Insets(0, 0, 0, 0);
	c12.gridx = 0;
	c12.gridy = 2;
	p12.add(count12, c12);
		
	JButton b12 = new JButton("Добавить набор");
	b12.setBackground(color2);
	b12.setForeground(Color.white);
	b12.setFont(fontbtn);
	b12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b12.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[12]!=true){
					q.addSetOfSet(12);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[12][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c12.insets = new Insets(2, 0, 5, 0);
	c12.gridx = 0;
	c12.gridy = 3;
	p12.add(b12, c12);
	
	JButton bt12 = new JButton(new ImageIcon("res/images/8.png"));
	bt12.setVisible(visible[12]);
	bt12.setToolTipText("На изучении");
	bt12.setContentAreaFilled(false);
	bt12.setBorder(null);
	bt12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c12.anchor = GridBagConstraints.LAST_LINE_END;
	c12.gridx = 1;
	c12.gridy = 3;
	p12.add(bt12, c12);
	
	//----------------------------------------------------Панель Фразовые глаголы для экспертов
	JPanel p13 = new JPanel();
	GridBagLayout gbl13 = new GridBagLayout();
	p13.setLayout(gbl13);
	p13.setPreferredSize(new Dimension(250, 250));
	p13.setBackground(Color.white);
	p13.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(13);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р13
	GridBagConstraints c13 = new GridBagConstraints();
				
	JLabel image13 = new JLabel(new ImageIcon("files/about the person/phrase expert/image.jpg")); //250*167
	c13.gridwidth=2;
	c13.gridx = 0;
	c13.gridy = 0;
	p13.add(image13, c13);
	
	JLabel text13 = new JLabel(count[13][0]);
	text13.setFont(fonttext);
	c13.gridx = 0;
	c13.gridy = 1;
	c13.insets = new Insets(5, 0, 5, 0);
	p13.add(text13, c13);
		
	JLabel count13 = new JLabel(count[13][1]);
	count13.setFont(fontcount);
	c13.insets = new Insets(0, 0, 0, 0);
	c13.gridx = 0;
	c13.gridy = 2;
	p13.add(count13, c13);
		
	JButton b13 = new JButton("Добавить набор");
	b13.setBackground(color2);
	b13.setForeground(Color.white);
	b13.setFont(fontbtn);
	b13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b13.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[13]!=true){
					q.addSetOfSet(13);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[13][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c13.insets = new Insets(2, 0, 5, 0);
	c13.gridx = 0;
	c13.gridy = 3;
	p13.add(b13, c13);
	
	JButton bt13 = new JButton(new ImageIcon("res/images/8.png"));
	bt13.setVisible(visible[13]);
	bt13.setToolTipText("На изучении");
	bt13.setContentAreaFilled(false);
	bt13.setBorder(null);
	bt13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c13.anchor = GridBagConstraints.LAST_LINE_END;
	c13.gridx = 1;
	c13.gridy = 3;
	p13.add(bt13, c13);
		
	//--------------------------------------------------------------Панель Фразовые глаголы (средний уровень) 
	JPanel p14 = new JPanel();
	GridBagLayout gbl14 = new GridBagLayout();
	p14.setLayout(gbl14);
	p14.setPreferredSize(new Dimension(250, 250));
	p14.setBackground(Color.white);
	p14.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(14);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р14
	GridBagConstraints c14 = new GridBagConstraints();
				
	JLabel image14 = new JLabel(new ImageIcon("files/about the person/phrase beginner/image.jpg")); //250*167
	c14.gridwidth=2;
	c14.gridx = 0;
	c14.gridy = 0;
	p14.add(image14, c14);
	
	JLabel text14 = new JLabel(count[14][0]);
	text14.setFont(fonttext);
	c14.gridx = 0;
	c14.gridy = 1;
	c14.insets = new Insets(5, 0, 5, 0);
	p14.add(text14, c14);
		
	JLabel count14 = new JLabel(count[14][1]);
	count14.setFont(fontcount);
	c14.insets = new Insets(0, 0, 0, 0);
	c14.gridx = 0;
	c14.gridy = 2;
	p14.add(count14, c14);
		
	JButton b14 = new JButton("Добавить набор");
	b14.setBackground(color2);
	b14.setForeground(Color.white);
	b14.setFont(fontbtn);
	b14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b14.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[14]!=true){
					q.addSetOfSet(14);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[14][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c14.insets = new Insets(2, 0, 5, 0);
	c14.gridx = 0;
	c14.gridy = 3;
	p14.add(b14, c14);
	
	JButton bt14 = new JButton(new ImageIcon("res/images/8.png"));
	bt14.setVisible(visible[14]);
	bt14.setToolTipText("На изучении");
	bt14.setContentAreaFilled(false);
	bt14.setBorder(null);
	bt14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c14.anchor = GridBagConstraints.LAST_LINE_END;
	c14.gridx = 1;
	c14.gridy = 3;
	p14.add(bt14, c14);	
	
	//------------------------------------------------------------------Панель Фразовые глаголы для начинающих
	JPanel p15 = new JPanel();
	GridBagLayout gbl15 = new GridBagLayout();
	p15.setLayout(gbl15);
	p15.setPreferredSize(new Dimension(250, 250));
	p15.setBackground(Color.white);
	p15.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(15);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р15
	GridBagConstraints c15 = new GridBagConstraints();
				
	JLabel image15 = new JLabel(new ImageIcon("files/about the person/phrase norm/image.jpg")); //250*167
	c15.gridwidth=2;
	c15.gridx = 0;
	c15.gridy = 0;
	p15.add(image15, c15);
	
	JLabel text15 = new JLabel(count[15][0]);
	text15.setFont(fonttext);
	c15.gridx = 0;
	c15.gridy = 1;
	c15.insets = new Insets(5, 0, 5, 0);
	p15.add(text15, c15);
		
	JLabel count15 = new JLabel(count[15][1]);
	count15.setFont(fontcount);
	c15.insets = new Insets(0, 0, 0, 0);
	c15.gridx = 0;
	c15.gridy = 2;
	p15.add(count15, c15);
		
	JButton b15 = new JButton("Добавить набор");
	b15.setBackground(color2);
	b15.setForeground(Color.white);
	b15.setFont(fontbtn);
	b15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b15.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[15]!=true){
					q.addSetOfSet(15);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[15][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c15.insets = new Insets(2, 0, 5, 0);
	c15.gridx = 0;
	c15.gridy = 3;
	p15.add(b15, c15);
	
	JButton bt15 = new JButton(new ImageIcon("res/images/8.png"));
	bt15.setVisible(visible[15]);
	bt15.setToolTipText("На изучении");
	bt15.setContentAreaFilled(false);
	bt15.setBorder(null);
	bt15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c15.anchor = GridBagConstraints.LAST_LINE_END;
	c15.gridx = 1;
	c15.gridy = 3;
	p15.add(bt15, c15);
	
	//----------------------------------------------------------------Панель О чувствах
	JPanel p16 = new JPanel();
	GridBagLayout gbl16 = new GridBagLayout();
	p16.setLayout(gbl16);
	p16.setPreferredSize(new Dimension(250, 250));
	p16.setBackground(Color.white);
	p16.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(16);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р16
	GridBagConstraints c16 = new GridBagConstraints();
				
	JLabel image16 = new JLabel(new ImageIcon("files/about the person/fellings/image.jpg")); //250*167
	c16.gridwidth=2;
	c16.gridx = 0;
	c16.gridy = 0;
	p16.add(image16, c16);
	
	JLabel text16 = new JLabel(count[16][0]);
	text16.setFont(fonttext);
	c16.gridx = 0;
	c16.gridy = 1;
	c16.insets = new Insets(5, 0, 5, 0);
	p16.add(text16, c16);
		
	JLabel count16 = new JLabel(count[16][1]);
	count16.setFont(fontcount);
	c16.insets = new Insets(0, 0, 0, 0);
	c16.gridx = 0;
	c16.gridy = 2;
	p16.add(count16, c16);
		
	JButton b16 = new JButton("Добавить набор");
	b16.setBackground(color2);
	b16.setForeground(Color.white);
	b16.setFont(fontbtn);
	b16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b16.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[16]!=true){
					q.addSetOfSet(16);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[16][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c16.insets = new Insets(2, 0, 5, 0);
	c16.gridx = 0;
	c16.gridy = 3;
	p16.add(b16, c16);
	
	JButton bt16 = new JButton(new ImageIcon("res/images/8.png"));
	bt16.setVisible(visible[16]);
	bt16.setToolTipText("На изучении");
	bt16.setContentAreaFilled(false);
	bt16.setBorder(null);
	bt16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c16.anchor = GridBagConstraints.LAST_LINE_END;
	c16.gridx = 1;
	c16.gridy = 3;
	p16.add(bt16, c16);
	
	//----------------------------------------------------------------Панель За едой
	JPanel p17 = new JPanel();
	GridBagLayout gbl17 = new GridBagLayout();
	p17.setLayout(gbl17);
	p17.setPreferredSize(new Dimension(250, 250));
	p17.setBackground(Color.white);
	p17.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(17);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р17
	GridBagConstraints c17 = new GridBagConstraints();
				
	JLabel image17 = new JLabel(new ImageIcon("files/about the person/for food/image.jpg")); //250*177
	c17.gridwidth=2;
	c17.gridx = 0;
	c17.gridy = 0;
	p17.add(image17, c17);
	
	JLabel text17 = new JLabel(count[17][0]);
	text17.setFont(fonttext);
	c17.gridx = 0;
	c17.gridy = 1;
	c17.insets = new Insets(5, 0, 5, 0);
	p17.add(text17, c17);
		
	JLabel count17 = new JLabel(count[17][1]);
	count17.setFont(fontcount);
	c17.insets = new Insets(0, 0, 0, 0);
	c17.gridx = 0;
	c17.gridy = 2;
	p17.add(count17, c17);
		
	JButton b17 = new JButton("Добавить набор");
	b17.setBackground(color2);
	b17.setForeground(Color.white);
	b17.setFont(fontbtn);
	b17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b17.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[17]!=true){
					q.addSetOfSet(17);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[17][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c17.insets = new Insets(2, 0, 5, 0);
	c17.gridx = 0;
	c17.gridy = 3;
	p17.add(b17, c17);
	
	JButton bt17 = new JButton(new ImageIcon("res/images/8.png"));
	bt17.setVisible(visible[17]);
	bt17.setToolTipText("На изучении");
	bt17.setContentAreaFilled(false);
	bt17.setBorder(null);
	bt17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c17.anchor = GridBagConstraints.LAST_LINE_END;
	c17.gridx = 1;
	c17.gridy = 3;
	p17.add(bt17, c17);
	
	//----------------------------------------------------------------Панель Косметика
	JPanel p18 = new JPanel();
	GridBagLayout gbl18 = new GridBagLayout();
	p18.setLayout(gbl18);
	p18.setPreferredSize(new Dimension(250, 250));
	p18.setBackground(Color.white);
	p18.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(18);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р18
	GridBagConstraints c18 = new GridBagConstraints();
				
	JLabel image18 = new JLabel(new ImageIcon("files/about the person/cosmetic/image.jpg")); //250*187
	c18.gridwidth=2;
	c18.gridx = 0;
	c18.gridy = 0;
	p18.add(image18, c18);
	
	JLabel text18 = new JLabel(count[18][0]);
	text18.setFont(fonttext);
	c18.gridx = 0;
	c18.gridy = 1;
	c18.insets = new Insets(5, 0, 5, 0);
	p18.add(text18, c18);
		
	JLabel count18 = new JLabel(count[18][1]);
	count18.setFont(fontcount);
	c18.insets = new Insets(0, 0, 0, 0);
	c18.gridx = 0;
	c18.gridy = 2;
	p18.add(count18, c18);
		
	JButton b18 = new JButton("Добавить набор");
	b18.setBackground(color2);
	b18.setForeground(Color.white);
	b18.setFont(fontbtn);
	b18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b18.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[18]!=true){
					q.addSetOfSet(18);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[18][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c18.insets = new Insets(2, 0, 5, 0);
	c18.gridx = 0;
	c18.gridy = 3;
	p18.add(b18, c18);
	
	JButton bt18 = new JButton(new ImageIcon("res/images/8.png"));
	bt18.setVisible(visible[18]);
	bt18.setToolTipText("На изучении");
	bt18.setContentAreaFilled(false);
	bt18.setBorder(null);
	bt18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c18.anchor = GridBagConstraints.LAST_LINE_END;
	c18.gridx = 1;
	c18.gridy = 3;
	p18.add(bt18, c18);
	
	//----------------------------------------------------------------Панель Религия
	JPanel p19 = new JPanel();
	GridBagLayout gbl19 = new GridBagLayout();
	p19.setLayout(gbl19);
	p19.setPreferredSize(new Dimension(250, 250));
	p19.setBackground(Color.white);
	p19.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(19);
		}
		public void mousePressed(MouseEvent e) {
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseExited(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}});
	p19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
	// Добавляем елементы на панель р19
	GridBagConstraints c19 = new GridBagConstraints();
				
	JLabel image19 = new JLabel(new ImageIcon("files/about the person/religion/image.jpg")); //250*197
	c19.gridwidth=2;
	c19.gridx = 0;
	c19.gridy = 0;
	p19.add(image19, c19);
	
	JLabel text19 = new JLabel(count[19][0]);
	text19.setFont(fonttext);
	c19.gridx = 0;
	c19.gridy = 1;
	c19.insets = new Insets(5, 0, 5, 0);
	p19.add(text19, c19);
		
	JLabel count19 = new JLabel(count[19][1]);
	count19.setFont(fontcount);
	c19.insets = new Insets(0, 0, 0, 0);
	c19.gridx = 0;
	c19.gridy = 2;
	p19.add(count19, c19);
		
	JButton b19 = new JButton("Добавить набор");
	b19.setBackground(color2);
	b19.setForeground(Color.white);
	b19.setFont(fontbtn);
	b19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b19.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (visible[19]!=true){
					q.addSetOfSet(19);
					repaintWindow();
				}else{
					JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[19][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c19.insets = new Insets(2, 0, 5, 0);
	c19.gridx = 0;
	c19.gridy = 3;
	p19.add(b19, c19);
	
	JButton bt19 = new JButton(new ImageIcon("res/images/8.png"));
	bt19.setVisible(visible[19]);
	bt19.setToolTipText("На изучении");
	bt19.setContentAreaFilled(false);
	bt19.setBorder(null);
	bt19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	c19.anchor = GridBagConstraints.LAST_LINE_END;
	c19.gridx = 1;
	c19.gridy = 3;
	p19.add(bt19, c19);
	
	//----------------------------------------------------------------Панель Заболевания
		JPanel p20 = new JPanel();
		GridBagLayout gbl20 = new GridBagLayout();
		p20.setLayout(gbl20);
		p20.setPreferredSize(new Dimension(250, 250));
		p20.setBackground(Color.white);
		p20.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(20);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р20
		GridBagConstraints c20 = new GridBagConstraints();
					
		JLabel image20 = new JLabel(new ImageIcon("files/about the person/disease/image.jpg")); //250*207
		c20.gridwidth=2;
		c20.gridx = 0;
		c20.gridy = 0;
		p20.add(image20, c20);
		
		JLabel text20 = new JLabel(count[20][0]);
		text20.setFont(fonttext);
		c20.gridx = 0;
		c20.gridy = 1;
		c20.insets = new Insets(5, 0, 5, 0);
		p20.add(text20, c20);
			
		JLabel count20 = new JLabel(count[20][1]);
		count20.setFont(fontcount);
		c20.insets = new Insets(0, 0, 0, 0);
		c20.gridx = 0;
		c20.gridy = 2;
		p20.add(count20, c20);
			
		JButton b20 = new JButton("Добавить набор");
		b20.setBackground(color2);
		b20.setForeground(Color.white);
		b20.setFont(fontbtn);
		b20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b20.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[20]!=true){
						q.addSetOfSet(20);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[20][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c20.insets = new Insets(2, 0, 5, 0);
		c20.gridx = 0;
		c20.gridy = 3;
		p20.add(b20, c20);
		
		JButton bt20 = new JButton(new ImageIcon("res/images/8.png"));
		bt20.setVisible(visible[20]);
		bt20.setToolTipText("На изучении");
		bt20.setContentAreaFilled(false);
		bt20.setBorder(null);
		bt20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c20.anchor = GridBagConstraints.LAST_LINE_END;
		c20.gridx = 1;
		c20.gridy = 3;
		p20.add(bt20, c20);
	
		//----------------------------------------------------------------Панель Мебель и посуда
		JPanel p21 = new JPanel();
		GridBagLayout gbl21 = new GridBagLayout();
		p21.setLayout(gbl21);
		p21.setPreferredSize(new Dimension(250, 250));
		p21.setBackground(Color.white);
		p21.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(21);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р21
		GridBagConstraints c21 = new GridBagConstraints();
					
		JLabel image21 = new JLabel(new ImageIcon("files/about the person/furniture/image.jpg")); //250*217
		c21.gridwidth=2;
		c21.gridx = 0;
		c21.gridy = 0;
		p21.add(image21, c21);
		
		JLabel text21 = new JLabel(count[21][0]);
		text21.setFont(fonttext);
		c21.gridx = 0;
		c21.gridy = 1;
		c21.insets = new Insets(5, 0, 5, 0);
		p21.add(text21, c21);
			
		JLabel count21 = new JLabel(count[21][1]);
		count21.setFont(fontcount);
		c21.insets = new Insets(0, 0, 0, 0);
		c21.gridx = 0;
		c21.gridy = 2;
		p21.add(count21, c21);
			
		JButton b21 = new JButton("Добавить набор");
		b21.setBackground(color2);
		b21.setForeground(Color.white);
		b21.setFont(fontbtn);
		b21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b21.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[21]!=true){
						q.addSetOfSet(21);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[21][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c21.insets = new Insets(2, 0, 5, 0);
		c21.gridx = 0;
		c21.gridy = 3;
		p21.add(b21, c21);
		
		JButton bt21 = new JButton(new ImageIcon("res/images/8.png"));
		bt21.setVisible(visible[21]);
		bt21.setToolTipText("На изучении");
		bt21.setContentAreaFilled(false);
		bt21.setBorder(null);
		bt21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c21.anchor = GridBagConstraints.LAST_LINE_END;
		c21.gridx = 1;
		c21.gridy = 3;
		p21.add(bt21, c21);	
		
			
		//----------------------------------------------------------------Панель Одежда и обувь
		JPanel p22 = new JPanel();
		GridBagLayout gbl22 = new GridBagLayout();
		p22.setLayout(gbl22);
		p22.setPreferredSize(new Dimension(250, 250));
		p22.setBackground(Color.white);
		p22.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(22);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р22
		GridBagConstraints c22 = new GridBagConstraints();
					
		JLabel image22 = new JLabel(new ImageIcon("files/about the person/clothes/image.jpg")); //250*227
		c22.gridwidth=2;
		c22.gridx = 0;
		c22.gridy = 0;
		p22.add(image22, c22);
		
		JLabel text22 = new JLabel(count[22][0]);
		text22.setFont(fonttext);
		c22.gridx = 0;
		c22.gridy = 1;
		c22.insets = new Insets(5, 0, 5, 0);
		p22.add(text22, c22);
			
		JLabel count22 = new JLabel(count[22][1]);
		count22.setFont(fontcount);
		c22.insets = new Insets(0, 0, 0, 0);
		c22.gridx = 0;
		c22.gridy = 2;
		p22.add(count22, c22);
			
		JButton b22 = new JButton("Добавить набор");
		b22.setBackground(color2);
		b22.setForeground(Color.white);
		b22.setFont(fontbtn);
		b22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b22.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[22]!=true){
						q.addSetOfSet(22);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[22][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c22.insets = new Insets(2, 0, 5, 0);
		c22.gridx = 0;
		c22.gridy = 3;
		p22.add(b22, c22);
		
		JButton bt22 = new JButton(new ImageIcon("res/images/8.png"));
		bt22.setVisible(visible[22]);
		bt22.setToolTipText("На изучении");
		bt22.setContentAreaFilled(false);
		bt22.setBorder(null);
		bt22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c22.anchor = GridBagConstraints.LAST_LINE_END;
		c22.gridx = 1;
		c22.gridy = 3;
		p22.add(bt22, c22);	
		
		//----------------------------------------------------------------Панель Обряды
		JPanel p23 = new JPanel();
		GridBagLayout gbl23 = new GridBagLayout();
		p23.setLayout(gbl23);
		p23.setPreferredSize(new Dimension(250, 250));
		p23.setBackground(Color.white);
		p23.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(23);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р23
		GridBagConstraints c23 = new GridBagConstraints();
					
		JLabel image23 = new JLabel(new ImageIcon("files/about the person/rites/image.jpg")); //250*237
		c23.gridwidth=2;
		c23.gridx = 0;
		c23.gridy = 0;
		p23.add(image23, c23);
		
		JLabel text23 = new JLabel(count[23][0]);
		text23.setFont(fonttext);
		c23.gridx = 0;
		c23.gridy = 1;
		c23.insets = new Insets(5, 0, 5, 0);
		p23.add(text23, c23);
			
		JLabel count23 = new JLabel(count[23][1]);
		count23.setFont(fontcount);
		c23.insets = new Insets(0, 0, 0, 0);
		c23.gridx = 0;
		c23.gridy = 2;
		p23.add(count23, c23);
			
		JButton b23 = new JButton("Добавить набор");
		b23.setBackground(color2);
		b23.setForeground(Color.white);
		b23.setFont(fontbtn);
		b23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b23.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[23]!=true){
						q.addSetOfSet(23);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[23][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c23.insets = new Insets(2, 0, 5, 0);
		c23.gridx = 0;
		c23.gridy = 3;
		p23.add(b23, c23);
		
		JButton bt23 = new JButton(new ImageIcon("res/images/8.png"));
		bt23.setVisible(visible[23]);
		bt23.setToolTipText("На изучении");
		bt23.setContentAreaFilled(false);
		bt23.setBorder(null);
		bt23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c23.anchor = GridBagConstraints.LAST_LINE_END;
		c23.gridx = 1;
		c23.gridy = 3;
		p23.add(bt23, c23);
	
		//----------------------------------------------------------------Панель Праздники
		JPanel p24 = new JPanel();
		GridBagLayout gbl24 = new GridBagLayout();
		p24.setLayout(gbl24);
		p24.setPreferredSize(new Dimension(250, 250));
		p24.setBackground(Color.white);
		p24.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(24);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р24
		GridBagConstraints c24 = new GridBagConstraints();
					
		JLabel image24 = new JLabel(new ImageIcon("files/about the person/holiday/image.jpg")); //250*247
		c24.gridwidth=2;
		c24.gridx = 0;
		c24.gridy = 0;
		p24.add(image24, c24);
		
		JLabel text24 = new JLabel(count[24][0]);
		text24.setFont(fonttext);
		c24.gridx = 0;
		c24.gridy = 1;
		c24.insets = new Insets(5, 0, 5, 0);
		p24.add(text24, c24);
			
		JLabel count24 = new JLabel(count[24][1]);
		count24.setFont(fontcount);
		c24.insets = new Insets(0, 0, 0, 0);
		c24.gridx = 0;
		c24.gridy = 2;
		p24.add(count24, c24);
			
		JButton b24 = new JButton("Добавить набор");
		b24.setBackground(color2);
		b24.setForeground(Color.white);
		b24.setFont(fontbtn);
		b24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b24.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[24]!=true){
						q.addSetOfSet(24);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[24][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c24.insets = new Insets(2, 0, 5, 0);
		c24.gridx = 0;
		c24.gridy = 3;
		p24.add(b24, c24);
		
		JButton bt24 = new JButton(new ImageIcon("res/images/8.png"));
		bt24.setVisible(visible[24]);
		bt24.setToolTipText("На изучении");
		bt24.setContentAreaFilled(false);
		bt24.setBorder(null);
		bt24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c24.anchor = GridBagConstraints.LAST_LINE_END;
		c24.gridx = 1;
		c24.gridy = 3;
		p24.add(bt24, c24);	
	
		//----------------------------------------------------------------Панель Внешность
		JPanel p25 = new JPanel();
		GridBagLayout gbl25 = new GridBagLayout();
		p25.setLayout(gbl25);
		p25.setPreferredSize(new Dimension(250, 250));
		p25.setBackground(Color.white);
		p25.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(25);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р25
		GridBagConstraints c25 = new GridBagConstraints();
					
		JLabel image25 = new JLabel(new ImageIcon("files/about the person/appearance/image.jpg")); //250*257
		c25.gridwidth=2;
		c25.gridx = 0;
		c25.gridy = 0;
		p25.add(image25, c25);
		
		JLabel text25 = new JLabel(count[25][0]);
		text25.setFont(fonttext);
		c25.gridx = 0;
		c25.gridy = 1;
		c25.insets = new Insets(5, 0, 5, 0);
		p25.add(text25, c25);
			
		JLabel count25 = new JLabel(count[25][1]);
		count25.setFont(fontcount);
		c25.insets = new Insets(0, 0, 0, 0);
		c25.gridx = 0;
		c25.gridy = 2;
		p25.add(count25, c25);
			
		JButton b25 = new JButton("Добавить набор");
		b25.setBackground(color2);
		b25.setForeground(Color.white);
		b25.setFont(fontbtn);
		b25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b25.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[25]!=true){
						q.addSetOfSet(25);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[25][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c25.insets = new Insets(2, 0, 5, 0);
		c25.gridx = 0;
		c25.gridy = 3;
		p25.add(b25, c25);
		
		JButton bt25 = new JButton(new ImageIcon("res/images/8.png"));
		bt25.setVisible(visible[25]);
		bt25.setToolTipText("На изучении");
		bt25.setContentAreaFilled(false);
		bt25.setBorder(null);
		bt25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c25.anchor = GridBagConstraints.LAST_LINE_END;
		c25.gridx = 1;
		c25.gridy = 3;
		p25.add(bt25, c25);	
	
		//----------------------------------------------------------------Панель Еда и напитки
		JPanel p26 = new JPanel();
		GridBagLayout gbl26 = new GridBagLayout();
		p26.setLayout(gbl26);
		p26.setPreferredSize(new Dimension(250, 250));
		p26.setBackground(Color.white);
		p26.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(26);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р26
		GridBagConstraints c26 = new GridBagConstraints();
					
		JLabel image26 = new JLabel(new ImageIcon("files/about the person/food and drinks/image.jpg")); //250*267
		c26.gridwidth=2;
		c26.gridx = 0;
		c26.gridy = 0;
		p26.add(image26, c26);
		
		JLabel text26 = new JLabel(count[26][0]);
		text26.setFont(fonttext);
		c26.gridx = 0;
		c26.gridy = 1;
		c26.insets = new Insets(5, 0, 5, 0);
		p26.add(text26, c26);
			
		JLabel count26 = new JLabel(count[26][1]);
		count26.setFont(fontcount);
		c26.insets = new Insets(0, 0, 0, 0);
		c26.gridx = 0;
		c26.gridy = 2;
		p26.add(count26, c26);
			
		JButton b26 = new JButton("Добавить набор");
		b26.setBackground(color2);
		b26.setForeground(Color.white);
		b26.setFont(fontbtn);
		b26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b26.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[26]!=true){
						q.addSetOfSet(26);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[26][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c26.insets = new Insets(2, 0, 5, 0);
		c26.gridx = 0;
		c26.gridy = 3;
		p26.add(b26, c26);
		
		JButton bt26 = new JButton(new ImageIcon("res/images/8.png"));
		bt26.setVisible(visible[26]);
		bt26.setToolTipText("На изучении");
		bt26.setContentAreaFilled(false);
		bt26.setBorder(null);
		bt26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c26.anchor = GridBagConstraints.LAST_LINE_END;
		c26.gridx = 1;
		c26.gridy = 3;
		p26.add(bt26, c26);		
		
	gl2.add(box21);//Инормационный блок
	gl2.add(box22);//Блок 1
	box22.add(p9);
	box22.add(Box.createHorizontalStrut(50));
	box22.add(p10);
	box22.add(Box.createHorizontalStrut(50));
	box22.add(p11);
	box22.add(Box.createHorizontalStrut(50));
	box22.add(p12);
				
	gl2.add(box23);//Отсуп между блоками
				
	gl2.add(box24);//Блок 2
	box24.add(p13);
	box24.add(Box.createHorizontalStrut(50));
	box24.add(p14);
	box24.add(Box.createHorizontalStrut(50));
	box24.add(p15);
	box24.add(Box.createHorizontalStrut(50));
	box24.add(p16);
	
	gl2.add(box25);//Отсуп между блоками
	
	gl2.add(box26);//Блок 3
	box26.add(p17);
	box26.add(Box.createHorizontalStrut(50));
	box26.add(p18);
	box26.add(Box.createHorizontalStrut(50));
	box26.add(p19);
	box26.add(Box.createHorizontalStrut(50));
	box26.add(p20);
	
	gl2.add(box27);//Отсуп между блоками
	
	gl2.add(box28);//Блок 4
	box28.add(p21);
	box28.add(Box.createHorizontalStrut(50));
	box28.add(p22);
	box28.add(Box.createHorizontalStrut(50));
	box28.add(p23);
	box28.add(Box.createHorizontalStrut(50));
	box28.add(p24);
	
	gl2.add(box29);//Отсуп между блоками
	
	gl2.add(box30);//Блок 5
	box30.add(p25);
	box30.add(Box.createHorizontalStrut(50));
	box30.add(p26);
	box30.add(Box.createHorizontalStrut(600));
	
	flp.add(gl2);
	}
	//Разворачивания сворачивания набора
	private void actionClickBtn2() {

		if(click==0){
			click=1;
			box23.setVisible(true);
			box24.setVisible(true);
			box25.setVisible(true);
			box26.setVisible(true);
			box27.setVisible(true);
			box28.setVisible(true);
			box29.setVisible(true);
			box30.setVisible(true);
			gl2.setPreferredSize(new Dimension(1165, 1520));
		}else{
			click=0;
			gl2.setPreferredSize(new Dimension(1165, 315));
			box23.setVisible(false);
			box24.setVisible(false);
			box25.setVisible(false);
			box26.setVisible(false);
			box27.setVisible(false);
			box28.setVisible(false);
			box29.setVisible(false);
			box30.setVisible(false);
		}
	}
	
	// Набор ХОББИ (2 подпанели)
	private void set3(){

		//Информация к набору
		Box box31 = Box.createHorizontalBox();
		box31.setPreferredSize(new Dimension(973, 50));
				
		Box	box32 = Box.createHorizontalBox();
		box32.setPreferredSize(new Dimension(1150, 250));
						
		JButton btn3 = new JButton(set[3]);
		btn3.setFont(fonttext);
		btn3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		box31.add(btn3);
		box31.add(Box.createHorizontalGlue());
									
		//Главная панель набора gl3
		gl3 = new JPanel();
		FlowLayout glp3 = new FlowLayout();
		gl3.setPreferredSize(new Dimension(1165, 315));//315
		gl3.setLayout(glp3);
		gl3.setOpaque(false);
					
		// ------------------------------------------------------------Панель Музыкальные Стили 
		JPanel p27 = new JPanel();
		GridBagLayout gbl27 = new GridBagLayout();
		p27.setLayout(gbl27);
		p27.setPreferredSize(new Dimension(250, 250));
		p27.setBackground(Color.white);
		p27.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(27);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р27
		GridBagConstraints c27 = new GridBagConstraints();
					
		JLabel image27 = new JLabel(new ImageIcon("files/hobby/musical styles/image.jpg")); //250*167
		c27.gridwidth=2;
		c27.gridx = 0;
		c27.gridy = 0;
		p27.add(image27, c27);
		
		JLabel text27 = new JLabel(count[27][0]);
		text27.setFont(fonttext);
		c27.gridx = 0;
		c27.gridy = 1;
		c27.insets = new Insets(5, 0, 5, 0);
		p27.add(text27, c27);
			
		JLabel count27 = new JLabel(count[27][1]);
		count27.setFont(fontcount);
		c27.insets = new Insets(0, 0, 0, 0);
		c27.gridx = 0;
		c27.gridy = 2;
		p27.add(count27, c27);
			
		JButton b27 = new JButton("Добавить набор");
		b27.setBackground(color2);
		b27.setForeground(Color.white);
		b27.setFont(fontbtn);
		b27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b27.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[27]!=true){
						q.addSetOfSet(27);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[27][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c27.insets = new Insets(2, 0, 5, 0);
		c27.gridx = 0;
		c27.gridy = 3;
		p27.add(b27, c27);
		
		JButton bt27 = new JButton(new ImageIcon("res/images/8.png"));
		bt27.setVisible(visible[27]);
		bt27.setToolTipText("На изучении");
		bt27.setContentAreaFilled(false);
		bt27.setBorder(null);
		bt27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c27.anchor = GridBagConstraints.LAST_LINE_END;
		c27.gridx = 1;
		c27.gridy = 3;
		p27.add(bt27, c27);
		
		//------------------------------------------------------Панель Образ жизни
		JPanel p28 = new JPanel();
		GridBagLayout gbl28 = new GridBagLayout();
		p28.setLayout(gbl28);
		p28.setPreferredSize(new Dimension(250, 250));
		p28.setBackground(Color.white);
		p28.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(28);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р28
		GridBagConstraints c28 = new GridBagConstraints();
					
		JLabel image28 = new JLabel(new ImageIcon("files/hobby/lifestyles/image.jpg")); //250*167
		c28.gridwidth=2;
		c28.gridx = 0;
		c28.gridy = 0;
		p28.add(image28, c28);
		
		JLabel text28 = new JLabel(count[28][0]);
		text28.setFont(fonttext);
		c28.gridx = 0;
		c28.gridy = 1;
		c28.insets = new Insets(5, 0, 5, 0);
		p28.add(text28, c28);
			
		JLabel count28 = new JLabel(count[28][1]);
		count28.setFont(fontcount);
		c28.insets = new Insets(0, 0, 0, 0);
		c28.gridx = 0;
		c28.gridy = 2;
		p28.add(count28, c28);
			
		JButton b28 = new JButton("Добавить набор");
		b28.setBackground(color2);
		b28.setForeground(Color.white);
		b28.setFont(fontbtn);
		b28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b28.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[28]!=true){
						q.addSetOfSet(28);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[28][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c28.insets = new Insets(2, 0, 5, 0);
		c28.gridx = 0;
		c28.gridy = 3;
		p28.add(b28, c28);
		
		JButton bt28 = new JButton(new ImageIcon("res/images/8.png"));
		bt28.setVisible(visible[28]);
		bt28.setToolTipText("На изучении");
		bt28.setContentAreaFilled(false);
		bt28.setBorder(null);
		bt28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c28.anchor = GridBagConstraints.LAST_LINE_END;
		c28.gridx = 1;
		c28.gridy = 3;
		p28.add(bt28, c28);	
		
		gl3.add(box31);//Инормационный блок
		gl3.add(box32);//Блок 1
		box32.add(p27);
		box32.add(Box.createHorizontalStrut(50));
		box32.add(p28);
		box32.add(Box.createHorizontalStrut(600));
				
		flp.add(gl3);
	}
	
	// Набор РАЗНОЕ (2 подпанели)
	private void set4(){

		//Информация к набору
		Box box41 = Box.createHorizontalBox();
		box41.setPreferredSize(new Dimension(973, 50));
				
		Box	box42 = Box.createHorizontalBox();
		box42.setPreferredSize(new Dimension(1150, 250));
						
		JButton btn4 = new JButton(set[4]);
		btn4.setFont(fonttext);
		btn4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		box41.add(btn4);
		box41.add(Box.createHorizontalGlue());
									
		//Главная панель набора gl4
		gl4 = new JPanel();
		FlowLayout glp4 = new FlowLayout();
		gl4.setPreferredSize(new Dimension(1165, 315));//315
		gl4.setLayout(glp4);
		gl4.setOpaque(false);
					
		// ------------------------------------------------------------Панель Транспорт и вождение 
		JPanel p29 = new JPanel();
		GridBagLayout gbl29 = new GridBagLayout();
		p29.setLayout(gbl29);
		p29.setPreferredSize(new Dimension(250, 250));
		p29.setBackground(Color.white);
		p29.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(29);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р29
		GridBagConstraints c29 = new GridBagConstraints();
					
		JLabel image29 = new JLabel(new ImageIcon("files/miscellanea/transport and driving/image.jpg")); //250*167
		c29.gridwidth=2;
		c29.gridx = 0;
		c29.gridy = 0;
		p29.add(image29, c29);
		
		JLabel text29 = new JLabel(count[29][0]);
		text29.setFont(fonttext);
		c29.gridx = 0;
		c29.gridy = 1;
		c29.insets = new Insets(5, 0, 5, 0);
		p29.add(text29, c29);
			
		JLabel count29 = new JLabel(count[29][1]);
		count29.setFont(fontcount);
		c29.insets = new Insets(0, 0, 0, 0);
		c29.gridx = 0;
		c29.gridy = 2;
		p29.add(count29, c29);
			
		JButton b29 = new JButton("Добавить набор");
		b29.setBackground(color2);
		b29.setForeground(Color.white);
		b29.setFont(fontbtn);
		b29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b29.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[29]!=true){
						q.addSetOfSet(29);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[29][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c29.insets = new Insets(2, 0, 5, 0);
		c29.gridx = 0;
		c29.gridy = 3;
		p29.add(b29, c29);
		
		JButton bt29 = new JButton(new ImageIcon("res/images/8.png"));
		bt29.setVisible(visible[29]);
		bt29.setToolTipText("На изучении");
		bt29.setContentAreaFilled(false);
		bt29.setBorder(null);
		bt29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c29.anchor = GridBagConstraints.LAST_LINE_END;
		c29.gridx = 1;
		c29.gridy = 3;
		p29.add(bt29, c29);
		
		//------------------------------------------------------Панель Ругательства 16+
		JPanel p30 = new JPanel();
		GridBagLayout gbl30 = new GridBagLayout();
		p30.setLayout(gbl30);
		p30.setPreferredSize(new Dimension(250, 250));
		p30.setBackground(Color.white);
		p30.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(30);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р30
		GridBagConstraints c30 = new GridBagConstraints();
					
		JLabel image30 = new JLabel(new ImageIcon("files/miscellanea/swearing/image.jpg")); //250*167
		c30.gridwidth=2;
		c30.gridx = 0;
		c30.gridy = 0;
		p30.add(image30, c30);
		
		JLabel text30 = new JLabel(count[30][0]);
		text30.setFont(fonttext);
		c30.gridx = 0;
		c30.gridy = 1;
		c30.insets = new Insets(5, 0, 5, 0);
		p30.add(text30, c30);
			
		JLabel count30 = new JLabel(count[30][1]);
		count30.setFont(fontcount);
		c30.insets = new Insets(0, 0, 0, 0);
		c30.gridx = 0;
		c30.gridy = 2;
		p30.add(count30, c30);
			
		JButton b30 = new JButton("Добавить набор");
		b30.setBackground(color2);
		b30.setForeground(Color.white);
		b30.setFont(fontbtn);
		b30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b30.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[30]!=true){
						q.addSetOfSet(30);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[30][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c30.insets = new Insets(2, 0, 5, 0);
		c30.gridx = 0;
		c30.gridy = 3;
		p30.add(b30, c30);
		
		JButton bt30 = new JButton(new ImageIcon("res/images/8.png"));
		bt30.setVisible(visible[30]);
		bt30.setToolTipText("На изучении");
		bt30.setContentAreaFilled(false);
		bt30.setBorder(null);
		bt30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c30.anchor = GridBagConstraints.LAST_LINE_END;
		c30.gridx = 1;
		c30.gridy = 3;
		p30.add(bt30, c30);	
		
		gl4.add(box41);//Инормационный блок
		gl4.add(box42);//Блок 1
		box42.add(p29);
		box42.add(Box.createHorizontalStrut(50));
		box42.add(p30);
		box42.add(Box.createHorizontalStrut(600));
				
		flp.add(gl4);
	}
	
	//Набор Части речи, фразовые глаголы и идиомы (17 подпанелей)
	private void set5(){

		//Информация к набору
		Box box51 = Box.createHorizontalBox();
		box51.setPreferredSize(new Dimension(973, 50));
				
		Box	box52 = Box.createHorizontalBox();
		box52.setPreferredSize(new Dimension(1150, 250));
							
		box53 = Box.createHorizontalBox();// Отступ между боксами
		box53.setPreferredSize(new Dimension(1150, 40));
		box53.setVisible(false);
							
		box54 = Box.createHorizontalBox();
		box54.setPreferredSize(new Dimension(1150, 250));
		box54.setVisible(false);
		
		box55 = Box.createHorizontalBox();// Отступ между боксами
		box55.setPreferredSize(new Dimension(1150, 40));
		box55.setVisible(false);
							
		box56 = Box.createHorizontalBox();
		box56.setPreferredSize(new Dimension(1150, 250));
		box56.setVisible(false);
		
		box57 = Box.createHorizontalBox();// Отступ между боксами
		box57.setPreferredSize(new Dimension(1150, 40));
		box57.setVisible(false);
							
		box58 = Box.createHorizontalBox();
		box58.setPreferredSize(new Dimension(1150, 250));
		box58.setVisible(false);
		
		box59 = Box.createHorizontalBox();// Отступ между боксами
		box59.setPreferredSize(new Dimension(1150, 40));
		box59.setVisible(false);
							
		box60 = Box.createHorizontalBox();
		box60.setPreferredSize(new Dimension(1150, 250));
		box60.setVisible(false);
		
		JButton btn5 = new JButton(set[5]);
		btn5.setFont(fonttext);
		btn5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn5.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			actionClickBtn5();
		}
		});
		box51.add(btn5);
		box51.add(Box.createHorizontalGlue());
					
		JButton btn51 = new JButton("еще");
		btn51.setFont(fontbtn);
		btn51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn51.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			actionClickBtn5();
		}
		});
		box51.add(btn51);
					
		//Главная панель набора gl5
		gl5 = new JPanel();
		FlowLayout glp5 = new FlowLayout();
		gl5.setPreferredSize(new Dimension(1165, 315));//315
		gl5.setLayout(glp5);
		gl5.setOpaque(false);
					
		// ------------------------------------------------------------Панель Глаголы состояния
		JPanel p31 = new JPanel();
		GridBagLayout gbl31 = new GridBagLayout();
		p31.setLayout(gbl31);
		p31.setPreferredSize(new Dimension(250, 250));
		p31.setBackground(Color.white);
		p31.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(31);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р31
		GridBagConstraints c31 = new GridBagConstraints();
					
		JLabel image31 = new JLabel(new ImageIcon("files/parts of speech/state verbs/image.jpg")); //250*167
		c31.gridwidth=2;
		c31.gridx = 0;
		c31.gridy = 0;
		p31.add(image31, c31);
		
		JLabel text31 = new JLabel(count[31][0]);
		text31.setFont(fonttext);
		c31.gridx = 0;
		c31.gridy = 1;
		c31.insets = new Insets(5, 0, 5, 0);
		p31.add(text31, c31);
			
		JLabel count31 = new JLabel(count[31][1]);
		count31.setFont(fontcount);
		c31.insets = new Insets(0, 0, 0, 0);
		c31.gridx = 0;
		c31.gridy = 2;
		p31.add(count31, c31);
			
		JButton b31 = new JButton("Добавить набор");
		b31.setBackground(color2);
		b31.setForeground(Color.white);
		b31.setFont(fontbtn);
		b31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b31.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[31]!=true){
						q.addSetOfSet(31);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[31][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c31.insets = new Insets(2, 0, 5, 0);
		c31.gridx = 0;
		c31.gridy = 3;
		p31.add(b31, c31);
		
		JButton bt31 = new JButton(new ImageIcon("res/images/8.png"));
		bt31.setVisible(visible[31]);
		bt31.setToolTipText("На изучении");
		bt31.setContentAreaFilled(false);
		bt31.setBorder(null);
		bt31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c31.anchor = GridBagConstraints.LAST_LINE_END;
		c31.gridx = 1;
		c31.gridy = 3;
		p31.add(bt31, c31);
		
		//------------------------------------------------------Панель Предлоги (средний уровень)
		JPanel p32 = new JPanel();
		GridBagLayout gbl32 = new GridBagLayout();
		p32.setLayout(gbl32);
		p32.setPreferredSize(new Dimension(250, 250));
		p32.setBackground(Color.white);
		p32.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(32);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р9
		GridBagConstraints c32 = new GridBagConstraints();
					
		JLabel image32 = new JLabel(new ImageIcon("files/parts of speech/prepositions middle/image.jpg")); //250*167
		c32.gridwidth=2;
		c32.gridx = 0;
		c32.gridy = 0;
		p32.add(image32, c32);
		
		JLabel text32 = new JLabel(count[32][0]);
		text32.setFont(fonttext);
		c32.gridx = 0;
		c32.gridy = 1;
		c32.insets = new Insets(5, 0, 5, 0);
		p32.add(text32, c32);
			
		JLabel count32 = new JLabel(count[32][1]);
		count32.setFont(fontcount);
		c32.insets = new Insets(0, 0, 0, 0);
		c32.gridx = 0;
		c32.gridy = 2;
		p32.add(count32, c32);
			
		JButton b32 = new JButton("Добавить набор");
		b32.setBackground(color2);
		b32.setForeground(Color.white);
		b32.setFont(fontbtn);
		b32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b32.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[32]!=true){
						q.addSetOfSet(32);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[32][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c32.insets = new Insets(2, 0, 5, 0);
		c32.gridx = 0;
		c32.gridy = 3;
		p32.add(b32, c32);
		
		JButton bt32 = new JButton(new ImageIcon("res/images/8.png"));
		bt32.setVisible(visible[32]);
		bt32.setToolTipText("На изучении");
		bt32.setContentAreaFilled(false);
		bt32.setBorder(null);
		bt32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c32.anchor = GridBagConstraints.LAST_LINE_END;
		c32.gridx = 1;
		c32.gridy = 3;
		p32.add(bt32, c32);
			
		//---------------------------------------------------------Панель Ложные друзья переводчика
		JPanel p33 = new JPanel();
		GridBagLayout gbl33 = new GridBagLayout();
		p33.setLayout(gbl33);
		p33.setPreferredSize(new Dimension(250, 250));
		p33.setBackground(Color.white);
		p33.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(33);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р33
		GridBagConstraints c33 = new GridBagConstraints();
					
		JLabel image33 = new JLabel(new ImageIcon("files/parts of speech/false friends/image.jpg")); //250*167
		c33.gridwidth=2;
		c33.gridx = 0;
		c33.gridy = 0;
		p33.add(image33, c33);
		
		JLabel text33 = new JLabel(count[33][0]);
		text33.setFont(fonttext);
		c33.gridx = 0;
		c33.gridy = 1;
		c33.insets = new Insets(5, 0, 5, 0);
		p33.add(text33, c33);
			
		JLabel count33 = new JLabel(count[33][1]);
		count33.setFont(fontcount);
		c33.insets = new Insets(0, 0, 0, 0);
		c33.gridx = 0;
		c33.gridy = 2;
		p33.add(count33, c33);
			
		JButton b33 = new JButton("Добавить набор");
		b33.setBackground(color2);
		b33.setForeground(Color.white);
		b33.setFont(fontbtn);
		b33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b33.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[33]!=true){
						q.addSetOfSet(33);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[33][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c33.insets = new Insets(2, 0, 5, 0);
		c33.gridx = 0;
		c33.gridy = 3;
		p33.add(b33, c33);
		
		JButton bt33 = new JButton(new ImageIcon("res/images/8.png"));
		bt33.setVisible(visible[33]);
		bt33.setToolTipText("На изучении");
		bt33.setContentAreaFilled(false);
		bt33.setBorder(null);
		bt33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c33.anchor = GridBagConstraints.LAST_LINE_END;
		c33.gridx = 1;
		c33.gridy = 3;
		p33.add(bt33, c33);	
			
		//----------------------------------------------------------Панель Идиомы для экспертов
		JPanel p34 = new JPanel();
		GridBagLayout gbl34 = new GridBagLayout();
		p34.setLayout(gbl34);
		p34.setPreferredSize(new Dimension(250, 250));
		p34.setBackground(Color.white);
		p34.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(34);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р34
		GridBagConstraints c34 = new GridBagConstraints();
					
		JLabel image34 = new JLabel(new ImageIcon("files/parts of speech/idioms for expert/image.jpg")); //250*167
		c34.gridwidth=2;
		c34.gridx = 0;
		c34.gridy = 0;
		p34.add(image34, c34);
		
		JLabel text34 = new JLabel(count[34][0]);
		text34.setFont(fonttext);
		c34.gridx = 0;
		c34.gridy = 1;
		c34.insets = new Insets(5, 0, 5, 0);
		p34.add(text34, c34);
			
		JLabel count34 = new JLabel(count[34][1]);
		count34.setFont(fontcount);
		c34.insets = new Insets(0, 0, 0, 0);
		c34.gridx = 0;
		c34.gridy = 2;
		p34.add(count34, c34);
			
		JButton b34 = new JButton("Добавить набор");
		b34.setBackground(color2);
		b34.setForeground(Color.white);
		b34.setFont(fontbtn);
		b34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b34.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[34]!=true){
						q.addSetOfSet(34);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[34][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c34.insets = new Insets(2, 0, 5, 0);
		c34.gridx = 0;
		c34.gridy = 3;
		p34.add(b34, c34);
		
		JButton bt34 = new JButton(new ImageIcon("res/images/8.png"));
		bt34.setVisible(visible[34]);
		bt34.setToolTipText("На изучении");
		bt34.setContentAreaFilled(false);
		bt34.setBorder(null);
		bt34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c34.anchor = GridBagConstraints.LAST_LINE_END;
		c34.gridx = 1;
		c34.gridy = 3;
		p34.add(bt34, c34);
		
		//----------------------------------------------------Панель Идиомы для продвинутых
		JPanel p35 = new JPanel();
		GridBagLayout gbl35 = new GridBagLayout();
		p35.setLayout(gbl35);
		p35.setPreferredSize(new Dimension(250, 250));
		p35.setBackground(Color.white);
		p35.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(35);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р35
		GridBagConstraints c35 = new GridBagConstraints();
					
		JLabel image35 = new JLabel(new ImageIcon("files/parts of speech/idioms for advanced/image.jpg")); //250*167
		c35.gridwidth=2;
		c35.gridx = 0;
		c35.gridy = 0;
		p35.add(image35, c35);
		
		JLabel text35 = new JLabel(count[35][0]);
		text35.setFont(fonttext);
		c35.gridx = 0;
		c35.gridy = 1;
		c35.insets = new Insets(5, 0, 5, 0);
		p35.add(text35, c35);
			
		JLabel count35 = new JLabel(count[35][1]);
		count35.setFont(fontcount);
		c35.insets = new Insets(0, 0, 0, 0);
		c35.gridx = 0;
		c35.gridy = 2;
		p35.add(count35, c35);
			
		JButton b35 = new JButton("Добавить набор");
		b35.setBackground(color2);
		b35.setForeground(Color.white);
		b35.setFont(fontbtn);
		b35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b35.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[35]!=true){
						q.addSetOfSet(35);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[35][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c35.insets = new Insets(2, 0, 5, 0);
		c35.gridx = 0;
		c35.gridy = 3;
		p35.add(b35, c35);
		
		JButton bt35 = new JButton(new ImageIcon("res/images/8.png"));
		bt35.setVisible(visible[35]);
		bt35.setToolTipText("На изучении");
		bt35.setContentAreaFilled(false);
		bt35.setBorder(null);
		bt35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c35.anchor = GridBagConstraints.LAST_LINE_END;
		c35.gridx = 1;
		c35.gridy = 3;
		p35.add(bt35, c35);
			
		//--------------------------------------------------------------Панель Идиомы
		JPanel p36 = new JPanel();
		GridBagLayout gbl36 = new GridBagLayout();
		p36.setLayout(gbl36);
		p36.setPreferredSize(new Dimension(250, 250));
		p36.setBackground(Color.white);
		p36.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(36);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р36
		GridBagConstraints c36 = new GridBagConstraints();
					
		JLabel image36 = new JLabel(new ImageIcon("files/parts of speech/idioms/image.jpg")); //250*167
		c36.gridwidth=2;
		c36.gridx = 0;
		c36.gridy = 0;
		p36.add(image36, c36);
		
		JLabel text36 = new JLabel(count[36][0]);
		text36.setFont(fonttext);
		c36.gridx = 0;
		c36.gridy = 1;
		c36.insets = new Insets(5, 0, 5, 0);
		p36.add(text36, c36);
			
		JLabel count36 = new JLabel(count[36][1]);
		count36.setFont(fontcount);
		c36.insets = new Insets(0, 0, 0, 0);
		c36.gridx = 0;
		c36.gridy = 2;
		p36.add(count36, c36);
			
		JButton b36 = new JButton("Добавить набор");
		b36.setBackground(color2);
		b36.setForeground(Color.white);
		b36.setFont(fontbtn);
		b36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b36.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[36]!=true){
						q.addSetOfSet(36);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[36][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c36.insets = new Insets(2, 0, 5, 0);
		c36.gridx = 0;
		c36.gridy = 3;
		p36.add(b36, c36);
		
		JButton bt36 = new JButton(new ImageIcon("res/images/8.png"));
		bt36.setVisible(visible[36]);
		bt36.setToolTipText("На изучении");
		bt36.setContentAreaFilled(false);
		bt36.setBorder(null);
		bt36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c36.anchor = GridBagConstraints.LAST_LINE_END;
		c36.gridx = 1;
		c36.gridy = 3;
		p36.add(bt36, c36);	
		
		//------------------------------------------------------------------Панель Неправильные глаголы для экспертов
		JPanel p37 = new JPanel();
		GridBagLayout gbl37 = new GridBagLayout();
		p37.setLayout(gbl37);
		p37.setPreferredSize(new Dimension(250, 250));
		p37.setBackground(Color.white);
		p37.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(37);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р37
		GridBagConstraints c37 = new GridBagConstraints();
					
		JLabel image37 = new JLabel(new ImageIcon("files/parts of speech/irregular verbs for experts/image.png")); //250*167
		c37.gridwidth=2;
		c37.gridx = 0;
		c37.gridy = 0;
		p37.add(image37, c37);
		
		JLabel text37 = new JLabel(count[37][0]);
		text37.setFont(fonttext);
		c37.gridx = 0;
		c37.gridy = 1;
		c37.insets = new Insets(5, 0, 5, 0);
		p37.add(text37, c37);
			
		JLabel count37 = new JLabel(count[37][1]);
		count37.setFont(fontcount);
		c37.insets = new Insets(0, 0, 0, 0);
		c37.gridx = 0;
		c37.gridy = 2;
		p37.add(count37, c37);
			
		JButton b37 = new JButton("Добавить набор");
		b37.setBackground(color2);
		b37.setForeground(Color.white);
		b37.setFont(fontbtn);
		b37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b37.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[37]!=true){
						q.addSetOfSet(37);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[37][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c37.insets = new Insets(2, 0, 5, 0);
		c37.gridx = 0;
		c37.gridy = 3;
		p37.add(b37, c37);
		
		JButton bt37 = new JButton(new ImageIcon("res/images/8.png"));
		bt37.setVisible(visible[37]);
		bt37.setToolTipText("На изучении");
		bt37.setContentAreaFilled(false);
		bt37.setBorder(null);
		bt37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c37.anchor = GridBagConstraints.LAST_LINE_END;
		c37.gridx = 1;
		c37.gridy = 3;
		p37.add(bt37, c37);
		
		//----------------------------------------------------------------Панель Неправильные глаголы (средний уровень)
		JPanel p38 = new JPanel();
		GridBagLayout gbl38 = new GridBagLayout();
		p38.setLayout(gbl38);
		p38.setPreferredSize(new Dimension(250, 250));
		p38.setBackground(Color.white);
		p38.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(38);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р38
		GridBagConstraints c38 = new GridBagConstraints();
					
		JLabel image38 = new JLabel(new ImageIcon("files/parts of speech/irregular verbs middle/image.jpg")); //250*387
		c38.gridwidth=2;
		c38.gridx = 0;
		c38.gridy = 0;
		p38.add(image38, c38);
		
		JLabel text38 = new JLabel(count[38][0]);
		text38.setFont(fonttext);
		c38.gridx = 0;
		c38.gridy = 1;
		c38.insets = new Insets(5, 0, 5, 0);
		p38.add(text38, c38);
			
		JLabel count38 = new JLabel(count[38][1]);
		count38.setFont(fontcount);
		c38.insets = new Insets(0, 0, 0, 0);
		c38.gridx = 0;
		c38.gridy = 2;
		p38.add(count38, c38);
			
		JButton b38 = new JButton("Добавить набор");
		b38.setBackground(color2);
		b38.setForeground(Color.white);
		b38.setFont(fontbtn);
		b38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b38.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[38]!=true){
						q.addSetOfSet(38);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[38][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c38.insets = new Insets(2, 0, 5, 0);
		c38.gridx = 0;
		c38.gridy = 3;
		p38.add(b38, c38);
		
		JButton bt38 = new JButton(new ImageIcon("res/images/8.png"));
		bt38.setVisible(visible[38]);
		bt38.setToolTipText("На изучении");
		bt38.setContentAreaFilled(false);
		bt38.setBorder(null);
		bt38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c38.anchor = GridBagConstraints.LAST_LINE_END;
		c38.gridx = 1;
		c38.gridy = 3;
		p38.add(bt38, c38);
		
		//----------------------------------------------------------------Панель 50 глаголов
		JPanel p39 = new JPanel();
		GridBagLayout gbl39 = new GridBagLayout();
		p39.setLayout(gbl39);
		p39.setPreferredSize(new Dimension(250, 250));
		p39.setBackground(Color.white);
		p39.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(39);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р39
		GridBagConstraints c39 = new GridBagConstraints();
					
		JLabel image39 = new JLabel(new ImageIcon("files/parts of speech/50 verbs/image.jpg")); //250*397
		c39.gridwidth=2;
		c39.gridx = 0;
		c39.gridy = 0;
		p39.add(image39, c39);
		
		JLabel text39 = new JLabel(count[39][0]);
		text39.setFont(fonttext);
		c39.gridx = 0;
		c39.gridy = 1;
		c39.insets = new Insets(5, 0, 5, 0);
		p39.add(text39, c39);
			
		JLabel count39 = new JLabel(count[39][1]);
		count39.setFont(fontcount);
		c39.insets = new Insets(0, 0, 0, 0);
		c39.gridx = 0;
		c39.gridy = 2;
		p39.add(count39, c39);
			
		JButton b39 = new JButton("Добавить набор");
		b39.setBackground(color2);
		b39.setForeground(Color.white);
		b39.setFont(fontbtn);
		b39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b39.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[39]!=true){
						q.addSetOfSet(39);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[39][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c39.insets = new Insets(2, 0, 5, 0);
		c39.gridx = 0;
		c39.gridy = 3;
		p39.add(b39, c39);
		
		JButton bt39 = new JButton(new ImageIcon("res/images/8.png"));
		bt39.setVisible(visible[39]);
		bt39.setToolTipText("На изучении");
		bt39.setContentAreaFilled(false);
		bt39.setBorder(null);
		bt39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c39.anchor = GridBagConstraints.LAST_LINE_END;
		c39.gridx = 1;
		c39.gridy = 3;
		p39.add(bt39, c39);
		
		//----------------------------------------------------------------Панель Предлоги (набор для начинающих)
		JPanel p40 = new JPanel();
		GridBagLayout gbl40 = new GridBagLayout();
		p40.setLayout(gbl40);
		p40.setPreferredSize(new Dimension(250, 250));
		p40.setBackground(Color.white);
		p40.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(40);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р40
		GridBagConstraints c40 = new GridBagConstraints();
					
		JLabel image40 = new JLabel(new ImageIcon("files/parts of speech/prepositions beginner/image.jpg")); //250*407
		c40.gridwidth=2;
		c40.gridx = 0;
		c40.gridy = 0;
		p40.add(image40, c40);
		
		JLabel text40 = new JLabel(count[40][0]);
		text40.setFont(fonttext);
		c40.gridx = 0;
		c40.gridy = 1;
		c40.insets = new Insets(5, 0, 5, 0);
		p40.add(text40, c40);
			
		JLabel count40 = new JLabel(count[40][1]);
		count40.setFont(fontcount);
		c40.insets = new Insets(0, 0, 0, 0);
		c40.gridx = 0;
		c40.gridy = 2;
		p40.add(count40, c40);
			
		JButton b40 = new JButton("Добавить набор");
		b40.setBackground(color2);
		b40.setForeground(Color.white);
		b40.setFont(fontbtn);
		b40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b40.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[40]!=true){
						q.addSetOfSet(40);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[40][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c40.insets = new Insets(2, 0, 5, 0);
		c40.gridx = 0;
		c40.gridy = 3;
		p40.add(b40, c40);
		
		JButton bt40 = new JButton(new ImageIcon("res/images/8.png"));
		bt40.setVisible(visible[40]);
		bt40.setToolTipText("На изучении");
		bt40.setContentAreaFilled(false);
		bt40.setBorder(null);
		bt40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c40.anchor = GridBagConstraints.LAST_LINE_END;
		c40.gridx = 1;
		c40.gridy = 3;
		p40.add(bt40, c40);
		
		//----------------------------------------------------------------Панель Топ 100 неправильных глаголов
		JPanel p41 = new JPanel();
		GridBagLayout gbl41 = new GridBagLayout();
		p41.setLayout(gbl41);
		p41.setPreferredSize(new Dimension(250, 250));
		p41.setBackground(Color.white);
		p41.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(41);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р41
		GridBagConstraints c41 = new GridBagConstraints();
					
		JLabel image41 = new JLabel(new ImageIcon("files/parts of speech/top 100 irregular verbs/image.png")); //250*417
		c41.gridwidth=2;
		c41.gridx = 0;
		c41.gridy = 0;
		p41.add(image41, c41);
		
		JLabel text41 = new JLabel(count[41][0]);
		text41.setFont(fonttext);
		c41.gridx = 0;
		c41.gridy = 1;
		c41.insets = new Insets(5, 0, 5, 0);
		p41.add(text41, c41);
			
		JLabel count41 = new JLabel(count[41][1]);
		count41.setFont(fontcount);
		c41.insets = new Insets(0, 0, 0, 0);
		c41.gridx = 0;
		c41.gridy = 2;
		p41.add(count41, c41);
			
		JButton b41 = new JButton("Добавить набор");
		b41.setBackground(color2);
		b41.setForeground(Color.white);
		b41.setFont(fontbtn);
		b41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b41.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[41]!=true){
						q.addSetOfSet(41);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[41][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c41.insets = new Insets(2, 0, 5, 0);
		c41.gridx = 0;
		c41.gridy = 3;
		p41.add(b41, c41);
		
		JButton bt41 = new JButton(new ImageIcon("res/images/8.png"));
		bt41.setVisible(visible[41]);
		bt41.setToolTipText("На изучении");
		bt41.setContentAreaFilled(false);
		bt41.setBorder(null);
		bt41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c41.anchor = GridBagConstraints.LAST_LINE_END;
		c41.gridx = 1;
		c41.gridy = 3;
		p41.add(bt41, c41);
		
		//----------------------------------------------------------------Панель Топ 50 неправильных глаголов
			JPanel p42 = new JPanel();
			GridBagLayout gbl42 = new GridBagLayout();
			p42.setLayout(gbl42);
			p42.setPreferredSize(new Dimension(250, 250));
			p42.setBackground(Color.white);
			p42.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(42);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р42
			GridBagConstraints c42 = new GridBagConstraints();
						
			JLabel image42 = new JLabel(new ImageIcon("files/parts of speech/top 50 irregular verbs/image.png")); //250*427
			c42.gridwidth=2;
			c42.gridx = 0;
			c42.gridy = 0;
			p42.add(image42, c42);
			
			JLabel text42 = new JLabel(count[42][0]);
			text42.setFont(fonttext);
			c42.gridx = 0;
			c42.gridy = 1;
			c42.insets = new Insets(5, 0, 5, 0);
			p42.add(text42, c42);
				
			JLabel count42 = new JLabel(count[42][1]);
			count42.setFont(fontcount);
			c42.insets = new Insets(0, 0, 0, 0);
			c42.gridx = 0;
			c42.gridy = 2;
			p42.add(count42, c42);
				
			JButton b42 = new JButton("Добавить набор");
			b42.setBackground(color2);
			b42.setForeground(Color.white);
			b42.setFont(fontbtn);
			b42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b42.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[42]!=true){
							q.addSetOfSet(42);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[42][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c42.insets = new Insets(2, 0, 5, 0);
			c42.gridx = 0;
			c42.gridy = 3;
			p42.add(b42, c42);
			
			JButton bt42 = new JButton(new ImageIcon("res/images/8.png"));
			bt42.setVisible(visible[42]);
			bt42.setToolTipText("На изучении");
			bt42.setContentAreaFilled(false);
			bt42.setBorder(null);
			bt42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c42.anchor = GridBagConstraints.LAST_LINE_END;
			c42.gridx = 1;
			c42.gridy = 3;
			p42.add(bt42, c42);
		
			//----------------------------------------------------------------Панель Топ 100 наречий
			JPanel p43 = new JPanel();
			GridBagLayout gbl43 = new GridBagLayout();
			p43.setLayout(gbl43);
			p43.setPreferredSize(new Dimension(250, 250));
			p43.setBackground(Color.white);
			p43.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(43);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р43
			GridBagConstraints c43 = new GridBagConstraints();
						
			JLabel image43 = new JLabel(new ImageIcon("files/parts of speech/top 100 adverbs/image.jpg")); //250*437
			c43.gridwidth=2;
			c43.gridx = 0;
			c43.gridy = 0;
			p43.add(image43, c43);
			
			JLabel text43 = new JLabel(count[43][0]);
			text43.setFont(fonttext);
			c43.gridx = 0;
			c43.gridy = 1;
			c43.insets = new Insets(5, 0, 5, 0);
			p43.add(text43, c43);
				
			JLabel count43 = new JLabel(count[43][1]);
			count43.setFont(fontcount);
			c43.insets = new Insets(0, 0, 0, 0);
			c43.gridx = 0;
			c43.gridy = 2;
			p43.add(count43, c43);
				
			JButton b43 = new JButton("Добавить набор");
			b43.setBackground(color2);
			b43.setForeground(Color.white);
			b43.setFont(fontbtn);
			b43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b43.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[43]!=true){
							q.addSetOfSet(43);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[43][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c43.insets = new Insets(2, 0, 5, 0);
			c43.gridx = 0;
			c43.gridy = 3;
			p43.add(b43, c43);
			
			JButton bt43 = new JButton(new ImageIcon("res/images/8.png"));
			bt43.setVisible(visible[43]);
			bt43.setToolTipText("На изучении");
			bt43.setContentAreaFilled(false);
			bt43.setBorder(null);
			bt43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c43.anchor = GridBagConstraints.LAST_LINE_END;
			c43.gridx = 1;
			c43.gridy = 3;
			p43.add(bt43, c43);	
			
				
			//----------------------------------------------------------------Панель Топ 100 глаголов
			JPanel p44 = new JPanel();
			GridBagLayout gbl44 = new GridBagLayout();
			p44.setLayout(gbl44);
			p44.setPreferredSize(new Dimension(250, 250));
			p44.setBackground(Color.white);
			p44.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(44);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р44
			GridBagConstraints c44 = new GridBagConstraints();
						
			JLabel image44 = new JLabel(new ImageIcon("files/parts of speech/top 100 verbs/image.jpg")); //250*447
			c44.gridwidth=2;
			c44.gridx = 0;
			c44.gridy = 0;
			p44.add(image44, c44);
			
			JLabel text44 = new JLabel(count[44][0]);
			text44.setFont(fonttext);
			c44.gridx = 0;
			c44.gridy = 1;
			c44.insets = new Insets(5, 0, 5, 0);
			p44.add(text44, c44);
				
			JLabel count44 = new JLabel(count[44][1]);
			count44.setFont(fontcount);
			c44.insets = new Insets(0, 0, 0, 0);
			c44.gridx = 0;
			c44.gridy = 2;
			p44.add(count44, c44);
				
			JButton b44 = new JButton("Добавить набор");
			b44.setBackground(color2);
			b44.setForeground(Color.white);
			b44.setFont(fontbtn);
			b44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b44.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[44]!=true){
							q.addSetOfSet(44);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[44][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c44.insets = new Insets(2, 0, 5, 0);
			c44.gridx = 0;
			c44.gridy = 3;
			p44.add(b44, c44);
			
			JButton bt44 = new JButton(new ImageIcon("res/images/8.png"));
			bt44.setVisible(visible[44]);
			bt44.setToolTipText("На изучении");
			bt44.setContentAreaFilled(false);
			bt44.setBorder(null);
			bt44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c44.anchor = GridBagConstraints.LAST_LINE_END;
			c44.gridx = 1;
			c44.gridy = 3;
			p44.add(bt44, c44);	
			
			//----------------------------------------------------------------Панель ТОП 100 прилагательных
			JPanel p45 = new JPanel();
			GridBagLayout gbl45 = new GridBagLayout();
			p45.setLayout(gbl45);
			p45.setPreferredSize(new Dimension(250, 250));
			p45.setBackground(Color.white);
			p45.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(45);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р45
			GridBagConstraints c45 = new GridBagConstraints();
						
			JLabel image45 = new JLabel(new ImageIcon("files/parts of speech/top 100 adjectives/image.jpeg")); //250*457
			c45.gridwidth=2;
			c45.gridx = 0;
			c45.gridy = 0;
			p45.add(image45, c45);
			
			JLabel text45 = new JLabel(count[45][0]);
			text45.setFont(fonttext);
			c45.gridx = 0;
			c45.gridy = 1;
			c45.insets = new Insets(5, 0, 5, 0);
			p45.add(text45, c45);
				
			JLabel count45 = new JLabel(count[45][1]);
			count45.setFont(fontcount);
			c45.insets = new Insets(0, 0, 0, 0);
			c45.gridx = 0;
			c45.gridy = 2;
			p45.add(count45, c45);
				
			JButton b45 = new JButton("Добавить набор");
			b45.setBackground(color2);
			b45.setForeground(Color.white);
			b45.setFont(fontbtn);
			b45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b45.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[45]!=true){
							q.addSetOfSet(45);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[45][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c45.insets = new Insets(2, 0, 5, 0);
			c45.gridx = 0;
			c45.gridy = 3;
			p45.add(b45, c45);
			
			JButton bt45 = new JButton(new ImageIcon("res/images/8.png"));
			bt45.setVisible(visible[45]);
			bt45.setToolTipText("На изучении");
			bt45.setContentAreaFilled(false);
			bt45.setBorder(null);
			bt45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c45.anchor = GridBagConstraints.LAST_LINE_END;
			c45.gridx = 1;
			c45.gridy = 3;
			p45.add(bt45, c45);
		
			//----------------------------------------------------------------Панель TOP 170 фразовых глаголов
			JPanel p46 = new JPanel();
			GridBagLayout gbl46 = new GridBagLayout();
			p46.setLayout(gbl46);
			p46.setPreferredSize(new Dimension(250, 250));
			p46.setBackground(Color.white);
			p46.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(46);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р46
			GridBagConstraints c46 = new GridBagConstraints();
						
			JLabel image46 = new JLabel(new ImageIcon("files/parts of speech/top 170 phrasal verbs/image.jpg")); //250*467
			c46.gridwidth=2;
			c46.gridx = 0;
			c46.gridy = 0;
			p46.add(image46, c46);
			
			JLabel text46 = new JLabel(count[46][0]);
			text46.setFont(fonttext);
			c46.gridx = 0;
			c46.gridy = 1;
			c46.insets = new Insets(5, 0, 5, 0);
			p46.add(text46, c46);
				
			JLabel count46 = new JLabel(count[46][1]);
			count46.setFont(fontcount);
			c46.insets = new Insets(0, 0, 0, 0);
			c46.gridx = 0;
			c46.gridy = 2;
			p46.add(count46, c46);
				
			JButton b46 = new JButton("Добавить набор");
			b46.setBackground(color2);
			b46.setForeground(Color.white);
			b46.setFont(fontbtn);
			b46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b46.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[46]!=true){
							q.addSetOfSet(46);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[46][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c46.insets = new Insets(2, 0, 5, 0);
			c46.gridx = 0;
			c46.gridy = 3;
			p46.add(b46, c46);
			
			JButton bt46 = new JButton(new ImageIcon("res/images/8.png"));
			bt46.setVisible(visible[46]);
			bt46.setToolTipText("На изучении");
			bt46.setContentAreaFilled(false);
			bt46.setBorder(null);
			bt46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c46.anchor = GridBagConstraints.LAST_LINE_END;
			c46.gridx = 1;
			c46.gridy = 3;
			p46.add(bt46, c46);	
		
			//----------------------------------------------------------------Панель Неправильные глаголы
			JPanel p47 = new JPanel();
			GridBagLayout gbl47 = new GridBagLayout();
			p47.setLayout(gbl47);
			p47.setPreferredSize(new Dimension(470, 470));
			p47.setBackground(Color.white);
			p47.addMouseListener(new MouseListener() {
				
				public void mouseClicked(MouseEvent e) {
					showSetWordsPanel(47);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
				}
				public void mouseEntered(MouseEvent e) {
				}});
			p47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
			// Добавляем елементы на панель р47
			GridBagConstraints c47 = new GridBagConstraints();
						
			JLabel image47 = new JLabel(new ImageIcon("files/parts of speech/irregular verbs/image.jpg")); //470*477
			c47.gridwidth=2;
			c47.gridx = 0;
			c47.gridy = 0;
			p47.add(image47, c47);
			
			JLabel text47 = new JLabel(count[47][0]);
			text47.setFont(fonttext);
			c47.gridx = 0;
			c47.gridy = 1;
			c47.insets = new Insets(5, 0, 5, 0);
			p47.add(text47, c47);
				
			JLabel count47 = new JLabel(count[47][1]);
			count47.setFont(fontcount);
			c47.insets = new Insets(0, 0, 0, 0);
			c47.gridx = 0;
			c47.gridy = 2;
			p47.add(count47, c47);
				
			JButton b47 = new JButton("Добавить набор");
			b47.setBackground(color2);
			b47.setForeground(Color.white);
			b47.setFont(fontbtn);
			b47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			b47.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (visible[47]!=true){
							q.addSetOfSet(47);
							repaintWindow();
						}else{
							JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[47][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				});
			c47.insets = new Insets(2, 0, 5, 0);
			c47.gridx = 0;
			c47.gridy = 3;
			p47.add(b47, c47);
			
			JButton bt47 = new JButton(new ImageIcon("res/images/8.png"));
			bt47.setVisible(visible[47]);
			bt47.setToolTipText("На изучении");
			bt47.setContentAreaFilled(false);
			bt47.setBorder(null);
			bt47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			c47.anchor = GridBagConstraints.LAST_LINE_END;
			c47.gridx = 1;
			c47.gridy = 3;
			p47.add(bt47, c47);	
						
			
		gl5.add(box51);//Инормационный блок
		gl5.add(box52);//Блок 1
		box52.add(p31);
		box52.add(Box.createHorizontalStrut(50));
		box52.add(p32);
		box52.add(Box.createHorizontalStrut(50));
		box52.add(p33);
		box52.add(Box.createHorizontalStrut(50));
		box52.add(p34);
					
		gl5.add(box53);//Отсуп между блоками
					
		gl5.add(box54);//Блок 2
		box54.add(p35);
		box54.add(Box.createHorizontalStrut(50));
		box54.add(p36);
		box54.add(Box.createHorizontalStrut(50));
		box54.add(p37);
		box54.add(Box.createHorizontalStrut(50));
		box54.add(p38);
		
		gl5.add(box55);//Отсуп между блоками
		
		gl5.add(box56);//Блок 3
		box56.add(p39);
		box56.add(Box.createHorizontalStrut(50));
		box56.add(p40);
		box56.add(Box.createHorizontalStrut(50));
		box56.add(p41);
		box56.add(Box.createHorizontalStrut(50));
		box56.add(p42);
		
		gl5.add(box57);//Отсуп между блоками
		
		gl5.add(box58);//Блок 4
		box58.add(p43);
		box58.add(Box.createHorizontalStrut(50));
		box58.add(p44);
		box58.add(Box.createHorizontalStrut(50));
		box58.add(p45);
		box58.add(Box.createHorizontalStrut(50));
		box58.add(p46);
		
		gl5.add(box59);//Отсуп между блоками
		
		gl5.add(box60);//Блок 5
		box60.add(p47);
		box60.add(Box.createHorizontalStrut(900));
		
		flp.add(gl5);
	}
	//Разворачивания сворачивания набора
	private void actionClickBtn5() {

			if(click==0){
				click=1;
				box53.setVisible(true);
				box54.setVisible(true);
				box55.setVisible(true);
				box56.setVisible(true);
				box57.setVisible(true);
				box58.setVisible(true);
				box59.setVisible(true);
				box60.setVisible(true);
				gl5.setPreferredSize(new Dimension(1165, 1520));
			}else{
				click=0;
				gl5.setPreferredSize(new Dimension(1165, 315));
				box53.setVisible(false);
				box54.setVisible(false);
				box55.setVisible(false);
				box56.setVisible(false);
				box57.setVisible(false);
				box58.setVisible(false);
				box59.setVisible(false);
				box60.setVisible(false);
			}
		}
	
	// Набор Литература и Кино(5 подпанелей)
	public void set6(){

		//Информация к набору
		Box box61 = Box.createHorizontalBox();
		box61.setPreferredSize(new Dimension(973, 50));
				
		Box	box62 = Box.createHorizontalBox();
		box62.setPreferredSize(new Dimension(1150, 250));
		
		box63 = Box.createHorizontalBox();// Отступ между боксами
		box63.setPreferredSize(new Dimension(1150, 40));
		box63.setVisible(false);
				
		box64 = Box.createHorizontalBox();
		box64.setPreferredSize(new Dimension(1150, 250));
		box64.setVisible(false);
		
		JButton btn6 = new JButton(set[6]);
		btn6.setFont(fonttext);
		btn6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn6.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			actionClickBtn6();
		}
		});
		box61.add(btn6);
		box61.add(Box.createHorizontalGlue());
					
		JButton btn61 = new JButton("еще");
		btn61.setFont(fontbtn);
		btn61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn61.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			actionClickBtn6();
		}
		});
		box61.add(btn61);
		
		//Главная панель набора gl6
		gl6 = new JPanel();
		FlowLayout glp6 = new FlowLayout();
		gl6.setPreferredSize(new Dimension(1165, 315));//315
		gl6.setLayout(glp6);
		gl6.setOpaque(false);
		
		//------------------------------------------------------Панель Friends
		JPanel p48 = new JPanel();
		GridBagLayout gbl48 = new GridBagLayout();
		p48.setLayout(gbl48);
		p48.setPreferredSize(new Dimension(250, 250));
		p48.setBackground(Color.white);
		p48.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(48);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р9
		GridBagConstraints c48 = new GridBagConstraints();
					
		JLabel image48 = new JLabel(new ImageIcon("files/literature and film/friends/image.png")); //250*167
		c48.gridwidth=2;
		c48.gridx = 0;
		c48.gridy = 0;
		p48.add(image48, c48);
		
		JLabel text48 = new JLabel(count[48][0]);
		text48.setFont(fonttext);
		c48.gridx = 0;
		c48.gridy = 1;
		c48.insets = new Insets(5, 0, 5, 0);
		p48.add(text48, c48);
			
		JLabel count48 = new JLabel(count[48][1]);
		count48.setFont(fontcount);
		c48.insets = new Insets(0, 0, 0, 0);
		c48.gridx = 0;
		c48.gridy = 2;
		p48.add(count48, c48);
			
		JButton b48 = new JButton("Добавить набор");
		b48.setBackground(color2);
		b48.setForeground(Color.white);
		b48.setFont(fontbtn);
		b48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b48.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[48]!=true){
						q.addSetOfSet(48);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[48][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c48.insets = new Insets(2, 0, 5, 0);
		c48.gridx = 0;
		c48.gridy = 3;
		p48.add(b48, c48);
		
		JButton bt48 = new JButton(new ImageIcon("res/images/8.png"));
		bt48.setVisible(visible[48]);
		bt48.setToolTipText("На изучении");
		bt48.setContentAreaFilled(false);
		bt48.setBorder(null);
		bt48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c48.anchor = GridBagConstraints.LAST_LINE_END;
		c48.gridx = 1;
		c48.gridy = 3;
		p48.add(bt48, c48);
			
		//---------------------------------------------------------Панель Закон и порядок
		JPanel p49 = new JPanel();
		GridBagLayout gbl49 = new GridBagLayout();
		p49.setLayout(gbl49);
		p49.setPreferredSize(new Dimension(250, 250));
		p49.setBackground(Color.white);
		p49.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(49);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р49
		GridBagConstraints c49 = new GridBagConstraints();
					
		JLabel image49 = new JLabel(new ImageIcon("files/literature and film/law and Order/image.jpg")); //250*167
		c49.gridwidth=2;
		c49.gridx = 0;
		c49.gridy = 0;
		p49.add(image49, c49);
		
		JLabel text49 = new JLabel(count[49][0]);
		text49.setFont(fonttext);
		c49.gridx = 0;
		c49.gridy = 1;
		c49.insets = new Insets(5, 0, 5, 0);
		p49.add(text49, c49);
			
		JLabel count49 = new JLabel(count[49][1]);
		count49.setFont(fontcount);
		c49.insets = new Insets(0, 0, 0, 0);
		c49.gridx = 0;
		c49.gridy = 2;
		p49.add(count49, c49);
			
		JButton b49 = new JButton("Добавить набор");
		b49.setBackground(color2);
		b49.setForeground(Color.white);
		b49.setFont(fontbtn);
		b49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b49.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[49]!=true){
						q.addSetOfSet(49);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[49][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c49.insets = new Insets(2, 0, 5, 0);
		c49.gridx = 0;
		c49.gridy = 3;
		p49.add(b49, c49);
		
		JButton bt49 = new JButton(new ImageIcon("res/images/8.png"));
		bt49.setVisible(visible[49]);
		bt49.setToolTipText("На изучении");
		bt49.setContentAreaFilled(false);
		bt49.setBorder(null);
		bt49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c49.anchor = GridBagConstraints.LAST_LINE_END;
		c49.gridx = 1;
		c49.gridy = 3;
		p49.add(bt49, c49);	
			
		//----------------------------------------------------------Панель Holmes
		JPanel p50 = new JPanel();
		GridBagLayout gbl50 = new GridBagLayout();
		p50.setLayout(gbl50);
		p50.setPreferredSize(new Dimension(250, 250));
		p50.setBackground(Color.white);
		p50.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(50);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р50
		GridBagConstraints c50 = new GridBagConstraints();
					
		JLabel image50 = new JLabel(new ImageIcon("files/literature and film/holmes/image.png")); //250*167
		c50.gridwidth=2;
		c50.gridx = 0;
		c50.gridy = 0;
		p50.add(image50, c50);
		
		JLabel text50 = new JLabel(count[50][0]);
		text50.setFont(fonttext);
		c50.gridx = 0;
		c50.gridy = 1;
		c50.insets = new Insets(5, 0, 5, 0);
		p50.add(text50, c50);
			
		JLabel count50 = new JLabel(count[50][1]);
		count50.setFont(fontcount);
		c50.insets = new Insets(0, 0, 0, 0);
		c50.gridx = 0;
		c50.gridy = 2;
		p50.add(count50, c50);
			
		JButton b50 = new JButton("Добавить набор");
		b50.setBackground(color2);
		b50.setForeground(Color.white);
		b50.setFont(fontbtn);
		b50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b50.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[50]!=true){
						q.addSetOfSet(50);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[50][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c50.insets = new Insets(2, 0, 5, 0);
		c50.gridx = 0;
		c50.gridy = 3;
		p50.add(b50, c50);
		
		JButton bt50 = new JButton(new ImageIcon("res/images/8.png"));
		bt50.setVisible(visible[50]);
		bt50.setToolTipText("На изучении");
		bt50.setContentAreaFilled(false);
		bt50.setBorder(null);
		bt50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c50.anchor = GridBagConstraints.LAST_LINE_END;
		c50.gridx = 1;
		c50.gridy = 3;
		p50.add(bt50, c50);
		
		//----------------------------------------------------Панель The Game of Thrones
		JPanel p51 = new JPanel();
		GridBagLayout gbl51 = new GridBagLayout();
		p51.setLayout(gbl51);
		p51.setPreferredSize(new Dimension(250, 250));
		p51.setBackground(Color.white);
		p51.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(51);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р51
		GridBagConstraints c51 = new GridBagConstraints();
					
		JLabel image51 = new JLabel(new ImageIcon("files/literature and film/the game of thrones/image.png")); //250*167
		c51.gridwidth=2;
		c51.gridx = 0;
		c51.gridy = 0;
		p51.add(image51, c51);
		
		JLabel text51 = new JLabel(count[51][0]);
		text51.setFont(fonttext);
		c51.gridx = 0;
		c51.gridy = 1;
		c51.insets = new Insets(5, 0, 5, 0);
		p51.add(text51, c51);
			
		JLabel count51 = new JLabel(count[51][1]);
		count51.setFont(fontcount);
		c51.insets = new Insets(0, 0, 0, 0);
		c51.gridx = 0;
		c51.gridy = 2;
		p51.add(count51, c51);
			
		JButton b51 = new JButton("Добавить набор");
		b51.setBackground(color2);
		b51.setForeground(Color.white);
		b51.setFont(fontbtn);
		b51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b51.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[51]!=true){
						q.addSetOfSet(51);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[51][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c51.insets = new Insets(2, 0, 5, 0);
		c51.gridx = 0;
		c51.gridy = 3;
		p51.add(b51, c51);
		
		JButton bt51 = new JButton(new ImageIcon("res/images/8.png"));
		bt51.setVisible(visible[51]);
		bt51.setToolTipText("На изучении");
		bt51.setContentAreaFilled(false);
		bt51.setBorder(null);
		bt51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c51.anchor = GridBagConstraints.LAST_LINE_END;
		c51.gridx = 1;
		c51.gridy = 3;
		p51.add(bt51, c51);
		
		//----------------------------------------------------Панель The matrix
		JPanel p52 = new JPanel();
		GridBagLayout gbl52 = new GridBagLayout();
		p52.setLayout(gbl52);
		p52.setPreferredSize(new Dimension(250, 250));
		p52.setBackground(Color.white);
		p52.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(52);
			}
			public void mousePressed(MouseEvent e) {
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}});
		p52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Добавляем елементы на панель р52
		GridBagConstraints c52 = new GridBagConstraints();
					
		JLabel image52 = new JLabel(new ImageIcon("files/literature and film/the matrix/image.jpg")); //250*167
		c52.gridwidth=2;
		c52.gridx = 0;
		c52.gridy = 0;
		p52.add(image52, c52);
		
		JLabel text52 = new JLabel(count[52][0]);
		text52.setFont(fonttext);
		c52.gridx = 0;
		c52.gridy = 1;
		c52.insets = new Insets(5, 0, 5, 0);
		p52.add(text52, c52);
			
		JLabel count52 = new JLabel(count[52][1]);
		count52.setFont(fontcount);
		c52.insets = new Insets(0, 0, 0, 0);
		c52.gridx = 0;
		c52.gridy = 2;
		p52.add(count52, c52);
			
		JButton b52 = new JButton("Добавить набор");
		b52.setBackground(color2);
		b52.setForeground(Color.white);
		b52.setFont(fontbtn);
		b52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b52.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (visible[52]!=true){
						q.addSetOfSet(52);
						repaintWindow();
					}else{
						JOptionPane.showConfirmDialog(null,"Вы уже добавили набор \""+count[52][0]+"\" на изучения", "Информация",JOptionPane.PLAIN_MESSAGE);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c52.insets = new Insets(2, 0, 5, 0);
		c52.gridx = 0;
		c52.gridy = 3;
		p52.add(b52, c52);
		
		JButton bt52 = new JButton(new ImageIcon("res/images/8.png"));
		bt52.setVisible(visible[52]);
		bt52.setToolTipText("На изучении");
		bt52.setContentAreaFilled(false);
		bt52.setBorder(null);
		bt52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c52.anchor = GridBagConstraints.LAST_LINE_END;
		c52.gridx = 1;
		c52.gridy = 3;
		p52.add(bt52, c52);
				
		gl6.add(box61);//Инормационный блок
		gl6.add(box62);//Блок 1
		box62.add(p48);
		box62.add(Box.createHorizontalStrut(50));
		box62.add(p49);
		box62.add(Box.createHorizontalStrut(50));
		box62.add(p50);
		box62.add(Box.createHorizontalStrut(50));
		box62.add(p51);
		
		gl6.add(box63);//Отсуп между блоками
		
		gl6.add(box64);//Блок 2
		box64.add(p52);
		box64.add(Box.createHorizontalStrut(900));
		
		flp.add(gl6);
	}
	
	//Разворачивания сворачивания набора
	private void actionClickBtn6() {
		if(click==0){
			click=1;
			box63.setVisible(true);
			box64.setVisible(true);
			gl6.setPreferredSize(new Dimension(1165, 617));
		}else{
			click=0;
			gl6.setPreferredSize(new Dimension(1165, 315));
			box63.setVisible(false);
			box64.setVisible(false);
			}
	}
	
	//*****************  КОНЕЦ ДОБАВЛЕНИЯ НАБОРОВ ********************//
	
	//Личный словарь пользователя для определенного набора
 	public void showSetWordsPanel(int a){
		int b=a;
		flp.removeAll();
		try {
			flp.add(new SetWordsVocabularyPanel(b));
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		flp.validate();
		flp.repaint();
	}
	
	public void repaintWindow() throws Exception{
		removeAll();
		add(new WordsSetAddPanel());
		validate();
		repaint();
	}
}

 