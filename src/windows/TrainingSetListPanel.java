package windows;

/* Панель со списком наборов для тренировок
   Содержит информацию о наборах которые пользователь добавил на изучения
 */

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Main;
import data.Set;

public class TrainingSetListPanel extends JPanel{

	 Set s = new Set();
	 String[] elements;
	 String [][] data;
	 Object str;
	 String count;	 
	 int item;
	 
	public TrainingSetListPanel(String q) throws Exception {
	
		count=q;
		int z;		
		data = s.getCountAddSet();
		z=data.length;
		elements=new String[z];
		
		for(int i=0; i<data.length; i++){
			elements[i]=data[i][0];
		}
		
		for(int i=1; i<data.length; i++){
			if(data[i][1].equalsIgnoreCase(count)){
				item=i;
			}
		}
		
		setLayout(new FlowLayout());
		setOpaque(false);
		setBackground(Color.white);
				
		JLabel message = new JLabel("Тренировать");
		Font fonttext = new Font("Arial", Font.PLAIN, 24);
		message.setFont(fonttext);
		final Box box1 = Box.createHorizontalBox();
		box1.add(message);
		add(box1);
				 
		final JComboBox list = new JComboBox(elements);
		list.setSelectedIndex(item);
		list.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				str=list.getSelectedItem();
				item=list.getSelectedIndex();
				for(int i=0; i<=data.length;i++){
					if(data[i][0]==str){
						count=data[i][1];
						break;
					}
				}
					try {
						MainWindow s2 = new MainWindow();
						if(elements[0].equalsIgnoreCase((String) str)){
							s2.showTrainingPanel(count);
							s2.createAndShowGUI();
						}else{
							s2.showTrainingPanelAll(count,count);
							s2.createAndShowGUI();
						}
						} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			
			});
		Box box2 = Box.createHorizontalBox();
		box2.add(list);
		add(box2);
	}
}
