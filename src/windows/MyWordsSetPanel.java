package windows;

/* Панель мои слова
   Содержит информацию о наборах которые пользователь добавил на изучения
*/
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import data.Set;


public class MyWordsSetPanel extends JPanel{

	private static final long serialVersionUID = -1765432244713700759L;
	JPanel flp,gl1, gl2;
	JScrollPane jScrollPane;
	Box box13,box14, box23,box24; 
	Color color1,color2;
	Font fonttext,fontbtn,fontcount; 
	Border Line1;
	Set q= new Set();
	MainWindow q1= new MainWindow();
	Boolean [] visible;     // Видимость набора 
	String [][] data; // Информация для отображения набора
			
	public MyWordsSetPanel() throws Exception {
				
		visible = q.getSetVisible();
		data = q.getCountWords();
		
		color1 = new Color(245,242,233); // grey
		color2 = new Color(75, 142, 197); // btn
				
		fonttext = new Font("Arial", Font.PLAIN, 20);// Текст к названию набора
		fontbtn = new Font("Arial", Font.PLAIN, 14); // Кнопка еще + тренровать слова
		fontcount = new Font("Arial", Font.PLAIN, 12);// Счетчик слов
		
		Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize ();
	    int vert = sSize.height-240;
	    int hor  = sSize.width-25;
		
		// Создаем основную панель Border Layout (1 cлой)
		setLayout(new BorderLayout());
		setOpaque(false);
		setPreferredSize(new Dimension(1349, vert));
		
		// Панель FlowLayout (3 cлой)
		flp = new JPanel();
		FlowLayout fl = new FlowLayout(1,50,40);
		flp.setLayout(fl);
		Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		flp.setPreferredSize(new Dimension(1349, 1800));
		flp.setOpaque(false);
				
		set1(); //Набор Популярное
		set2(); //Набор О Человеке
		
		//(2 cлой) прокрутка gl панелей
		JScrollPane jScrollPane = new JScrollPane(flp);
		jScrollPane.setBorder(null);
		jScrollPane.getViewport().setOpaque( false );  
		jScrollPane.setOpaque( false );  
		jScrollPane.getVerticalScrollBar().setUnitIncrement(25);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);	
		
		add("Center", jScrollPane);
		
	}
	
	//******************    ДОБАВЛЕНИЯ НАБОРОВ  **********************//
	
	// Набор ПОПУЛЯРНОЕ (8 подпанелей, 4 cлой)  
	private void set1(){
								
	// ------------------------------------------------------------Панель ЦВЕТА
		JPanel p1 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p1.setLayout(gbl1);
		p1.setPreferredSize(new Dimension(250, 250));
		p1.setBackground(Color.white);
		p1.setVisible(visible[1]);
		p1.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {
				showSetWordsPanel(1);
			}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {
		}});
		p1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					
		// Добавляем елементы на панель р1
		GridBagConstraints c1 = new GridBagConstraints();
				
		JLabel image1 = new JLabel(new ImageIcon(data[1][2])); 
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth = 2;
		p1.add(image1, c1);
							
		JLabel text1 = new JLabel(data[1][0]);
		text1.setFont(fonttext);
		c1.gridx = 0;
		c1.gridy = 1;
		c1.insets = new Insets(5, 0, 5, 0);
		p1.add(text1, c1);
			
		JLabel count1 = new JLabel(data[1][1]);
		count1.setFont(fontcount);
		count1.setBackground(color1);
		c1.insets = new Insets(0, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 2;
		p1.add(count1, c1);
					
		JButton bt1 = new JButton("Изучить слова");
		bt1.setBackground(color2);
		bt1.setForeground(Color.white);
		bt1.setFont(fontbtn);
		bt1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bt1.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			String item="1";
			q1.showTrainingPanelAll(item, item);
			q1.createAndShowGUI();
		}
		});
		c1.insets = new Insets(2, 0, 5, 0);
		c1.gridx = 0;
		c1.gridy = 3;
		p1.add(bt1, c1);
		
		JButton b1 = new JButton(new ImageIcon("res/images/7.png"));
		b1.setToolTipText("Удалить набор");
		b1.setContentAreaFilled(false);
		b1.setBorder(null);
		b1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					q.delSetOfSet(1);
					q.delSetOfWords(1);
					repaintWindow();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			});
		c1.anchor = GridBagConstraints.LAST_LINE_END;
		c1.gridx = 1;
		c1.gridy = 3;
		p1.add(b1, c1);
	
	//------------------------------------------------------Панель Дом и быт
	JPanel p2 = new JPanel();
	GridBagLayout gbl2 = new GridBagLayout();
	p2.setLayout(gbl2);
	p2.setPreferredSize(new Dimension(250, 250));
	p2.setBackground(Color.white);
	p2.setVisible(visible[2]);
	p2.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(2);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р2
	GridBagConstraints c2 = new GridBagConstraints();
			
	JLabel image2 = new JLabel(new ImageIcon(data[2][2])); 
	c2.gridx = 0;
	c2.gridy = 0;
	c2.gridwidth = 2;
	p2.add(image2, c2);
						
	JLabel text2 = new JLabel(data[2][0]);
	text2.setFont(fonttext);
	c2.gridx = 0;
	c2.gridy = 1;
	c2.insets = new Insets(5, 0, 5, 0);
	p2.add(text2, c2);
		
	JLabel count2 = new JLabel(data[2][1]);
	count2.setFont(fontcount);
	count2.setBackground(color1);
	c2.insets = new Insets(0, 0, 0, 0);
	c2.gridx = 0;
	c2.gridy = 2;
	p2.add(count2, c2);
				
	JButton bt2 = new JButton("Изучить слова");
	bt2.setBackground(color2);
	bt2.setForeground(Color.white);
	bt2.setFont(fontbtn);
	bt2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt2.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="2";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c2.insets = new Insets(2, 0, 5, 0);
	c2.gridx = 0;
	c2.gridy = 3;
	p2.add(bt2, c2);
	
	JButton b2 = new JButton(new ImageIcon("res/images/7.png"));
	b2.setToolTipText("Удалить набор");
	b2.setContentAreaFilled(false);
	b2.setBorder(null);
	b2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(2);
				q.delSetOfWords(2);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c2.anchor = GridBagConstraints.LAST_LINE_END;
	c2.gridx = 1;
	c2.gridy = 3;
	p2.add(b2, c2);
	
	//---------------------------------------------------------Панель Строение человека
	JPanel p3 = new JPanel();
	GridBagLayout gbl3 = new GridBagLayout();
	p3.setLayout(gbl3);
	p3.setPreferredSize(new Dimension(250, 250));
	p3.setBackground(Color.white);
	p3.setVisible(visible[3]);
	p3.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(3);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р3
	GridBagConstraints c3 = new GridBagConstraints();
			
	JLabel image3 = new JLabel(new ImageIcon(data[3][2])); 
	c3.gridx = 0;
	c3.gridy = 0;
	c3.gridwidth = 2;
	p3.add(image3, c3);
						
	JLabel text3 = new JLabel(data[3][0]);
	text3.setFont(fonttext);
	c3.gridx = 0;
	c3.gridy = 1;
	c3.insets = new Insets(5, 0, 5, 0);
	p3.add(text3, c3);
		
	JLabel count3 = new JLabel(data[3][1]);
	count3.setFont(fontcount);
	count3.setBackground(color1);
	c3.insets = new Insets(0, 0, 0, 0);
	c3.gridx = 0;
	c3.gridy = 2;
	p3.add(count3, c3);
				
	JButton bt3 = new JButton("Изучить слова");
	bt3.setBackground(color2);
	bt3.setForeground(Color.white);
	bt3.setFont(fontbtn);
	bt3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt3.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="3";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c3.insets = new Insets(2, 0, 5, 0);
	c3.gridx = 0;
	c3.gridy = 3;
	p3.add(bt3, c3);
	
	JButton b3 = new JButton(new ImageIcon("res/images/7.png"));
	b3.setToolTipText("Удалить набор");
	b3.setContentAreaFilled(false);
	b3.setBorder(null);
	b3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b3.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(3);
				q.delSetOfWords(3);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c3.anchor = GridBagConstraints.LAST_LINE_END;
	c3.gridx = 1;
	c3.gridy = 3;
	p3.add(b3, c3);
				
	//----------------------------------------------------------Панель Члены семьи
	JPanel p4 = new JPanel();
	GridBagLayout gbl4 = new GridBagLayout();
	p4.setLayout(gbl4);
	p4.setPreferredSize(new Dimension(250, 250));
	p4.setBackground(Color.white);
	p4.setVisible(visible[4]);
	p4.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(4);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р4
	GridBagConstraints c4 = new GridBagConstraints();
			
	JLabel image4 = new JLabel(new ImageIcon(data[4][2])); 
	c4.gridx = 0;
	c4.gridy = 0;
	c4.gridwidth = 2;
	p4.add(image4, c4);
						
	JLabel text4 = new JLabel(data[4][0]);
	text4.setFont(fonttext);
	c4.gridx = 0;
	c4.gridy = 1;
	c4.insets = new Insets(5, 0, 5, 0);
	p4.add(text4, c4);
		
	JLabel count4 = new JLabel(data[4][1]);
	count4.setFont(fontcount);
	count4.setBackground(color1);
	c4.insets = new Insets(0, 0, 0, 0);
	c4.gridx = 0;
	c4.gridy = 2;
	p4.add(count4, c4);
				
	JButton bt4 = new JButton("Изучить слова");
	bt4.setBackground(color2);
	bt4.setForeground(Color.white);
	bt4.setFont(fontbtn);
	bt4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt4.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="4";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c4.insets = new Insets(2, 0, 5, 0);
	c4.gridx = 0;
	c4.gridy = 4;
	p4.add(bt4, c4);
	
	JButton b4 = new JButton(new ImageIcon("res/images/7.png"));
	b4.setToolTipText("Удалить набор");
	b4.setContentAreaFilled(false);
	b4.setBorder(null);
	b4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b4.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(4);
				q.delSetOfWords(4);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c4.anchor = GridBagConstraints.LAST_LINE_END;
	c4.gridx = 1;
	c4.gridy = 4;
	p4.add(b4, c4);
						
	//----------------------------------------------------Панель Чувства и эмоции
	JPanel p5 = new JPanel();
	GridBagLayout gbl5 = new GridBagLayout();
	p5.setLayout(gbl5);
	p5.setPreferredSize(new Dimension(250, 250));
	p5.setBackground(Color.white);
	p5.setVisible(visible[5]);
	p5.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(5);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р5
	GridBagConstraints c5 = new GridBagConstraints();
			
	JLabel image5 = new JLabel(new ImageIcon(data[5][2])); 
	c5.gridx = 0;
	c5.gridy = 0;
	c5.gridwidth = 2;
	p5.add(image5, c5);
						
	JLabel text5 = new JLabel(data[5][0]);
	text5.setFont(fonttext);
	c5.gridx = 0;
	c5.gridy = 1;
	c5.insets = new Insets(5, 0, 5, 0);
	p5.add(text5, c5);
		
	JLabel count5 = new JLabel(data[5][1]);
	count5.setFont(fontcount);
	count5.setBackground(color1);
	c5.insets = new Insets(0, 0, 0, 0);
	c5.gridx = 0;
	c5.gridy = 2;
	p5.add(count5, c5);
				
	JButton bt5 = new JButton("Изучить слова");
	bt5.setBackground(color2);
	bt5.setForeground(Color.white);
	bt5.setFont(fontbtn);
	bt5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt5.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="5";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c5.insets = new Insets(2, 0, 5, 0);
	c5.gridx = 0;
	c5.gridy = 3;
	p5.add(bt5, c5);
	
	JButton b5 = new JButton(new ImageIcon("res/images/7.png"));
	b5.setToolTipText("Удалить набор");
	b5.setContentAreaFilled(false);
	b5.setBorder(null);
	b5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b5.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(5);
				q.delSetOfWords(5);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c5.anchor = GridBagConstraints.LAST_LINE_END;
	c5.gridx = 1;
	c5.gridy = 3;
	p5.add(b5, c5);
				
	//--------------------------------------------------------------Панель ТОП 100 существительных
	JPanel p6 = new JPanel();
	GridBagLayout gbl6 = new GridBagLayout();
	p6.setLayout(gbl6);
	p6.setPreferredSize(new Dimension(250, 250));
	p6.setBackground(Color.white);
	p6.setVisible(visible[6]);
	p6.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(6);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р6
	GridBagConstraints c6 = new GridBagConstraints();
			
	JLabel image6 = new JLabel(new ImageIcon(data[6][2])); 
	c6.gridx = 0;
	c6.gridy = 0;
	c6.gridwidth = 2;
	p6.add(image6, c6);
						
	JLabel text6 = new JLabel(data[6][0]);
	text6.setFont(fonttext);
	c6.gridx = 0;
	c6.gridy = 1;
	c6.insets = new Insets(5, 0, 5, 0);
	p6.add(text6, c6);
		
	JLabel count6 = new JLabel(data[6][1]);
	count6.setFont(fontcount);
	count6.setBackground(color1);
	c6.insets = new Insets(0, 0, 0, 0);
	c6.gridx = 0;
	c6.gridy = 2;
	p6.add(count6, c6);
				
	JButton bt6 = new JButton("Изучить слова");
	bt6.setBackground(color2);
	bt6.setForeground(Color.white);
	bt6.setFont(fontbtn);
	bt6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt6.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="6";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c6.insets = new Insets(2, 0, 5, 0);
	c6.gridx = 0;
	c6.gridy = 3;
	p6.add(bt6, c6);
	
	JButton b6 = new JButton(new ImageIcon("res/images/7.png"));
	b6.setToolTipText("Удалить набор");
	b6.setContentAreaFilled(false);
	b6.setBorder(null);
	b6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b6.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(6);
				q.delSetOfWords(6);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c6.anchor = GridBagConstraints.LAST_LINE_END;
	c6.gridx = 1;
	c6.gridy = 3;
	p6.add(b6, c6);
	
	//------------------------------------------------------------------Панель Предлоги
	JPanel p7 = new JPanel();
	GridBagLayout gbl7 = new GridBagLayout();
	p7.setLayout(gbl7);
	p7.setPreferredSize(new Dimension(250, 250));
	p7.setBackground(Color.white);
	p7.setVisible(visible[7]);
	p7.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(7);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р7
	GridBagConstraints c7 = new GridBagConstraints();
			
	JLabel image7 = new JLabel(new ImageIcon(data[7][2])); 
	c7.gridx = 0;
	c7.gridy = 0;
	c7.gridwidth = 2;
	p7.add(image7, c7);
						
	JLabel text7 = new JLabel(data[7][0]);
	text7.setFont(fonttext);
	c7.gridx = 0;
	c7.gridy = 1;
	c7.insets = new Insets(5, 0, 5, 0);
	p7.add(text7, c7);
		
	JLabel count7 = new JLabel(data[7][1]);
	count7.setFont(fontcount);
	count7.setBackground(color1);
	c7.insets = new Insets(0, 0, 0, 0);
	c7.gridx = 0;
	c7.gridy = 2;
	p7.add(count7, c7);
				
	JButton bt7 = new JButton("Изучить слова");
	bt7.setBackground(color2);
	bt7.setForeground(Color.white);
	bt7.setFont(fontbtn);
	bt7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt7.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="7";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c7.insets = new Insets(2, 0, 5, 0);
	c7.gridx = 0;
	c7.gridy = 3;
	p7.add(bt7, c7);
	
	JButton b7 = new JButton(new ImageIcon("res/images/7.png"));
	b7.setToolTipText("Удалить набор");
	b7.setContentAreaFilled(false);
	b7.setBorder(null);
	b7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b7.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(7);
				q.delSetOfWords(7);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c7.anchor = GridBagConstraints.LAST_LINE_END;
	c7.gridx = 1;
	c7.gridy = 3;
	p7.add(b7, c7);
				
	//----------------------------------------------------------------Панель Местоимения
	JPanel p8 = new JPanel();
	GridBagLayout gbl8 = new GridBagLayout();
	p8.setLayout(gbl8);
	p8.setPreferredSize(new Dimension(250, 250));
	p8.setBackground(Color.white);
	p8.setVisible(visible[8]);
	p8.addMouseListener(new MouseListener() {
		
		public void mouseClicked(MouseEvent e) {
			showSetWordsPanel(8);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {
	}});
	p8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
	// Добавляем елементы на панель р8
	GridBagConstraints c8 = new GridBagConstraints();
			
	JLabel image8 = new JLabel(new ImageIcon(data[8][2])); 
	c8.gridx = 0;
	c8.gridy = 0;
	c8.gridwidth = 2;
	p8.add(image8, c8);
						
	JLabel text8 = new JLabel(data[8][0]);
	text8.setFont(fonttext);
	c8.gridx = 0;
	c8.gridy = 1;
	c8.insets = new Insets(5, 0, 5, 0);
	p8.add(text8, c8);
		
	JLabel count8 = new JLabel(data[8][1]);
	count8.setFont(fontcount);
	count8.setBackground(color1);
	c8.insets = new Insets(0, 0, 0, 0);
	c8.gridx = 0;
	c8.gridy = 2;
	p8.add(count8, c8);
				
	JButton bt8 = new JButton("Изучить слова");
	bt8.setBackground(color2);
	bt8.setForeground(Color.white);
	bt8.setFont(fontbtn);
	bt8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	bt8.addActionListener(new ActionListener() {
	
	public void actionPerformed(ActionEvent e) {
		String item="8";
		q1.showTrainingPanelAll(item, item);
		q1.createAndShowGUI();
	}
	});
	c8.insets = new Insets(2, 0, 5, 0);
	c8.gridx = 0;
	c8.gridy = 3;
	p8.add(bt8, c8);
	
	JButton b8 = new JButton(new ImageIcon("res/images/7.png"));
	b8.setToolTipText("Удалить набор");
	b8.setContentAreaFilled(false);
	b8.setBorder(null);
	b8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	b8.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				q.delSetOfSet(8);
				q.delSetOfWords(8);
				repaintWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		});
	c8.anchor = GridBagConstraints.LAST_LINE_END;
	c8.gridx = 1;
	c8.gridy = 3;
	p8.add(b8, c8);

	flp.add(p1);
	flp.add(p2);
	flp.add(p3);
	flp.add(p4);
	flp.add(p5);
	flp.add(p6);
	flp.add(p7);
	flp.add(p8);
	//--------------------------------------------------------Конец набора ПОПУЛЯРНОЕ
	}
			
	// Набор О ЧЕЛОВЕКЕ (18 подпанелей, 4 cлой) 
	private void set2(){

	// ------------------------------------------------------------Панель Эмоции (экспертный уровень)
	
		
	//---------------------------------------------------------Панель Эмоции (для начинающих)
	
		
	//----------------------------------------------------------Панель Все для дома
	
				
	//----------------------------------------------------Панель Чувства и эмоции
	
		
	//--------------------------------------------------------------Панель ТОП 100 существительных
	
	//------------------------------------------------------------------Панель Предлоги
	
		
	//----------------------------------------------------------------Панель Местоимения
	
		
	/*flp.add(p21);
	flp.add(p22);
	flp.add(p23);
	flp.add(p24);
	flp.add(p25);
	flp.add(p26);
	flp.add(p27);
	flp.add(p28);*/
	}
	
	//*****************  КОНЕЦ ДОБАВЛЕНИЯ НАБОРОВ ********************//
		
	public void repaintWindow() throws Exception{
		removeAll();
		add(new MyWordsSetPanel());
		validate();
		repaint();
	}
	
	//Личный словарь пользователя
	public void showSetWordsPanel(int a){
		int b=a;
		flp.removeAll();
		try {
			flp.add(new SetWordsVocabularyPanel(b));
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		flp.validate();
		flp.repaint();
	}
}