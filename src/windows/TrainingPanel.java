package windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import data.TakeProgresWords;

public class TrainingPanel extends JPanel {

	private static final long serialVersionUID = -1772362933820173815L;
	
	JPanel p1,p2,p3,p4,p5,p6;
	private Color color1,color2,color3;
	TakeProgresWords s=new TakeProgresWords();
	MainWindow s2 = new MainWindow();
	int[] allCount;
	String txt1,txt2,txt3,txt4,txt5,txt6,item1;
	String training;
	JButton btn1,btn2,btn3,btn5,btn6;
	
	public TrainingPanel(String a) throws Exception {
		
		item1=a;
		allCount = s.getProgresWordsAll();
				
		color1 = new Color(245,242,233); // grey
		color2 = new Color(75, 142, 197); // btn
		color3 = new Color(245,242,233); // grey
		
		Font fonttext = new Font("Arial", Font.PLAIN, 20);
		Font fontbtn = new Font("Arial", Font.PLAIN, 14);
		Font fontcount = new Font("Arial", Font.PLAIN, 12);
		
		// Создаем основную панель Grid Layout
		setLayout(new GridLayout(2,3,40,40));
		setOpaque(false);
		setBackground(color1);
		
		// Панель Слово-перевод
		p1 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p1.setLayout(gbl1);
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		p1.setPreferredSize(new Dimension(230, 200));
		p1.setBackground(Color.WHITE);
		p1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Панель Перевод-слово
		p2 = new JPanel();
		GridBagLayout gbl2 = new GridBagLayout();
		p2.setLayout(gbl2);
		p2.setPreferredSize(new Dimension(230, 200));
		p2.setBackground(Color.WHITE);
		p2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
		// Панель Конструктор слов
		p3 = new JPanel();
		GridBagLayout gbl3 = new GridBagLayout();
		p3.setLayout(gbl3);
		p3.setBackground(Color.WHITE);
		p3.setPreferredSize(new Dimension(230, 200));
		p3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));	
		
		// Панель audio
		p4 = new JPanel();
		GridBagLayout gbl4 = new GridBagLayout();
		p4.setLayout(gbl4);
		p4.setPreferredSize(new Dimension(230, 200));
		p4.setBackground(Color.WHITE);
		p4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));	
		
		// Панель Карточка слов
		p5 = new JPanel();
		GridBagLayout gbl5 = new GridBagLayout();
		p5.setLayout(gbl5);
		p5.setPreferredSize(new Dimension(230, 200));
		p5.setBackground(Color.WHITE);
		p5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		// Панель Кросворд слов
		p6 = new JPanel();
		GridBagLayout gbl6 = new GridBagLayout();
		p6.setLayout(gbl6);
		p6.setBackground(Color.WHITE);
		p6.setPreferredSize(new Dimension(230, 200));
		p6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		//------------------------------------------------Добавляем елементы на панель р1 (Слово-перевод)
		GridBagConstraints c1 = new GridBagConstraints();
		
		JLabel image1 = new JLabel(new ImageIcon("res/images/Word_translate.png"));
		c1.insets = new Insets(0, 5, 0, 5);
		c1.gridx = 0;
		c1.gridy = 0;
		p1.add(image1, c1);
		
		JLabel text1 = new JLabel("Слово-перевод");
		text1.setFont(fonttext);
		c1.gridx = 0;
		c1.gridy = 1;
		c1.insets = new Insets(5, 0, 5, 0);
		p1.add(text1, c1);
		
		btn1 = new JButton();
		btn1.setVisible(true);
		if (allCount[0]<=12 && allCount[0]!=0){
			txt1="Тренировать "+allCount[0]+" слов";
			btn1.setText(txt1);
		}else if(allCount[0]==0){
			btn1.setVisible(false);
		}else{
			txt1="Тренировать 12 слов";	
			btn1.setText(txt1);
		}
		btn1.setBackground(color2);
		btn1.setForeground(Color.white);
		btn1.setFont(fontbtn);
		btn1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
				training="words_translate";
				actionClickBtn1();
			}
		});
		c1.insets = new Insets(2, 0, 5, 0);
		c1.gridx = 0;
		c1.gridy = 2;
		p1.add(btn1, c1);
				
		if (allCount[0]==0){
			txt1="Нет слов для изучения";
		}else{
			txt1="еще "+allCount[0]+" слов";	
		}
		
		JLabel count1 = new JLabel(txt1);
		count1.setFont(fontcount);
		count1.setBackground(color1);
		c1.gridx = 0;
		c1.gridy = 3;
		c1.insets = new Insets(5, 0, 0, 0);
		p1.add(count1, c1);
		
		//------------------------------------------------Добавляем елементы на панель р2 (Перевод-слово)
		GridBagConstraints c2 = new GridBagConstraints();
				
		JLabel image2 = new JLabel(new ImageIcon("res/images/Translate_word.jpg"));
		c2.insets = new Insets(0, 5, 0, 5);
		c2.gridx = 0;
		c2.gridy = 0;
		p2.add(image2, c2);
				
		JLabel text2 = new JLabel("Перевод-слово");
		text2.setFont(fonttext);
		c2.gridx = 0;
		c2.gridy = 1;
		c2.insets = new Insets(5, 0, 5, 0);
		p2.add(text2, c2);
				
		btn2 = new JButton();
		btn2.setVisible(true);
		if (allCount[1]<=12 && allCount[1]!=0){
			txt2="Тренировать "+allCount[1]+" слов";
			btn2.setText(txt2);
		}else if(allCount[1]==0){
			btn2.setVisible(false);
		}else{
			txt2="Тренировать 12 слов";	
			btn2.setText(txt2);
		}
		btn2.setBackground(color2);
		btn2.setForeground(Color.white);
		btn2.setFont(fontbtn);
		btn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
				training="translate_words";
				actionClickBtn2();
			}
		});
		c2.insets = new Insets(2, 0, 5, 0);
		c2.gridx = 0;
		c2.gridy = 2;
		p2.add(btn2, c2);
				
		if (allCount[1]==0){
			txt2="Нет слов для изучения";
		}else{
			txt2="еще "+allCount[1]+" слов";	
		}
		
		JLabel count2 = new JLabel(txt2);
		count2.setFont(fontcount);
		count2.setBackground(color1);
		c2.gridx = 0;
		c2.gridy = 3;
		c2.insets = new Insets(5, 0, 0, 0);
		p2.add(count2, c2);
		
		//------------------------------------------------Добавляем елементы на панель р3 (Конструктор слов)
		GridBagConstraints c3 = new GridBagConstraints();
			
		JLabel image3 = new JLabel(new ImageIcon("res/images/Designer_word.png"));
		c3.insets = new Insets(0, 5, 0, 5);
		c3.gridx = 0;
		c3.gridy = 0;
		p3.add(image3, c3);
				
		JLabel text3 = new JLabel("Конструктор слов");
		text3.setFont(fonttext);
		c3.gridx = 0;
		c3.gridy = 1;
		c3.insets = new Insets(5, 0, 5, 0);
		p3.add(text3, c3);
		
		btn3 = new JButton();
		btn3.setVisible(true);
		if (allCount[2]<=12 && allCount[2]!=0){
			txt3="Тренировать "+allCount[2]+" слов";
			btn3.setText(txt3);
		}else if(allCount[2]==0){
			btn3.setVisible(false);
		}else{
			txt3="Тренировать 12 слов";	
			btn3.setText(txt3);
		}
		btn3.setBackground(color2);
		btn3.setForeground(Color.white);
		btn3.setFont(fontbtn);
		btn3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn3.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
				training="konstructor";
				actionClickBtn3();
			}
		});
		c3.insets = new Insets(2, 0, 5, 0);
		c3.gridx = 0;
		c3.gridy = 2;
		p3.add(btn3, c3);
				
		if (allCount[2]==0){
			txt3="Нет слов для изучения";
		}else{
			txt3="еще "+allCount[2]+" слов";	
		}
		JLabel count3 = new JLabel(txt3);
		count3.setFont(fontcount);
		count3.setBackground(color1);
		c3.gridx = 0;
		c3.gridy = 3;
		c3.insets = new Insets(5, 0, 0, 0);
		p3.add(count3, c3);		
		
		//------------------------------------------------Добавляем елементы на панель р4 (Аудирование)
		GridBagConstraints c4 = new GridBagConstraints();
			
		JLabel image4 = new JLabel(new ImageIcon("res/images/Audio.jpg"));
		c4.insets = new Insets(0, 5, 0, 5);
		c4.gridx = 0;
		c4.gridy = 0;
		p4.add(image4, c4);
				
		JLabel text4 = new JLabel("Аудирование");
		text4.setFont(fonttext);
		c4.gridx = 0;
		c4.gridy = 1;
		c4.insets = new Insets(5, 0, 5, 0);
		p4.add(text4, c4);
						
		JButton btn4 = new JButton("Тренировать 10 слов");
		btn4.setBackground(color2);
		btn4.setForeground(Color.white);
		btn4.setFont(fontbtn);
		btn4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		/*btn1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		actionClickBtn1();
		}
		});*/
		c4.insets = new Insets(2, 0, 5, 0);
		c4.gridx = 0;
		c4.gridy = 2;
		p4.add(btn4, c4);
		
		if (allCount[3]==0){
			txt4="Нет слов для изучения";
		}else{
			txt4="еще "+allCount[3]+" слов";	
		}
		JLabel count4 = new JLabel(txt4);
		count4.setFont(fontcount);
		count4.setBackground(color1);
		c4.gridx = 0;
		c4.gridy = 3;
		c4.insets = new Insets(5, 0, 0, 0);
		p4.add(count4, c4);

		//------------------------------------------------Добавляем елементы на панель р5 (Словарные карточки)
		GridBagConstraints c5 = new GridBagConstraints();
			
		JLabel image5 = new JLabel(new ImageIcon("res/images/Word_card.png"));
		c5.insets = new Insets(0, 5, 0, 5);
		c5.gridx = 0;
		c5.gridy = 0;
		p5.add(image5, c5);
				
		JLabel text5 = new JLabel("Словарные карточки");
		text5.setFont(fonttext);
		c5.gridx = 0;
		c5.gridy = 1;
		c5.insets = new Insets(5, 0, 5, 0);
		p5.add(text5, c5);
				
		btn5 = new JButton();
		btn5.setVisible(true);
		if (allCount[4]<=12 && allCount[4]!=0){
			txt5="Тренировать "+allCount[4]+" слов";
			btn5.setText(txt5);
		}else if(allCount[4]==0){
			btn5.setVisible(false);
		}else{
			txt5="Тренировать 12 слов";	
			btn5.setText(txt5);
		}
		btn5.setBackground(color2);
		btn5.setForeground(Color.white);
		btn5.setFont(fontbtn);
		btn5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn5.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				training="words_card";
				actionClickBtn5();
			}
		});
		c5.insets = new Insets(2, 0, 5, 0);
		c5.gridx = 0;
		c5.gridy = 2;
		p5.add(btn5, c5);
		
		if (allCount[4]==0){
			txt5="Нет слов для изучения";
		}else{
			txt5="еще "+allCount[4]+" слов";	
		}
		JLabel count5 = new JLabel(txt5);
		count5.setFont(fontcount);
		count5.setBackground(color1);
		c5.gridx = 0;
		c5.gridy = 3;
		c5.insets = new Insets(5, 0, 0, 0);
		p5.add(count5, c5);
		
		//------------------------------------------------Добавляем елементы на панель р6 (Кроссворд)
		GridBagConstraints c6 = new GridBagConstraints();
			
		JLabel image6 = new JLabel(new ImageIcon("res/images/Konstructor.jpg"));
		c6.insets = new Insets(0, 5, 0, 5);
		c6.gridx = 0;
		c6.gridy = 0;
		p6.add(image6, c6);
				
		JLabel text6 = new JLabel("Кроссворд");
		text6.setFont(fonttext);
		c6.gridx = 0;
		c6.gridy = 1;
		c6.insets = new Insets(5, 0, 5, 0);
		p6.add(text6, c6);
						
		btn6 = new JButton("Тренировать 12 слов");
		btn6.setBackground(color2);
		btn6.setForeground(Color.white);
		btn6.setFont(fontbtn);
		btn6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn6.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				training="crossword";
				actionClickBtn6();
			}
		});
		c6.insets = new Insets(2, 0, 5, 0);
		c6.gridx = 0;
		c6.gridy = 2;
		p6.add(btn6, c6);
		
		if (allCount[5]==0){
			txt6="Нет слов для изучения";
		}else{
			txt6="еще "+allCount[5]+" слов";	
		}
		JLabel count6 = new JLabel(txt6);
		count6.setFont(fontcount);
		count6.setBackground(color1);
		c6.gridx = 0;
		c6.gridy = 3;
		c6.insets = new Insets(5, 0, 0, 0);
		p6.add(count6, c6);
				
		add(p1);
		add(p2);
		add(p3);
		add(p4);
		add(p5);
		add(p6);
	}

	//Тренировка слово-перевод
	public void actionClickBtn1(){
		String item = null;
		s2.showWordsTranslatePanel(item, item1, training);
		s2.createAndShowGUI();
	}
			
	//Тренировка перевод-слово
	public void actionClickBtn2(){
		String item = null;
		s2.showTranslateWordsPanel(item, item1, training);
		s2.createAndShowGUI();
	}		
	
	//Тренировка конструктор слов
	public void actionClickBtn3(){
		String item = null;
		s2.showWordsKonstructorPanel(item, item1, training);
		s2.createAndShowGUI();
	}
		
	//Тренировка Словарные карточки
	public void actionClickBtn5(){
		String item = null;
		s2.showWordsKard(item, item1, training);
		s2.createAndShowGUI();
	}
	
	//Тренировка Кроссворд
	public void actionClickBtn6(){
		String item = null;
		s2.showCrossword(item, item1, training);
		s2.createAndShowGUI();
	}
}
