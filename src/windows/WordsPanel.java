package windows;

/* Панель выбора (Мои слова или Обзор)
   предназначена для перехода между панелей Мои слова и панелей Обзор
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.border.Border;

import data.Set;
import data.TakeWords;

public class WordsPanel extends JPanel{

	private static final long serialVersionUID = -1765432244713700759L;
	JPanel p1,p2,p3,windowContent;
	private Color color1,color2,color3;
	int a=1; //состояния панели
	Set s = new Set();
	MainWindow s2 = new MainWindow();
	
	//Панель по умолчанию
	public WordsPanel() throws Exception {
		
		color1 = new Color(245,242,233); // grey
		color2 = new Color(75, 142, 197); // btn
		color3 = new Color(245,242,233); // grey
		
		Font fontbtn = new Font("Arial", Font.PLAIN, 18);
		
		// Создаем основную панель Border Layout
		setLayout(new BorderLayout());
		//setBackground(color1);
		
		// Панель p1
		p1 = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		p1.setLayout(gbl1);
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		p1.setBackground(Color.white);
		//p1.setBorder(Line1);
		p1.setPreferredSize(new Dimension(520, 77));
								
		// Панель p2
		p2 = new JPanel();
		GridBagLayout gbl2 = new GridBagLayout();
		//p2.setBorder(Line1);
		p2.setLayout(gbl2);
		p2.setPreferredSize(new Dimension(61, 77));
				
		//------------------------------------------------Добавляем елементы на панель р1 (Мои слова)
		GridBagConstraints c1 = new GridBagConstraints();
		
		JButton btn1 = new JButton(new ImageIcon("res/images/5.png"));
		btn1.setContentAreaFilled(false);
		btn1.setBorder(null);
		btn1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c1.insets = new Insets(0, 0, 0, 0);
		c1.gridx = 0;
		c1.gridy = 0;
		p1.add(btn1, c1);
		
		int b=s.getStudyCountSetOfSet();
		JButton btn2 = new JButton("На изучении "+"("+b+")");
		btn2.setContentAreaFilled(false);
		btn2.setFont(fontbtn);
		btn2.setBorder(null);
		btn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c1.insets = new Insets(0, 10, 0, 10);
		c1.gridx = 1;
		c1.gridy = 0;
		p1.add(btn2, c1);
		
		JButton btn3 = new JButton("Изучено (0)");
		btn3.setContentAreaFilled(false);
		btn3.setFont(fontbtn);
		btn3.setBorder(null);
		btn3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		c1.insets = new Insets(0, 0, 0, 150);
		c1.gridx = 2;
		c1.gridy = 0;
		p1.add(btn3, c1);
		
		//------------------------------------------------Добавляем елементы на панель р2 (Обзор)
		GridBagConstraints c2 = new GridBagConstraints();
				
		JButton btn4 = new JButton(new ImageIcon("res/images/6.png"));
		btn4.setContentAreaFilled(false);
		btn4.setBorder(null);
		btn4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn4.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
				s2.showWordsPanelTwo();
				s2.showWordsSetAddPanel();
				s2.createAndShowGUI();
			}
		});
		c2.insets = new Insets(0, 0, 0, 0);
		c2.gridx = 0;
		c2.gridy = 0;
		p2.add(btn4, c2);
			
		add("West", p1);
		add("East", p2);
		
	}
	
	
}