package windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

import data.Set;

public class InsertData extends JPanel {

	private static final long serialVersionUID = 1L;
	Set s = new Set();
	String[] elements;
	String[] numbers=new String []{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
	int set_id,set_count;
	JTextField words1,words2,words3,words4,words5,words6,words7,words8,words9,words10;
	JTextField transc1,transc2,transc3,transc4,transc5,transc6,transc7,transc8,transc9,transc10;
	JTextField transl1,transl2,transl3,transl4,transl5,transl6,transl7,transl8,transl9,transl10;
	JPanel p2;
	JLabel atention;
			
	public InsertData() throws Exception {
				
		elements = s.getAllSet();
			
		setLayout(new BorderLayout());
		//setOpaque(false);
		setPreferredSize(new Dimension(600, 600));
		setBackground(Color.white);
		Border Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
		Font fonttext = new Font("Arial", Font.PLAIN, 14);
		
		JPanel p1 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p1.setLayout(fl);
		//p1.setOpaque(false);
		p1.setBorder(Line1);
		p1.setPreferredSize(new Dimension(550, 70));
		
		p2 = new JPanel();
		FlowLayout fl1 = new FlowLayout();
		p2.setLayout(fl1);
		//p2.setOpaque(false);
		p2.setBorder(Line1);
		p2.setPreferredSize(new Dimension(550, 700));
		
		JScrollPane jScrollPane = new JScrollPane(p2);
		jScrollPane.setBorder(null);
		jScrollPane.getViewport().setOpaque( false );  
		jScrollPane.setOpaque( false );  
		jScrollPane.getVerticalScrollBar().setUnitIncrement(25);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);	
				
		//--------------------------------Panel p1
		JLabel message = new JLabel("Выберите набор для добавления данных");
		Font fontmessage = new Font("Arial", Font.PLAIN, 20);
		message.setFont(fontmessage);
		Box b1 = Box.createHorizontalBox();
		b1.add(message);
		p1.add(b1);
				 
		final JComboBox list = new JComboBox(elements);
		list.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				set_id=list.getSelectedIndex()+1;
				atention.setVisible(false);
				}
		});
		Box b2 = Box.createHorizontalBox();
		b2.add(list);
		p1.add(b2);
				
		/*Box tmp = Box.createHorizontalBox();
		tmp.setPreferredSize(new Dimension(450, 30));
		p1.add(tmp);
		
		JLabel message1 = new JLabel("Выберите количество слов для добавления");
		message1.setFont(fontmessage);
		Box b3 = Box.createHorizontalBox();
		b3.add(message1);
		p1.add(b3);
				 
		final JComboBox list1 = new JComboBox(numbers);
		list1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				set_count=list1.getSelectedIndex()+1;
				System.out.println(set_count);
				}
		});
		Box b4 = Box.createHorizontalBox();
		b4.add(list1);
		p1.add(b4);*/
		//------------------------------Panel p2
		
		//Name fields
		Box bx3 = Box.createHorizontalBox();
		bx3.setPreferredSize(new Dimension(500, 50));
		bx3.setBorder(Line1);
				
		JLabel lb0 = new JLabel("№");
		bx3.add(Box.createHorizontalStrut(5));
		bx3.add(lb0);
		bx3.add(Box.createHorizontalStrut(20));
		
		JLabel lb = new JLabel("Слово на английском");
		bx3.add(lb);
		bx3.add(Box.createHorizontalStrut(40));
		
		JLabel lb1 = new JLabel("Слово на русском");
		bx3.add(lb1);
		bx3.add(Box.createHorizontalStrut(50));
		
		JLabel lb2 = new JLabel("Транскрипцыя");
		bx3.add(lb2);
		bx3.add(Box.createHorizontalStrut(30));
		p2.add(bx3);
		
		//Input fields 1
		Box box1 = Box.createHorizontalBox();
		box1.setPreferredSize(new Dimension(500, 40));
		box1.setBorder(Line1);
		
		JLabel n1 = new JLabel("1");
		box1.add(Box.createHorizontalStrut(11));
		box1.add(n1);
		box1.add(Box.createHorizontalStrut(10));
		
		words1 = new JTextField(25); //Words
		words1.setFont(fonttext);
		box1.add(words1);
		
		transl1 = new JTextField(25); //Translation
		transl1.setFont(fonttext);
		box1.add(transl1);
		
		transc1 = new JTextField(25); //Transcription
		transc1.setFont(fonttext);
		box1.add(transc1);
				
		//Input fields 2
		Box box2 = Box.createHorizontalBox();
		box2.setPreferredSize(new Dimension(500, 40));
		box2.setBorder(Line1);
		
		JLabel n2 = new JLabel("2");
		box2.add(Box.createHorizontalStrut(11));
		box2.add(n2);
		box2.add(Box.createHorizontalStrut(10));
		
		words2 = new JTextField(25); //Words
		words2.setFont(fonttext);
		box2.add(words2);
		
		transl2 = new JTextField(25); //Translation
		transl2.setFont(fonttext);
		box2.add(transl2);
		
		transc2 = new JTextField(25); //Transcription
		transc2.setFont(fonttext);
		box2.add(transc2);
	
		//Input fields 3
		Box box3 = Box.createHorizontalBox();
		box3.setPreferredSize(new Dimension(500, 40));
		box3.setBorder(Line1);
		
		JLabel n3 = new JLabel("3");
		box3.add(Box.createHorizontalStrut(11));
		box3.add(n3);
		box3.add(Box.createHorizontalStrut(10));
		
		words3 = new JTextField(25); //Words
		words3.setFont(fonttext);
		box3.add(words3);
		
		transl3 = new JTextField(25); //Translation
		transl3.setFont(fonttext);
		box3.add(transl3);
		
		transc3 = new JTextField(25); //Transcription
		transc3.setFont(fonttext);
		box3.add(transc3);
			
		//Input fields 4
		Box box4 = Box.createHorizontalBox();
		box4.setPreferredSize(new Dimension(500, 40));
		box4.setBorder(Line1);
		
		JLabel n4 = new JLabel("4");
		box4.add(Box.createHorizontalStrut(11));
		box4.add(n4);
		box4.add(Box.createHorizontalStrut(10));
		
		words4 = new JTextField(25); //Words
		words4.setFont(fonttext);
		box4.add(words4);
		
		transl4 = new JTextField(25); //Translation
		transl4.setFont(fonttext);
		box4.add(transl4);
		
		transc4 = new JTextField(25); //Transcription
		transc4.setFont(fonttext);
		box4.add(transc4);
				
		//Input fields 5
		Box box5 = Box.createHorizontalBox();
		box5.setPreferredSize(new Dimension(500, 40));
		box5.setBorder(Line1);
		
		JLabel n5 = new JLabel("5");
		box5.add(Box.createHorizontalStrut(11));
		box5.add(n5);
		box5.add(Box.createHorizontalStrut(10));
		
		words5 = new JTextField(25); //Words
		words5.setFont(fonttext);
		box5.add(words5);
		
		transl5 = new JTextField(25); //Translation
		transl5.setFont(fonttext);
		box5.add(transl5);
		
		transc5 = new JTextField(25); //Transcription
		transc5.setFont(fonttext);
		box5.add(transc5);
			
		//Input fields 6
		Box box6 = Box.createHorizontalBox();
		box6.setPreferredSize(new Dimension(500, 40));
		box6.setBorder(Line1);
		
		JLabel n6 = new JLabel("6");
		box6.add(Box.createHorizontalStrut(11));
		box6.add(n6);
		box6.add(Box.createHorizontalStrut(10));
		
		words6 = new JTextField(25); //Words
		words6.setFont(fonttext);
		box6.add(words6);
		
		transl6 = new JTextField(25); //Translation
		transl6.setFont(fonttext);
		box6.add(transl6);
		
		transc6 = new JTextField(25); //Transcription
		transc6.setFont(fonttext);
		box6.add(transc6);
			
		//Input fields 7
		Box box7 = Box.createHorizontalBox();
		box7.setPreferredSize(new Dimension(500, 40));
		box7.setBorder(Line1);
		
		JLabel n7 = new JLabel("7");
		box7.add(Box.createHorizontalStrut(11));
		box7.add(n7);
		box7.add(Box.createHorizontalStrut(10));
		
		words7 = new JTextField(25); //Words
		words7.setFont(fonttext);
		box7.add(words7);
		
		transl7 = new JTextField(25); //Translation
		transl7.setFont(fonttext);
		box7.add(transl7);
		
		transc7 = new JTextField(25); //Transcription
		transc7.setFont(fonttext);
		box7.add(transc7);
			
		//Input fields 8
		Box box8 = Box.createHorizontalBox();
		box8.setPreferredSize(new Dimension(500, 40));
		box8.setBorder(Line1);
		
		JLabel n8 = new JLabel("8");
		box8.add(Box.createHorizontalStrut(11));
		box8.add(n8);
		box8.add(Box.createHorizontalStrut(10));
		
		words8 = new JTextField(25); //Words
		words8.setFont(fonttext);
		box8.add(words8);
		
		transl8 = new JTextField(25); //Translation
		transl8.setFont(fonttext);
		box8.add(transl8);
		
		transc8 = new JTextField(25); //Transcription
		transc8.setFont(fonttext);
		box8.add(transc8);
			
		//Input fields 9
		Box box9 = Box.createHorizontalBox();
		box9.setPreferredSize(new Dimension(500, 40));
		box9.setBorder(Line1);
		
		JLabel n9 = new JLabel("9");
		box9.add(Box.createHorizontalStrut(11));
		box9.add(n9);
		box9.add(Box.createHorizontalStrut(10));
		
		words9 = new JTextField(25); //Words
		words9.setFont(fonttext);
		box9.add(words9);
		
		transl9 = new JTextField(25); //Translation
		transl9.setFont(fonttext);
		box9.add(transl9);
		
		transc9 = new JTextField(25); //Transcription
		transc9.setFont(fonttext);
		box9.add(transc9);
			
		//Input fields 10
		Box box10 = Box.createHorizontalBox();
		box10.setPreferredSize(new Dimension(500, 40));
		box10.setBorder(Line1);
		
		JLabel n10 = new JLabel("10");
		box10.add(Box.createHorizontalStrut(5));
		box10.add(n10);
		box10.add(Box.createHorizontalStrut(10));
		
		words10 = new JTextField(25); //Words
		words10.setFont(fonttext);
		box10.add(words10);
		
		transl10 = new JTextField(25); //Translation
		transl10.setFont(fonttext);
		box10.add(transl10);
		
		transc10 = new JTextField(25); //Transcription
		transc10.setFont(fonttext);
		box10.add(transc10);
		
		Box bx4 = Box.createHorizontalBox();
		bx4.setPreferredSize(new Dimension(500, 40));			
		
		//Button for send data
		JButton go = new JButton(new ImageIcon("res/images/3.png"));
		go.setText("Сохранить данные");
		//go.setBorder(null);
		//go.setBackground(null);
		//go.setEnabled(false);
		go.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		go.addActionListener(new ActionListener() {
					
			public void actionPerformed(ActionEvent e) {
				try {
					if (set_id!=0){
						SendData();
					} else{
						atention.setVisible(true);
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		bx4.add(go);
		
		atention = new JLabel("Выберите сначала набор для добавления");
		atention.setForeground(Color.red);
		atention.setVisible(false);
		bx4.add(Box.createHorizontalStrut(30));
		bx4.add(atention);
				
		p2.add(box1);
		p2.add(box2);
		p2.add(box3);
		p2.add(box4);
		p2.add(box5);
		p2.add(box6);
		p2.add(box7);
		p2.add(box8);
		p2.add(box9);
		p2.add(box10);
		p2.add(bx4);
				
		add("North", p1);
		add("Center", jScrollPane);
	}
	
	public void SendData() throws Exception{
		
		Set a=new Set();
		String [][] data;
		String audio, image;
		String tw1,tw2,tw3,tw4,tw5,tw6,tw7,tw8,tw9,tw10;
		String tr1,tr2,tr3,tr4,tr5,tr6,tr7,tr8,tr9,tr10;
		String trc1,trc2,trc3,trc4,trc5,trc6,trc7,trc8,trc9,trc10;
		String ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10;
		String ti1,ti2,ti3,ti4,ti5,ti6,ti7,ti8,ti9,ti10;
		
		Map<Integer, String> map = new HashMap<Integer, String>(); //words
		Map<Integer, String> map1 = new HashMap<Integer, String>(); //translation
		Map<Integer, String> map2 = new HashMap<Integer, String>(); //transcription
		Map<Integer, String> map3 = new HashMap<Integer, String>(); //audio
		Map<Integer, String> map4 = new HashMap<Integer, String>(); //image
		
		data=a.getPassSet(set_id);
		audio=data[0][0];
		image=data[0][1];
		
		//First form
		tw1 = words1.getText(); map.put(1,tw1);
		tr1 = transl1.getText(); map1.put(1,tr1);
		trc1 = transc1.getText(); map2.put(1,trc1);
		ta1 = audio+words1.getText()+".mp3"; map3.put(1,ta1);
		ti1 = image+words1.getText()+".jpg"; map4.put(1,ti1);
		
		//Second form
		tw2 = words2.getText(); map.put(2,tw2);
		tr2 = transl2.getText(); map1.put(2,tr2);
		trc2 = transc2.getText(); map2.put(2,trc2);
		ta2 = audio+words2.getText()+".mp3"; map3.put(2,ta2);
		ti2 = image+words2.getText()+".jpg"; map4.put(2,ti2);
		
		//Third form
		tw3 = words3.getText(); map.put(3,tw3);
		tr3 = transl3.getText(); map1.put(3,tr3);
		trc3 = transc3.getText(); map2.put(3,trc3);
		ta3 = audio+words3.getText()+".mp3"; map3.put(3,ta3);
		ti3 = image+words3.getText()+".jpg"; map4.put(3,ti3);
		
		//Four form
		tw4 = words4.getText(); map.put(4,tw4);
		tr4 = transl4.getText(); map1.put(4,tr4);
		trc4 = transc4.getText(); map2.put(4,trc4);
		ta4 = audio+words4.getText()+".mp3"; map3.put(4,ta4);
		ti4 = image+words4.getText()+".jpg"; map4.put(4,ti4);
		
		//Fife form
		tw5 = words5.getText(); map.put(5,tw5);
		tr5 = transl5.getText(); map1.put(5,tr5);
		trc5 = transc5.getText(); map2.put(5,trc5);
		ta5 = audio+words5.getText()+".mp3"; map3.put(5,ta5);
		ti5 = image+words5.getText()+".jpg"; map4.put(5,ti5);
		
		//sixth form
		tw6 = words6.getText(); map.put(6,tw6);
		tr6 = transl6.getText(); map1.put(6,tr6);
		trc6 = transc6.getText(); map2.put(6,trc6);
		ta6 = audio+words6.getText()+".mp3"; map3.put(6,ta6);
		ti6 = image+words6.getText()+".jpg"; map4.put(6,ti6);
		
		//Seven form
		tw7 = words7.getText(); map.put(7,tw7);
		tr7 = transl7.getText(); map1.put(7,tr7);
		trc7 = transc7.getText(); map2.put(7,trc7);
		ta7 = audio+words7.getText()+".mp3"; map3.put(7,ta7);
		ti7 = image+words7.getText()+".jpg"; map4.put(7,ti7);
		
		//Eight form
		/*tw8 = words8.getText(); map.put(8,tw8);
		tr8 = transl8.getText(); map1.put(8,tr8);
		trc8 = transc8.getText(); map2.put(8,trc8);
		ta8 = audio+words8.getText()+".mp3"; map3.put(8,ta8);
		ti8 = image+words8.getText()+".jpg"; map4.put(8,ti8);
				
	    //Nine form
		tw9 = words9.getText(); map.put(9,tw9);
		tr9 = transl9.getText(); map1.put(9,tr9);
		trc9 = transc9.getText(); map2.put(9,trc9);
		ta9 = audio+words9.getText()+".mp3"; map3.put(9,ta9);
		ti9 = image+words9.getText()+".jpg"; map4.put(9,ti9);
		
		//Ten form
		tw10 = words10.getText(); map.put(10,tw10);
		tr10 = transl10.getText(); map1.put(10,tr10);
		trc10 = transc10.getText(); map2.put(10,trc10);
		ta10 = audio+words10.getText()+".mp3"; map3.put(10,ta10);
		ti10 = image+words10.getText()+".jpg"; map4.put(10,ti10);*/
				
		a.addData(set_id, map, map1, map2, map3, map4);
		
		for (int i=1; i<=map.size(); i++){
			//System.out.println(map.get(i)+" "+map1.get(i)+" "+map2.get(i)+" "+map3.get(i)+" "+map4.get(i));
		}
	}
}
