package windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import data.TakeWords;
import data.TakeWordsVocabluary;
import training.Crossword;
import training.DesignerWords;
import training.Result_of_training;
import training.TranslateWords;
import training.WordsCard;
import training.WordsTranslate;

public class MainWindow implements KeyListener {

	JFrame frame,frame1;
	private JPanel windowContent;
	private JPanel p1,p12,p13;
	JPanel p2;
	private Color Color1, Color2, Color3;
	private Border Line1;
	private JButton btn1, btn2, btn3, btn4;
	Box box1,box2,box3; 
	public ImagePanel pp;
	Dimension sSize,maxSize;
	int vert;
	public MainWindow() throws Exception {
				
		/*------------------------Создаем панели---------------------------------------*/
		Color1 = new Color(146, 243, 102); // green
		Color2 = new Color(250, 194, 90); // yellow
		Color3 = new Color(245,242,233); // grey
		
		 pp = new ImagePanel();
	     pp.setLayout(new FlowLayout());
	        try {
	            pp.setImage(ImageIO.read(new File("res/images/fon.jpg")));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	     
	    // pp.addKeyListener(this);
		// pp.setFocusable(true);     
			
	     //Получаем размер окна   
	     sSize = Toolkit.getDefaultToolkit ().getScreenSize ();
	     vert = sSize.height-90;
	     int hor  = sSize.width-25; 
	     //System.out.println(hor+" "+vert);
	     	     	     
		// Создаем основную панель Border Layout
		windowContent = new JPanel();
		BorderLayout bl = new BorderLayout();
		windowContent.setPreferredSize(new Dimension(hor, vert));//690
		//windowContent.setPreferredSize(new Dimension(1350, 690));//690
		windowContent.setOpaque(false);
		windowContent.setLayout(bl);
		Line1 = BorderFactory.createLineBorder(Color.BLACK, 1, true);
				
		// Область где будет меню и подменю (North p1 (p12,p13))
		p1 = new JPanel();
		BorderLayout bl1 = new BorderLayout();
		p1.setLayout(bl1);
		p1.setOpaque(false);
		p1.setPreferredSize(new Dimension(1000, 150));
	
		// Создаем панель (p12)
		p12 = new JPanel();
		GridBagLayout gbl12 = new GridBagLayout();
		p12.setLayout(gbl12);
		p12.setPreferredSize(new Dimension(600, 70));
		p12.setOpaque(false);

		// Создаем панель (p13)
		p13 = new JPanel();
		GridBagLayout gbl13 = new GridBagLayout();
		p13.setLayout(gbl13);
		p13.setPreferredSize(new Dimension(1000, 80));
		p13.setOpaque(false);
		
		// Создаем панель (Center p2)
		p2 = new JPanel();
		FlowLayout fl = new FlowLayout();
		p2.setLayout(fl);
		p2.setBackground(Color3);
		p2.setOpaque(false);
	
		/*-----------------------------------------------------------------------------------------------*/

		/*-------------------------------Добавляем елементы на панели------------------------------------*/

		// ------------------------------Добавляем елементы на панель p1
		GridBagConstraints c = new GridBagConstraints();
		Font fontbtn = new Font("Arial", Font.PLAIN, 20);

		btn1 = new JButton(new ImageIcon("res/images/words.png"));
		btn1.setRolloverEnabled(true);
		btn1.setRolloverIcon(new ImageIcon("res/images/words2.png"));
		btn1.setBorder(null);
		btn1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				btn1.setSelected(true);
				btn1.setSelectedIcon(new ImageIcon("res/images/words3.png"));
				btn2.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				repairPanel1();
				showWordsPanel();
				showMyWordsSetPanel();
			}
		});
		c.insets = new Insets(0, 0, 0, 10);
		c.gridx = 0;
		c.gridy = 0;
		p12.add(btn1,c);

		btn2 = new JButton(new ImageIcon("res/images/training.png"));
		btn2.setRolloverEnabled(true);
		btn2.setRolloverIcon(new ImageIcon("res/images/training2.png"));
		btn2.setBorder(null);
		btn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				repairPanel1();
				showTrainingSet();
				showTrainingPanel("0");
			}
		});
		c.insets = new Insets(0, 0, 0, 10);
		c.gridx = 1;
		c.gridy = 0;
		p12.add(btn2,c);

		btn3 = new JButton(new ImageIcon("res/images/vocabluary.png"));
		btn3.setRolloverEnabled(true);
		btn3.setRolloverIcon(new ImageIcon("res/images/vocabluary2.png"));
		btn3.setBorder(null);
		btn3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				btn3.setSelected(true);
				btn3.setSelectedIcon(new ImageIcon("res/images/vocabluary3.png"));
				btn1.setSelected(false);
				btn2.setSelected(false);
				btn4.setSelected(false);
				repairPanel1();
				try {
					showVocabularyPanel();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		c.insets = new Insets(0, 0, 0, 10);
		c.gridx = 2;
		c.gridy = 0;
		p12.add(btn3,c);

		btn4 = new JButton(new ImageIcon("res/images/settings.png"));
		btn4.setRolloverEnabled(true);
		btn4.setRolloverIcon(new ImageIcon("res/images/settings2.png"));
		btn4.setBorder(null);
		btn4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btn4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				btn4.setSelected(true);
				btn4.setSelectedIcon(new ImageIcon("res/images/settings3.png"));
				btn1.setSelected(false);
				btn2.setSelected(false);
				btn3.setSelected(false);
				repairPanel1();
				showSettingsPanel();
			}
		});
		c.insets = new Insets(0, 0, 0, 10);
		c.gridx = 3;
		c.gridy = 0;
		p12.add(btn4,c);
			
		p1.add("North", p12);
		p1.add("Center", p13);
		/*-----------------------------------------------------------------------------------------------*/

		// Выставляем панель p1 в северную часть
		windowContent.add("North", p1);

		// Выставляем панель р2 в центральную часть окна
		windowContent.add("Center", p2);
		pp.add(windowContent);					
	}
	
	//Слова и фразы "Мои слова"
	public void showWordsPanel(){
				
		try {
			p13.removeAll();
			p13.add(new WordsPanel());
			btn1.setSelected(true);
			btn1.setSelectedIcon(new ImageIcon("res/images/words3.png"));
			btn2.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p13.validate();
			p13.repaint();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
	}
	
	//Слова и фразы "Обзор"
	public void showWordsPanelTwo(){
					
		try {
			p13.removeAll();
			p13.add(new WordsPanelTwo());
			btn1.setSelected(true);
			btn1.setSelectedIcon(new ImageIcon("res/images/words3.png"));
			btn2.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p13.validate();
			p13.repaint();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
	}
	
	//Слова и фразы MyWordsSetPanel
	public void showMyWordsSetPanel(){
					
		try {
			p2.removeAll();
			p2.add(new MyWordsSetPanel());
			btn1.setSelected(true);
			btn1.setSelectedIcon(new ImageIcon("res/images/words3.png"));
			btn2.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p2.validate();
			p2.repaint();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
	}

	//Слова и фразы WordsSetAddPanel
	public void showWordsSetAddPanel(){
						
		try {
			p2.removeAll();
			p2.add(new WordsSetAddPanel());
			btn1.setSelected(true);
			btn1.setSelectedIcon(new ImageIcon("res/images/words3.png"));
			btn2.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p2.validate();
			p2.repaint();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
	}
		
	//Выбираем набор для тренировки 
	public void showTrainingSet(){
		
		try {
			p13.removeAll();
			p13.add(new TrainingSetListPanel("0"));
			btn2.setSelected(true);
			btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
			btn1.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p13.validate();
			p13.repaint();
			} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	
	//Панель из шести тренировок + прогрес всех слов
	public void showTrainingPanel(String a){
		
		try {
			p13.removeAll();
			p13.add(new TrainingSetListPanel(a));
			p13.validate();
			p13.repaint();
			
			p2.removeAll();
			p2.add(new TrainingPanel(a));
			btn2.setSelected(true);
			btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
			btn1.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			p2.validate();
			p2.repaint();
			} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}
	
	//Панель из шести тренировок + прогресс по наборам
	public void showTrainingPanelAll(String a,  String b){
		
		try {
			p13.removeAll();
			p13.add(new TrainingSetListPanel(a));
			p13.validate();
			p13.repaint();
			
			p2.removeAll();
			p2.add(new TrainingPanelAll(a,b));
			p2.validate();
			p2.repaint();
			
			btn2.setSelected(true);
			btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
			btn1.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
			} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}
	
	//Панель Тренировка (Слово-Первод)
	public void showWordsTranslatePanel(String item, String item1, String training){
		TakeWords m =new TakeWords();
		String[][] words;
		String[][] randwords;
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		map.put(1, "test");
		map1.put(1, 777);
		int prog,size;
			try {
				words=m.getWordsSet(item, training);
				randwords=m.getWordsSetRandom();
				size=words.length;
				if(size>12){
					prog=100/12;
				}else{
					prog=100/size;
				}
				p2.removeAll();
				WordsTranslate WordsTranslate = new WordsTranslate(0,words,randwords,item,item1,training,prog,0,map,map1);
				p2.add(WordsTranslate);
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				p2.validate();
				p2.repaint();
				} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
	}	
	
	//Панель Тренировка (Первод-Слово)
	public void showTranslateWordsPanel(String item, String item1, String training){
		TakeWords m =new TakeWords();
		String[][] words;
		String[][] randwords;
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		map.put(1, "test");
		map1.put(1, 777);
		int prog,size;
			try {
				words=m.getWordsSet(item, training);
				randwords=m.getWordsSetRandom();
				size=words.length;
				if(size>12){
					prog=100/12;
				}else{
					prog=100/size;
				}
				p2.removeAll();
				TranslateWords TranslateWords = new TranslateWords(0,words,randwords,item,item1,training,prog,0,map,map1);
				p2.add(TranslateWords);
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				p2.validate();
				p2.repaint();
				} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
	}
	
	//Панель Тренировка (Конструктор слов)
	public void showWordsKonstructorPanel(String item, String item1, String training){
		TakeWords m =new TakeWords();
		String[][] words;
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		map.put(1, "test");
		map1.put(1, 777);
		int prog,size;
			try {
				words=m.getWordsSet(item, training);
				size=words.length;
				if(size>12){
					prog=100/12;
				}else{
					prog=100/size;
				}
				p2.removeAll();
				DesignerWords designerWords = new DesignerWords(0,words,map,map1,item,item1,prog,0,training);
				p2.add(designerWords);
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				p2.validate();
				p2.repaint();
				} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
		}
	
	//Панель Тренировка (Словарные карточки)
	public void showWordsKard(String item, String item1, String training){
		TakeWords m =new TakeWords();
		String[][] words;
			try {
				if (vert<750){
					p1.setPreferredSize(new Dimension(1000, 85));
					p13.setVisible(false);
				}
				words=m.getWordsSet(item, training);
				p2.removeAll();
				WordsCard wordsCard = new WordsCard(words,item,item1,training );
				p2.add(wordsCard);
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				p2.validate();
				p2.repaint();
				} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
		}

	//Панель Тренировка (Кроссворд)
	public void showCrossword(String item, String item1, String training){
		TakeWords m =new TakeWords();
		String[][] words;
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		map.put(1, "test");
		map1.put(1, 777);
		int prog,size;
			try {
				if (vert<750){
					p1.setPreferredSize(new Dimension(1000, 85));
					p13.setVisible(false);
				}
				words=m.getWordsSet(item, training);
				size=words.length;
				if(size>12){
					prog=100/12;
				}else{
					prog=100/size;
				}
				p2.removeAll();
				Crossword crossword = new Crossword(0,words,map,map1,item,item1,prog,0,training);
				p2.add(crossword);
				btn2.setSelected(true);
				btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
				btn1.setSelected(false);
				btn3.setSelected(false);
				btn4.setSelected(false);
				p2.validate();
				p2.repaint();
				} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
	}	
	
	//Личный словарь пользователя
	public void showVocabularyPanel() throws Exception{
		TakeWordsVocabluary s = new TakeWordsVocabluary();
		String [][] words;
		words=s.getWords();
		try {
			p2.removeAll();
			if (words==null){
				JLabel temp = new JLabel(new ImageIcon("res/images/noWords.jpeg"));
				p2.add(temp);
				p2.validate();
				p2.repaint();
			}else{
				p2.add(new VocabularyPanel());
				p2.validate();
				p2.repaint();
			}	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//Панель настроек
	public void showSettingsPanel(){
		try {
			p2.removeAll();
			//p2.add(new SettingsPanel());
			p2.add(new InsertData());
			btn4.setSelected(true);
			btn4.setSelectedIcon(new ImageIcon("res/images/settings3.png"));
			btn1.setSelected(false);
			btn2.setSelected(false);
			btn3.setSelected(false);
			p2.validate();
			p2.repaint();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Панель результат тренировок
	public void showResultPanel(String [][] data, Map<Integer, String> a, Map<Integer, Integer> b, String item, String item1, String training){
		try {
			if (vert<750){
				p1.setPreferredSize(new Dimension(1000, 85));
				p13.setVisible(false);
			}
			p2.removeAll();
			p2.add(new Result_of_training(data, a, b, item,item1, training));
			p2.validate();
			p2.repaint();
			btn2.setSelected(true);
			btn2.setSelectedIcon(new ImageIcon("res/images/training3.png"));
			btn1.setSelected(false);
			btn3.setSelected(false);
			btn4.setSelected(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	public void repairPanel1(){
		p1.setPreferredSize(new Dimension(1000, 150));
		p13.setVisible(true);
	}
	
	// Создаем фрейм и задаем для него панель
	public void createAndShowGUI() {
		frame = new JFrame("Study English");
		frame.setContentPane(pp);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowFocusListener (new WindowFocusListener() {
		
			public void windowGainedFocus(WindowEvent e) {}
							
			public void windowLostFocus(WindowEvent e) {
				//frame.dispose();
			}
		});
		frame.pack();
		frame.setVisible(true);
	}
	
	public void keyTyped(KeyEvent e) {
		
	}
	
	public void keyPressed(KeyEvent e) {
		int b = e.getKeyCode();
		System.out.println("Key Pressed: " + b);
	}
	
	public void keyReleased(KeyEvent e) {
		
	}
}