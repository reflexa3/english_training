package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
 
public class TakeWordsVocabluary{
	String[][] words;
	
	/*Получаем список все слов пользователя в файле (VocabluaryPanel) 
	 откуда вызываеться файл TakeWordsVocabluaryPanel*/
	public String[][] getWords() throws Exception {
		
		int a,b,c;
		String d,e,l;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		Map<Integer, Integer> map2 = new HashMap<Integer, Integer>();
				
		Connection conn = null;
		Statement stmt = null;
		Connection conn1 = null;
		Statement stmt1 = null;
		ResultSet rs =null;
		        
		try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		conn1 = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt1 = conn1.createStatement();
    		
    		rs = stmt.executeQuery("SELECT words_id FROM table0");
    		int i=1;
    		while (rs.next()) {
    			map.put(i, rs.getInt(1));
    			i++;
    		}
    		
    	//Проверка есть ли слова в базе
    	if (map.size()!=0){
    			
    		rs = stmt.executeQuery("SELECT set_of_words_id FROM table0");
    		int i1=1;
    		while (rs.next()) {
    			map1.put(i1, rs.getInt(1));
    			i1++;
    		}
    		
    		a=map.size();
    		
    		for(int w=1; w<=a; w++){
    			l=map.get(w).toString();
    			rs = stmt.executeQuery("SELECT progress_id FROM table0 WHERE words_id="+l);
    			while (rs.next()) {
					map2.put(w, rs.getInt(1));
				}
    		}
    		    		
    		b=map.get(1);//words_id
    		c=map1.get(1);//set_of_words_id
    		d=map2.get(1).toString();//progress_id
    		
    		words = new String [a][7]; //отдельно заполняем первый елемент массива
    			rs = stmt1.executeQuery("SELECT table1.set_of_words, table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 	"FROM table1, table2, table3, table4, table5, table6 " +
				 	"WHERE table1.set_of_words_id ="+c+" AND table2.words_id ="+b+" AND table3.words_id ="+b+" AND table4.words_id ="+b+" AND table5.words_id ="+b+" AND table6.words_id ="+b);
				while (rs.next()) {
					words[0] = new String []{rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(1),rs.getString(6),d};
				}
    		
    		for (int i2=2; i2<=a; i2++) {
				int j=map.get(i2);//words_id
				int j1=map1.get(i2);//set_of_words_id
				e=map2.get(i2).toString();//progress_id
				rs = stmt1.executeQuery("SELECT table1.set_of_words, table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 	"FROM table1, table2, table3, table4, table5, table6 " +
				 	"WHERE table1.set_of_words_id ="+j1+" AND table2.words_id ="+j+" AND table3.words_id ="+j+" AND table4.words_id ="+j+" AND table5.words_id ="+j+" AND table6.words_id ="+j);
				while (rs.next()) {
					words[i2-1] = new String []{rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(1),rs.getString(6),e};
				}
    		}
    	}else{
        	words=null;
        }
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (stmt1 != null) {
                stmt1.close();
            }
        }
		
       /* for (int z=0; z<; z++){
			//System.out.println(z+" "+words[z][0]+" "+words[z][1]+" "+words[z][2]+" "+words[z][3]+" "+words[z][4]+" "+words[z][5]+" "+words[z][6]);
		}
		
        for (int i=1; i<=a; i++){
			//System.out.println(map.get(i)+" "+map1.get(i));
		}*/
       
		return words;
	}
	
	/*Получаем список слов только для определенного набора
	на панели "Обзор" в файле (WordsSetAddPanel) откуда вызываеться файл
	TakeWordsVocabluaryPanel*/
	public String[][] getSetOfWords(int p) throws Exception {
		
		int t=p;
		int a,b,c;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		String[][] words;
		
		Connection conn1 = null;
		Statement stmt1 = null;
		ResultSet rs =null;
		        
		try {
        	    		
    		conn1 = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt1 = conn1.createStatement();
    		
    		rs = stmt1.executeQuery("SELECT words_id FROM table2 WHERE set_of_words_id="+t);
    		int i=1;
    		while (rs.next()) {
    			map.put(i, rs.getInt(1));
    			i++;
    		}
    		
    		rs = stmt1.executeQuery("SELECT set_of_words_id FROM table2 WHERE set_of_words_id="+t);
    		int i1=1;
    		while (rs.next()) {
    			map1.put(i1, rs.getInt(1));
    			i1++;
    		}
    		
    		a=map.size();
    		b=map.get(1);//words_id
    		c=map1.get(1);//set_of_words_id
    		
    		words = new String [a][6]; //отдельно заполняем первый елемент массива
    			rs = stmt1.executeQuery("SELECT table1.set_of_words, table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 	"FROM table1, table2, table3, table4, table5, table6 " +
				 	"WHERE table1.set_of_words_id ="+c+" AND table2.words_id ="+b+" AND table3.words_id ="+b+" AND table4.words_id ="+b+" AND table5.words_id ="+b+" AND table6.words_id ="+b);
				while (rs.next()) {
					words[0] = new String []{rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(1),rs.getString(6)};
				}
    		
    		for (int i2=2; i2<=a; i2++) {
				int j=map.get(i2);//words_id
				int j1=map1.get(i2);//set_of_words_id
				rs = stmt1.executeQuery("SELECT table1.set_of_words, table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 	"FROM table1, table2, table3, table4, table5, table6 " +
				 	"WHERE table1.set_of_words_id ="+j1+" AND table2.words_id ="+j+" AND table3.words_id ="+j+" AND table4.words_id ="+j+" AND table5.words_id ="+j+" AND table6.words_id ="+j);
				while (rs.next()) {
					words[i2-1] = new String []{rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(1),rs.getString(6)};
				}
    		}
    		
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt1 != null) {
                stmt1.close();
            }
        }
		
        for (int z=0; z<a; z++){
			//System.out.println(z+". "+words[z][0]+". "+words[z][1]+". "+words[z][2]+". "+words[z][3]+". "+words[z][4]+". "+words[z][5]);
		}
		
        for (int i=1; i<=a; i++){
			//System.out.println(map.get(i)+" "+map1.get(i));
		}
		return words;
	}
	
	static Connection getConnection(String dbURL, String user, String password)
		      throws SQLException, ClassNotFoundException {
		    Class.forName("com.mysql.jdbc.Driver");
		    return DriverManager.getConnection(dbURL, user, password);
		  }

}