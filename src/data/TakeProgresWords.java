package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
 
public class TakeProgresWords{
	
	
	/*Получаем список прогреса всех добавленых на изучения слов 
	 пользователя отдельно по каждой тренировке в файле (TrainingPanel) 
	 откуда вызываеться файл TakeProgresWords*/
	public int[] getProgresWordsAll() throws Exception {
		
		int a0,a1,a2,a3,a4,a5;
		int [] words = new int [6];
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		        
		try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		rs = stmt.executeQuery("SELECT words_translate, COUNT(words_translate) FROM table0 WHERE words_translate='0'");
    		while (rs.next()) {
    			a0=rs.getInt(2);
    			words[0]=a0;
    		}
    		rs = stmt.executeQuery("SELECT translate_words, COUNT(translate_words) FROM table0 WHERE translate_words='0'");
    		while (rs.next()) {
    			a1=rs.getInt(2);
    			words[1]=a1;
    		}
    		rs = stmt.executeQuery("SELECT konstructor, COUNT(konstructor) FROM table0 WHERE konstructor='0'");
    		while (rs.next()) {
    			a2=rs.getInt(2);
    			words[2]=a2;
    		}
    		rs = stmt.executeQuery("SELECT audio, COUNT(audio) FROM table0 WHERE audio='0'");
    		while (rs.next()) {
    			a3=rs.getInt(2);
    			words[3]=a3;
    		}
    		rs = stmt.executeQuery("SELECT words_card, COUNT(words_card) FROM table0 WHERE words_card='0'");
    		while (rs.next()) {
    			a4=rs.getInt(2);
    			words[4]=a4;
    		}
    		rs = stmt.executeQuery("SELECT crossword, COUNT(crossword) FROM table0 WHERE crossword='0'");
    		while (rs.next()) {
    			a5=rs.getInt(2);
    			words[5]=a5;
    		}
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
          
        }
		//System.out.println(words[0]+" "+words[1]+" "+words[2]+" "+words[3]+" "+words[4]+" "+words[5]);
      
		return words;
	}
	
	/*Получаем список прогреса конкретного набора слов 
	 отдельно по каждой тренировке в файле (TrainingPanelAll) 
	 откуда вызываеться файл TakeProgresWords*/
	public int[] getProgresWords(String item) throws Exception {
					
		int a0,a1,a2,a3,a4,a5;
		int [] words = new int [6];
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		
		try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    			
    		rs = stmt.executeQuery("SELECT words_translate, COUNT(words_translate) FROM table0  WHERE set_of_words_id="+item+" AND words_translate='0'");
    		while (rs.next()) {
    			a0=rs.getInt(2);
    			words[0]=a0;
    		}
    		rs = stmt.executeQuery("SELECT translate_words, COUNT(translate_words) FROM table0 WHERE set_of_words_id="+item+" AND translate_words='0'");
    		while (rs.next()) {
    			a1=rs.getInt(2);
    			words[1]=a1;
    		}
    		rs = stmt.executeQuery("SELECT konstructor, COUNT(konstructor) FROM table0 WHERE set_of_words_id="+item+" AND konstructor='0'");
    		while (rs.next()) {
    			a2=rs.getInt(2);
    			words[2]=a2;
    		}
    		rs = stmt.executeQuery("SELECT audio, COUNT(audio) FROM table0 WHERE set_of_words_id="+item+" AND audio='0'");
    		while (rs.next()) {
    			a3=rs.getInt(2);
    			words[3]=a3;
    		}
    		rs = stmt.executeQuery("SELECT words_card, COUNT(words_card) FROM table0 WHERE set_of_words_id="+item+" AND words_card='0'");
    		while (rs.next()) {
    			a4=rs.getInt(2);
    			words[4]=a4;
    		}
    		rs = stmt.executeQuery("SELECT crossword, COUNT(crossword) FROM table0 WHERE set_of_words_id="+item+" AND crossword='0'");
    		while (rs.next()) {
    			a5=rs.getInt(2);
    			words[5]=a5;
    		}
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
          
        }
		//System.out.println(words[0]+" "+words[1]+" "+words[2]+" "+words[3]+" "+words[4]+" "+words[5]);
      	return words;
	}
		
	/*Результат тренировки вносим в базу*/
	public void ChangeProgressWords(String training, Map<Integer, String> a, Map<Integer, Integer> b) throws Exception {
				
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		map=a;
		map1=b;
		String d;
		int f,q = 0;
		int c=map.size();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
						
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		for (int i=1; i<=c; i++) {
    			d=map.get(i);
    			f=map1.get(i);
    			stmt.executeUpdate("UPDATE table0 SET "+training+"="+f+" WHERE words_id="+d);
    			rs=stmt.executeQuery("SELECT progress_id FROM table0 WHERE words_id="+d);
    			while (rs.next()) {
        			q=rs.getInt(1);
        		}
    			if(map1.get(i)==1){
    				q=q+1;
    			}
    			stmt.executeUpdate("UPDATE table0 SET progress_id="+q+" WHERE words_id="+d);
    		}   	       
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }
	
	static Connection getConnection(String dbURL, String user, String password)
		      throws SQLException, ClassNotFoundException {
		    Class.forName("com.mysql.jdbc.Driver");
		    return DriverManager.getConnection(dbURL, user, password);
		  }

}