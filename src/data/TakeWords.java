package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
 
public class TakeWords{
	
	/*Получаем список слов набора/наборов для разных тренировок*/
	public String[][] getWordsSet(String item, String training) throws Exception {
		
		int[] tmp;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		String[][] words10; 
		int a;
		String b,c,d,f,l;
				
		Connection conn = null;
		Statement stmt = null;
		Connection conn1 = null;
		Statement stmt1 = null;
		ResultSet rs =null;
		//System.out.println(item+" "+training);
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		conn1 = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt1 = conn1.createStatement();
    		
    		if(item!=null){
    			rs = stmt.executeQuery("SELECT words_id FROM table0 WHERE set_of_words_id="+item+" AND "+training+"=0");
    			int i=1;
    			while (rs.next()) {
    				map.put(i, rs.getInt(1));
    			i++;
    		}
    		}else{
    			rs = stmt.executeQuery("SELECT words_id FROM table0 WHERE "+training+"=0");
        		int i=1;
        		while (rs.next()) {
        			map.put(i, rs.getInt(1));
        			i++;
        		}
    		}
    		
    		//-------------Sort map random
    		a=map.size();
    		tmp=new int[a];
    		tmp[0]=map.get(1);
    		for (int i1=2; i1<=a; i1++){
    			tmp[i1-1]=map.get(i1);
    		}
    		shuffleArray(tmp);
    		for (int i1=1; i1<=a; i1++){
    			map.put(i1, tmp[i1-1]);
    		}
    		//-------------End sort
    		    		
    		for(int w=1; w<=a; w++){
    			l=map.get(w).toString();
    			rs = stmt.executeQuery("SELECT progress_id FROM table0 WHERE words_id="+l);
    			while (rs.next()) {
					map1.put(w, rs.getInt(1));
				}
    		}
    		
			b=map.get(1).toString();//words_id
			d=map1.get(1).toString();//progress_id
    		words10 = new String [a][7];
    		//отдельно заполняем первый елемент массива
    		rs = stmt1.executeQuery("SELECT table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 "FROM table2, table3, table4, table5, table6 " +
				 "WHERE table2.words_id ="+b+" AND table3.words_id ="+b+" AND table4.words_id ="+b+" AND table5.words_id ="+b+" AND table6.words_id ="+b);
				while (rs.next()) {
					words10[0] = new String []{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),b,d};
				}
    		
    		for (int i1=2; i1<=a; i1++) {
				c=map.get(i1).toString();
				f=map1.get(i1).toString();
					rs = stmt1.executeQuery("SELECT table2.words, table3.translation, table4.transcription, table5.audio, table6.image " +
				 	"FROM table2, table3, table4, table5, table6 " +
				 	"WHERE table2.words_id ="+c+" AND table3.words_id ="+c+" AND table4.words_id ="+c+" AND table5.words_id ="+c+" AND table6.words_id ="+c);
				while (rs.next()) {
					words10[i1-1] = new String []{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),c,f};
				}
    		}
            
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (stmt1 != null) {
                stmt1.close();
            }
        }
        for (int z=0; z<a; z++){
			//System.out.println(z+"  "+words10[z][0]+" "+words10[z][1]+" "+words10[z][2]+" "+words10[z][3]+" "+words10[z][4]+" "+words10[z][5]+" "+words10[z][6]);
		}
        
        for (int i=1; i<=a; i++){
			//System.out.println(map.get(i));
		}
		return words10;
	}
	
	/*Получаем список всех слов для тренировок перевод-слово*/
	public String[][] getWordsSetRandom() throws SQLException, ClassNotFoundException{

		int[] tmp;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		String[][] words10; 
		int a;
		String b,c,d,f,l;
				
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		
        try {
        	conn = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt = conn.createStatement();
    		
    		rs = stmt.executeQuery("SELECT words_id FROM table2");
        	int i=1;
        		while (rs.next()) {
        			map.put(i, rs.getInt(1));
        			i++;
        		}
    		    		
    		//-------------Sort map random
    		a=map.size();
    		tmp=new int[a];
    		tmp[0]=map.get(1);
    		for (int i1=2; i1<=a; i1++){
    			tmp[i1-1]=map.get(i1);
    		}
    		shuffleArray(tmp);
    		for (int i1=1; i1<=a; i1++){
    			map.put(i1, tmp[i1-1]);
    		}
    		//-------------End sort
    	    		
			b=map.get(1).toString();//words_id
			words10 = new String [a][2];
    		
			//отдельно заполняем первый елемент массива
    		rs = stmt.executeQuery("SELECT table2.words, table3.translation " +
				 "FROM table2, table3 " +
				 "WHERE table2.words_id ="+b+" AND table3.words_id ="+b);
				while (rs.next()) {
					words10[0] = new String []{rs.getString(1), rs.getString(2)};
				}
    		
    		for (int i1=2; i1<=a; i1++) {
				c=map.get(i1).toString();
				rs = stmt.executeQuery("SELECT table2.words, table3.translation " +
				 	"FROM table2, table3 " +
				 	"WHERE table2.words_id ="+c+" AND table3.words_id ="+c);
				while (rs.next()) {
					words10[i1-1] = new String []{rs.getString(1), rs.getString(2)};
				}
    		}
            
		}finally {
			if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
       return words10;		
	}
	
	// Алгоритм перемешивания рандомно
	public static String shuffleArray(int[] tmp) {
		
		int n = tmp.length;
		Random random = new Random();
		random.nextInt();
		for (int i = 0; i < n; i++) {
			int change = i + random.nextInt(n - i);
			swap(tmp, i, change);
		}
		return null;
	}

	private static void swap(int[] tmp, int i, int change) {
		// TODO Auto-generated method stub
		int temp = tmp[i];
		tmp[i] = tmp[change];
		tmp[change] = temp;
	}
	
	static Connection getConnection(String dbURL, String user, String password)
		      throws SQLException, ClassNotFoundException {
		    Class.forName("com.mysql.jdbc.Driver");
		    return DriverManager.getConnection(dbURL, user, password);
		  }
}