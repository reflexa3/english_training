package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
   
public class Set {
	//int set_of_words_id, words_id;
	//String words,translation,transcription,audio,image;
	
	/*Додаем набор для изучения на панели "Обзор" в файле (WordsSetAddPanel)
	при нажатии кнопки "Добавить набор"	(устанавливаем видимость набора 
	+ копируем слова в таблицу пользователя)*/
	public void addSetOfSet(int a) throws Exception {
		Connection conn = null;
		Statement stmt = null;
				
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		stmt.executeUpdate("UPDATE table1 SET visible='true' WHERE set_of_words_id="+a+"");
    		//stmt.executeUpdate("UPDATE table1 SET visible='false'");
    		//stmt.executeUpdate("UPDATE table1 SET visible='true'");
    		stmt.execute("INSERT INTO user_bd.table0 (words_id,set_of_words_id)" +
    					  "SELECT (words_id),(set_of_words_id)" +
    					  "FROM english.table2 " +
    					  "WHERE table2.set_of_words_id="+a);
    	    		
    		System.out.println("set was add");
		} finally {
               if (stmt != null) {
            	   stmt.close();
               }
        }
	}
	
	/*Получаем список наборов для панели "Мои слова" в файле (MyWordsSetPanel) 
	по которому будет устанавливаться отображаемость набора*/
	public Boolean[] getSetVisible() throws Exception {
		
		Boolean[] sets= new Boolean[139];
		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
				
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		rs = stmt.executeQuery("SELECT set_of_words_id, visible FROM table1");
    			while (rs.next()) {
    				map.put(rs.getInt(1), rs.getBoolean(2));
    			}
    	} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        
        for (int i=1; i<sets.length; i++){
        	sets[i] = map.get(i);
        	//System.out.println(i+" "+sets[i]);
        }
		return sets;
	}
	
	/*Удаляем набор для изучения на панели "Мои слова" в файле (MyWordsSetPanel)
	при нажатии на красную иконку */
	public void delSetOfSet(int i) throws Exception {
		Connection conn = null;
		Statement stmt = null;
						
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		stmt.executeUpdate("UPDATE table1 SET visible='false' WHERE set_of_words_id="+i+"");
    		System.out.println("data was sended");
		} finally {
               if (stmt != null) {
                stmt.close();
            }
        }
	}	
	
	/*Получаем Список наборов для панели "Обзор" в файле (WordsSetAddPanel) и 
	 *Список наборов + подсчет поднаборов в файле (WordsPanel)*/
	public String[] getSetOfSet() throws Exception {
			
		String[] set15 = new String [16]; 
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
				
        try {
        	conn = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt = conn.createStatement();
    		
    		rs = stmt.executeQuery("SELECT set_of_set_id, set_of_set FROM table0");
    			while (rs.next()) {
    				map.put(rs.getInt(1), rs.getString(2));
    			}
    		
    		rs = stmt.executeQuery("SELECT set_of_set, COUNT(set_of_words) FROM table1 GROUP BY set_of_set");
				while (rs.next()) {
    				map1.put(rs.getInt(1), rs.getInt(2));
    			}
    		     
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        
        int count=0;
                        
        for (int z=1; z<16; z++){
        	set15[z]=map.get(z)+" ("+map1.get(z)+")";
        	count=count+map1.get(z);
        //	System.out.println(set15[z]);
        }
              
        set15[0]="Все наборы слов"+" ("+count+")";
                
		return set15;
	}
	
	/*Получаем количество наборов на изучении для панели "Мои слова"
	 в файле (WordsPanel)*/
	public int getStudyCountSetOfSet() throws Exception {
		
		int a=0;	
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
				
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		
    		rs = stmt.executeQuery("SELECT visible, COUNT(visible) FROM table1 GROUP BY visible='true'");
				while (rs.next()) {
					if(rs.getInt(2)==138){
						a=0;
					}else{
						a=rs.getInt(2);	
					}
    				
				}
    	} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
       return a;
	}
		
	/*Возвращаем названия набора + количество в нем слов для панели "Мои слова"
	в файле (MyWordsSetPanel) и панели "Обзор" в файле (WordsSetAddPanel)*/
	public String[][] getCountWords() throws Exception {
	
		String[][] data = new String [139][3];
	
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, String> map1 = new HashMap<Integer, String>();
		Map<Integer, String> map2 = new HashMap<Integer, String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
			
		try {
			conn = getConnection("jdbc:mysql://localhost/english", "root", "");
			stmt = conn.createStatement();
		
			rs = stmt.executeQuery("SELECT set_of_words_id, set_of_words FROM table1_1");
				while (rs.next()) {
					map.put(rs.getInt(1), rs.getString(2));
				}
										
			rs = stmt.executeQuery("SELECT set_of_words_id, COUNT(set_of_words_id) FROM table2 GROUP BY set_of_words_id");
				while (rs.next()) {
					map1.put(rs.getInt(1), rs.getString(2));
				}
		    
			rs = stmt.executeQuery("SELECT set_of_words_id, pass FROM table7");
				while (rs.next()) {
					map2.put(rs.getInt(1), rs.getString(2));
				}
				
		} finally {
			if (rs != null) {
				rs.close();
			}
        if (stmt != null) {
            stmt.close();
        	}
		}
		
		for (int i=1; i<data.length; i++){
			data[i] = new String []{map.get(i), map1.get(i)+" слов", map2.get(i)};
			//System.out.println(data[i][0]+" "+data[i][1]+" "+data[i][2]);
		}
		return data;
	}

	/*Возвращаем названия набора + количество в нем слов которые добавлены на узучения
	 и отображаются на панели для тренировок*/
	public String[][] getCountAddSet() throws Exception {
		
		String[] elements0; 
		String[] elements; 
		String[][] elements1; 
		
		Map<Integer, String> map0 = new HashMap<Integer, String>();
		Map<Integer, String> map = new HashMap<Integer, String>();
		Map<Integer, String> map1 = new HashMap<Integer, String>();
		Map<Integer, Boolean> map2 = new HashMap<Integer, Boolean>();
					
		ArrayList<String> test = new ArrayList<String>();	
		ArrayList<String> test1 = new ArrayList<String>();	
					
		Connection conn = null;
		Statement stmt = null;
		Connection conn1 = null;
		Statement stmt1 = null;
		ResultSet rs =null;
		int a=0;
		int b=0;
		int b1=0;
		int b0=0;
		
        try {
        	conn = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt = conn.createStatement();
    		
    		conn1 = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt1 = conn1.createStatement();
    		
    		rs = stmt.executeQuery("SELECT set_of_words_id, set_of_words FROM table1");
    			while (rs.next()) {
    				map0.put(rs.getInt(1), rs.getString(1));
    				map.put(rs.getInt(1), rs.getString(2));
    			}
    		
    		rs = stmt.executeQuery("SELECT set_of_words_id, COUNT(set_of_words_id) FROM table2 GROUP BY set_of_words_id");
				while (rs.next()) {
    				map1.put(rs.getInt(1), rs.getString(2));
    			}
				
			rs = stmt1.executeQuery("SELECT set_of_words_id, visible FROM table1");
				while (rs.next()) {
					map2.put(rs.getInt(1), rs.getBoolean(2));
				}
				
			rs = stmt1.executeQuery("SELECT words_id, COUNT(words_id) FROM table0");
				while (rs.next()) {
    				a=rs.getInt(2);
				}
		} finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (stmt1 != null) {
                stmt1.close();
            }
        }
        
        b0=map0.size();
        elements0 = new String[b0];
        elements0[0]=map0.get(1);
        for (int i=1; i<elements0.length; i++){
        	elements0[i]=map0.get(i);
        	//System.out.println(i+" "+elements0[i]+" "+map0.get(i));
              }
        
        b=map.size();
		elements = new String [b];
		elements[0]="Все мои слова "+a;   
        for (int i=1; i<elements.length; i++){
        	elements[i]=map.get(i)+" "+map1.get(i);
        	//System.out.println(i+" "+elements[i]+" "+map2.get(i)+" "+elements0[i]);
        }
        
        test.add(elements[0]);
        test1.add("");
        for (int i=1; i<elements.length; i++){
        	if(map2.get(i)==true){
        		test.add(elements[i]);
        		test1.add(elements0[i]);
        	}
        }
        
       b1=test.size();
       elements1 = new String [b1][2];	          
       for (int i=0; i<test.size(); i++){
        	elements1[i]= new String []{test.get(i),test1.get(i)};
       }
        
       for (int z=0; z<elements1.length; z++){
			//System.out.println(z+" "+elements1[z][0]+" "+elements1[z][1]);
		}
       
        for (int i=0; i<test.size(); i++){
        	//System.out.println(i+" "+test.get(i)+" "+test1.get(i));
        }
        	return elements1;
	}
	
	/*Удаляем набор для изучения на панели "Мои слова" в файле (MyWordsSetPanel)
	при нажатии на красную иконку */
	public void delSetOfWords(int i) throws Exception {
		Connection conn = null;
		Statement stmt = null;
						
        try {
        	conn = getConnection("jdbc:mysql://localhost/user_bd", "root", "");
    		stmt = conn.createStatement();
    		stmt.execute ("DELETE FROM user_bd.table0 WHERE table0.set_of_words_id="+i+"");
    		//System.out.println("data was deleted");
		} finally {
               if (stmt != null) {
                stmt.close();
            }
        }
	}
	
	/*Возвращаем названия всех наборов*/
	public String[] getAllSet() throws Exception {
		
		String[] elements; 
		Map<Integer, String> map = new HashMap<Integer, String>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		int a=0;
		
       try {
    	   conn = getConnection("jdbc:mysql://localhost/english", "root", "");
   		   stmt = conn.createStatement();
   	   		
   		   rs = stmt.executeQuery("SELECT set_of_words_id, set_of_words FROM table1");
   		   while (rs.next()) {
   			   map.put(rs.getInt(1), rs.getString(2));
   		   }
       	}finally {
           if (rs != null) {
               rs.close();
           }
           if (stmt != null) {
               stmt.close();
           }
        }
      
       a=map.size();
       elements = new String [a];
       elements[0]=map.get(1);   
       
       for (int i=2; i<=a; i++){
    	   elements[i-1]=map.get(i);
       }
        return elements;
	}
	
	/*Получаем путь к папкам со звуком и картинками по выбраному списку 
	 * в файле InsertData*/
	public String[][] getPassSet(int a) throws Exception {
	
		String[][] data = new String [1][2];
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
			
		try {
			conn = getConnection("jdbc:mysql://localhost/english", "root", "");
			stmt = conn.createStatement();
		
			rs = stmt.executeQuery("SELECT audio, image FROM table8 WHERE set_of_words_id="+a);
				while (rs.next()) {
					data[0] = new String []{rs.getString(1),rs.getString(2)};
				}
				   				
		} finally {
			if (rs != null) {
				rs.close();
			}
        if (stmt != null) {
            stmt.close();
        	}
		}
			//System.out.println(data[0][0]+" "+data[0][1]);
		return data;
	}

	/*Додаем данные в бд */
	public void addData(int a, Map<Integer, String> b, Map<Integer, String> c,  Map<Integer, String> d,  Map<Integer, String> e,  Map<Integer, String> f) throws Exception {
		
		int i,set_of_words_id, words_id = 0;
		String words,translation,transcription,audio,image;
		
		Map<Integer, String> map = new HashMap<Integer, String>(); //words
		Map<Integer, String> map1 = new HashMap<Integer, String>(); //translation
		Map<Integer, String> map2 = new HashMap<Integer, String>(); //transcription
		Map<Integer, String> map3 = new HashMap<Integer, String>(); //audio
		Map<Integer, String> map4 = new HashMap<Integer, String>(); //image
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		
		map=b;
		map1=c;
		map2=d;
		map3=e;
		map4=f;
		
		set_of_words_id=a;
				
		for (i=1; i<map.size(); i++){
			//System.out.println(a+" "+map.get(i)+" "+map1.get(i)+" "+map2.get(i)+" "+map3.get(i)+" "+map4.get(i));
		}
						
        try {
        	conn = getConnection("jdbc:mysql://localhost/english", "root", "");
    		stmt = conn.createStatement();
    		
    		for (i=1; i<=map.size(); i++){
    			
    			words=map.get(i);
    			translation=map1.get(i);
    			transcription=map2.get(i);
    			audio=map3.get(i);
    			image=map4.get(i);
    			
    			stmt.execute("INSERT INTO table2 (set_of_words_id, words)"+"VALUES('"+set_of_words_id+"','"+words+"')");
    			rs = stmt.executeQuery("SELECT words_id FROM table2 WHERE words="+"'"+words+"'");
    			while (rs.next()){
    				words_id=rs.getInt(1);
    			}
    			stmt.execute("INSERT INTO table3 (words_id, translation)"+"VALUES('"+words_id+"','"+translation+"')");
    			stmt.execute("INSERT INTO table4 (words_id, transcription)"+"VALUES('"+words_id+"','"+transcription+"')");
    			stmt.execute("INSERT INTO table5 (words_id, audio)"+"VALUES('"+words_id+"','"+audio+"')");
    			stmt.execute("INSERT INTO table6 (words_id, image)"+"VALUES('"+words_id+"','"+image+"')");
    		}
    	} finally {
			if (stmt != null) {
				stmt.close();
            }
        }
	}
	
	static Connection getConnection(String dbURL, String user, String password)
		      throws SQLException, ClassNotFoundException {
		    Class.forName("com.mysql.jdbc.Driver");
		    return DriverManager.getConnection(dbURL, user, password);
	 }

}